package cz.atha.flashcards.lib.util;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileTraversal {
	private List<String> excludeDirectories = new ArrayList<String>();

	public final void traverse(final File f) throws IOException {
		if (f.isDirectory()) {

			// look through exclude directories that we don't traverse
			for (String dir : this.excludeDirectories) {
				if (f.getAbsolutePath().startsWith(dir))
					return;
			}

			onDirectory(f);
			final File[] childs = f.listFiles();
			if (childs == null)
				System.out.println("null here!: "+f.getAbsolutePath());
			for (File child : childs) {
				traverse(child);
			}
			return;
		}
		onFile(f);
	}

	public void onDirectory(final File d) {
	}

	public void onFile(final File f) throws IOException {
	}

	public void addExcludeDirectory(String dir) {
		excludeDirectories.add(dir);
	}
}
