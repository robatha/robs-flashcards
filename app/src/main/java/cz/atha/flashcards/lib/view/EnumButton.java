/**
 *
 */
package cz.atha.flashcards.lib.view;

import java.util.ArrayList;
import java.util.List;

import cz.atha.flashcards.R;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;;
import android.util.SparseArray;

import android.view.ViewParent;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import androidx.appcompat.widget.AppCompatToggleButton;

/**
 * @author rob
 *
 */
public class EnumButton extends AppCompatToggleButton implements OnCheckedChangeListener {

    final public static String LOGTAG = "EnumButton";
    final public static int NO_VALUE = -1;

    private static List<EnumButton> allButtons = new ArrayList<EnumButton>();
    private static SparseArray<List<EnumButton>> allButtonsMap = new SparseArray<List<EnumButton>>();


    private int group = NO_VALUE;
    private OnCheckedChangeListener listener = null;
    private ViewParent page = null;
    private boolean disableListener = false;

    private Context context = null;
    private ViewParent inflatedParentView;

    public static List<EnumButton> getButtons(ViewParent view, int group) {



        List<EnumButton> l = new ArrayList<EnumButton>();

        for (EnumButton button : allButtons) {
            if (button.getGroup() == group && button.getPage().equals(view))
                l.add(button);
        }
        return l;
    }

    public List<EnumButton> getOtherEnumButtonsInView() {

        List<EnumButton> l = new ArrayList<EnumButton>();

        for (EnumButton button : allButtons) {
            if ( button.getGroup() == this.group && button.getInflatedParentView().equals(this.getInflatedParentView()))
                l.add(button);
        }
        return l;
    }

    public void clearOtherEnumButtonsInView() {
        List<EnumButton> l = getOtherEnumButtonsInView();
        for (EnumButton button : l) {
            button.setDisableListener(true);
            button.setChecked(false);
            button.setDisableListener(false);
        }
    }

    private static void addButtonToGroup(EnumButton button){
        List<EnumButton> buttonGroup = EnumButton.allButtonsMap.get(button.getGroup());
        if(buttonGroup == null) {
            buttonGroup = new ArrayList<EnumButton>();
            EnumButton.allButtonsMap.put(button.getGroup(), buttonGroup);
        }
        buttonGroup.add(button);
    }

    public EnumButton(Context ctx, int group) {
        super(ctx);
        context = ctx;

        super.setOnCheckedChangeListener(this);

        EnumButton.allButtons.add(this);
        EnumButton.addButtonToGroup(this);

        this.setGroup(group);

    }

    public EnumButton(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        context = ctx;

        super.setOnCheckedChangeListener(this);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EnumButton);
        int group = a.getResourceId(R.styleable.EnumButton_group, NO_VALUE);

        EnumButton.allButtons.add(this);
        EnumButton.addButtonToGroup(this);

        this.setGroup(group);

        a.recycle();
    }

    public EnumButton(Context ctx, AttributeSet attrs, int defStyle) {
        super(ctx, attrs);
        context = ctx;
        super.setOnCheckedChangeListener(this);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EnumButton);
        int group = a.getResourceId(R.styleable.EnumButton_group, NO_VALUE);

        EnumButton.allButtons.add(this);
        EnumButton.addButtonToGroup(this);

        this.setGroup(group);

        a.recycle();
    }

    public void setDisableListener(boolean d) {
        disableListener = d;
    }

    private ViewParent getPage() {
        if (this.page != null)
            return this.page;

        ViewParent v = this.getParent();

        while (v.getParent() != null && !(v.getParent() instanceof ViewPager)) {
            v = v.getParent();
        }
        this.page = v;
        return v;
    }

    public ViewParent getInflatedParentView() {
        if (inflatedParentView == null)
            inflatedParentView = getPage();
        return inflatedParentView;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (disableListener)
            return;
        boolean somethingChecked = false;
        List<EnumButton> buttons = EnumButton.getButtons(this.getPage(), this.getGroup());
        for (EnumButton button : buttons) {
            // we need to make sure at least one in the group is checked
            if (button.isChecked()) {
                somethingChecked = true;
                // and we need to uncheck everything else
                if (!button.equals(this) && isChecked)
                    button.setChecked(false);
            }

        }
        // probably tried to uncheck a checked button
        if (!somethingChecked)
            this.setChecked(true);

        if (listener != null && isChecked)
            listener.onCheckedChanged(buttonView, isChecked);
    }

    @Override
    public void setOnCheckedChangeListener(OnCheckedChangeListener l) {
        this.listener = l;
    }

    /**
     * @return the group
     */
    public int getGroup() {
        return group;
    }

    /**
     * @param group
     *            the group to set
     */
    public void setGroup(int group) {
        this.group = group;
    }

}