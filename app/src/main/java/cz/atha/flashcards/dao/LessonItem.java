package cz.atha.flashcards.dao;

import android.provider.BaseColumns;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = LessonItem.TABLE_NAME)
public class LessonItem {

    private static final String LOGTAG = "LESSON_ITEM";

    /**
     * The name of the table.
     */
    public static final String TABLE_NAME = "lesson_items";

    /**
     * The name of the ID column.
     */
    public static final String COLUMN_ID = BaseColumns._ID;

    @ColumnInfo(index = true)
    public String src;

    /**
     * The name of the name column.
     */
    public static final String COLUMN_LESSON_ID = "lesson_id";

    /**
     * what
     */
    public static final String COLUMN_ITEM_TABLE = "item_table";

    public static final String COLUMN_ITEM_ID = "item_id";


    /**
     * The unique ID o
     */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = COLUMN_ID)
    public long id;

    @ColumnInfo(index = true, name = COLUMN_LESSON_ID)
    public long lessonId;

    @ColumnInfo(name = COLUMN_ITEM_TABLE)
    public String itemTable;

    @ColumnInfo(index = true, name = COLUMN_ITEM_ID)
    public long lessonItemId;

}

