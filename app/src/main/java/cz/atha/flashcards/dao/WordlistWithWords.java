package cz.atha.flashcards.dao;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class WordlistWithWords {

    @Embedded
    public Wordlist wordlist;


    @Relation(parentColumn = Wordlist.COLUMN_ID,
            entityColumn = Word.COLUMN_WORDLIST_ID,
            entity = Word.class)

    public List<Word> words;
}
