package cz.atha.flashcards.dao;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

public class AppProvider extends ContentProvider {

    static final String PROVIDER_NAME = "cz.atha.flashcards.dao.AppProvider";
    static final String URL = "content://" + PROVIDER_NAME + "/articles";
    static final Uri CONTENT_URI = Uri.parse(URL);

    static final String ID = "id";
    static final String title = "title";
    static final String content = "content";

    static final int ARTICLES = 1;
    static final int ARTICLES_ID = 2;
    static final int WORDLISTS = 3;
    static final int WORDLISTS_ID = 4;
    static final int WORDLISTS_WORDS = 5;

    static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        uriMatcher.addURI(PROVIDER_NAME, "articles", ARTICLES);
        uriMatcher.addURI(PROVIDER_NAME, "articles/#", ARTICLES_ID);
        uriMatcher.addURI(PROVIDER_NAME, "wordlists", WORDLISTS);
        uriMatcher.addURI(PROVIDER_NAME, "wordlists/#", WORDLISTS_ID);
        uriMatcher.addURI(PROVIDER_NAME, "wordlists/#/words", WORDLISTS_WORDS);
    }

    private static HashMap<String, String> values;
    static final String DATABASE_NAME = "flashcardDB";
    static final String TABLE_ARTICLE = "articleTable";
    static final int DATABASE_VERSION = 1;

    private SQLiteDatabase db;
    SQLiteOpenHelper dbHelper;

    class DBHelper extends SQLiteOpenHelper {

     public DBHelper (Context context){
         super(context, DATABASE_NAME, null, DATABASE_VERSION);
     }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(" CREATE TABLE " + TABLE_ARTICLE +
                    " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    " title TEXT NOT NULL, " +
                    " content TEXT NOT NULL);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLE );
            onCreate(db);
        }
    };

    @Override
    public boolean onCreate() {

        dbHelper = new DBHelper(getContext());
        // permissions to be writable
        db = dbHelper.getWritableDatabase();

        return db != null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder) {

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TABLE_ARTICLE);

        switch (uriMatcher.match(uri)) {
            case ARTICLES:
                qb.setProjectionMap(values);
                break;
            case ARTICLES_ID:
                qb.appendWhere( ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (sortOrder == null || sortOrder == "") {
            sortOrder = ID;
        }
        Cursor c = qb.query(db, projection, selection, selectionArgs, null,
                null, sortOrder);

        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)){
            // Get all records
            case ARTICLES:
                return "vnd.android.cursor.dir/articles";
            // Get a particular record
            case ARTICLES_ID:
                return "vnd.android.cursor.item/articles";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        long row = db.insert(TABLE_ARTICLE, "", values);

        // If record is added successfully
        if(row > 0) {
            Uri newUri = ContentUris.withAppendedId(CONTENT_URI, row);
            getContext().getContentResolver().notifyChange(newUri, null);
            return newUri;
        }
        throw new SQLException("Fail to add a new record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case ARTICLES:
                // delete all the records of the table
                count = db.delete(TABLE_ARTICLE, selection, selectionArgs);
                break;
            case ARTICLES_ID:
                String id = uri.getLastPathSegment(); //gets the id
                count = db.delete( TABLE_ARTICLE, ID +  " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;

        switch (uriMatcher.match(uri)){
            case ARTICLES:
                count = db.update(TABLE_ARTICLE, values, selection, selectionArgs);
                break;
            case ARTICLES_ID:
                count = db.update(TABLE_ARTICLE, values, ID +
                        " = " + uri.getLastPathSegment() +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI " + uri );
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
