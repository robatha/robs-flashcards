package cz.atha.flashcards.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ModelAdjectiveFormDao {

    @Query("SELECT COUNT(*) FROM " + ModelAdjectiveForm.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(ModelAdjectiveForm form);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(ModelAdjectiveForm[] forms);

    @Query("SELECT * FROM " + ModelAdjectiveForm.TABLE_NAME)
    List<ModelAdjectiveForm> selectAll();

    @Query("SELECT * FROM " + ModelAdjectiveForm.TABLE_NAME + " WHERE " + ModelAdjectiveForm.COLUMN_ID + " = :formId")
    ModelAdjectiveForm selectById(long formId);

    @Query("DELETE FROM " + ModelAdjectiveForm.TABLE_NAME + " WHERE " + ModelAdjectiveForm.COLUMN_ID + " = :id")
    int deleteById(long id);

    @Update
    int update(ModelAdjectiveForm form);
}
