package cz.atha.flashcards.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ModelVerbFormDao {

    @Query("SELECT COUNT(*) FROM " + ModelVerbForm.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(ModelVerbForm form);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(ModelVerbForm[] forms);

    @Query("SELECT * FROM " + ModelVerbForm.TABLE_NAME)
    List<ModelVerbForm> selectAll();

    @Query("SELECT * FROM " + ModelVerbForm.TABLE_NAME + " WHERE " + ModelVerbForm.COLUMN_ID + " = :formId")
    ModelVerbForm selectById(long formId);

    @Query("DELETE FROM " + ModelVerbForm.TABLE_NAME + " WHERE " + ModelVerbForm.COLUMN_ID + " = :id")
    int deleteById(long id);

    @Update
    int update(ModelVerbForm form);
}
