package cz.atha.flashcards.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LessonDao {

    @Query("SELECT COUNT(*) FROM " + Lesson.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Lesson lesson);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(Lesson[] lessons);

    @Query("SELECT * FROM " + Lesson.TABLE_NAME)
    List<Lesson> selectAll();

    @Query("SELECT * FROM " + Lesson.TABLE_NAME + " WHERE " + Lesson.COLUMN_ID + " = :lessonId")
    Lesson selectById(long lessonId);

    @Query("DELETE FROM " + Lesson.TABLE_NAME + " WHERE " + Lesson.COLUMN_ID + " = :id")
    int deleteById(long id);

    @Transaction
    @Query("SELECT * FROM " + Lesson.TABLE_NAME + " WHERE " + Lesson.COLUMN_ID + " = :lessonId")
    public LessonWithLessonItems selectLessonWithItems(long lessonId);

    @Update
    int update(Lesson lesson);
}
