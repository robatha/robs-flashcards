package cz.atha.flashcards.dao;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.atha.flashcards.grammar.models.ModelWithForms;

@Database(entities = {Article.class, Wordlist.class, Word.class, Lesson.class, LessonItem.class,
        Model.class, ModelAdjectiveForm.class, ModelNounForm.class, ModelVerbForm.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    final private static String LOGTAG = "AppDatabase";

    @SuppressWarnings("WeakerAccess")
    public abstract ArticleDao article();

    public abstract WordlistDao wordlist();

    public abstract WordDao word();

    public abstract LessonDao lesson();

    public abstract LessonItemDao lessonItem();

    public abstract ModelDao model();

    public abstract ModelAdjectiveFormDao modelAdjectiveForm();

    public abstract ModelNounFormDao modelNounForm();

    public abstract ModelVerbFormDao modelVerbForm();

    private static AppDatabase me;


    public static synchronized AppDatabase getInstance(Context context) {
        if (me == null) {
            me = Room
                    .databaseBuilder(context.getApplicationContext(), AppDatabase.class, "ex")
                    .build();
        }
        return me;
    }

    public static void switchToInMemory(Context context) {
        me = Room.inMemoryDatabaseBuilder(context.getApplicationContext(), AppDatabase.class).build();
    }

    public void populateInitialData(File dir) {

        File grammarDir = new File(dir, "grammar");
        File vocabDir = new File(dir, "wordlists");
        File lessonDir = new File(dir, "lessons");
        File commonDir = new File(dir, "common");
        File modelDir = new File(commonDir, "models");
        File adjectiveModelDir = new File(modelDir, "adjective");
        File nounModelDir = new File(modelDir, "noun");
        File verbModelDir = new File(modelDir, "verb");

        Map<String,Long>lessonItemSources=new HashMap<>();

 //       AppRepository repository = AppRepository.getInstance()
        runInTransaction(new Runnable() {
            @Override
            public void run() {
                if (article().count() == 0) {
                    for (Article article : Article.getInitialData(grammarDir)) {
                        long id = article().insert(article);
                        File tmp = new File(article.src);
                        if (lessonItemSources.containsKey(tmp)) {
                            Log.w(LOGTAG, "Duplicate filename for lessonItem: " + tmp);
                        } else
                            lessonItemSources.put(tmp.getName(), new Long(id));
                    }
                }

                if (wordlist().count() == 0) {
                    List<WordlistWithWords> listOfLists = Wordlist.getInitialData(vocabDir);
                    if (listOfLists != null)
                    for (WordlistWithWords wordlistWithWords : listOfLists) {
                        long id = wordlist().insert(wordlistWithWords.wordlist);
                        for (Word word : wordlistWithWords.words) {
                            word.wordlistId=id;
                            word().insert(word);
                        }
                        File tmp = new File(wordlistWithWords.wordlist.src);
                        if(lessonItemSources.containsKey(tmp)){
                            Log.w(LOGTAG, "Duplicate filename for lessonItem: "+tmp);
                        }else
                            lessonItemSources.put(tmp.getName(),new Long(id));
                    }
                }

                if(lesson().count() == 0){
                    for(LessonWithLessonItems lessonWithLessonItems : Lesson.getInitialData(lessonDir)){
                        long id = lesson().insert(lessonWithLessonItems.lesson);
                        for(LessonItem item : lessonWithLessonItems.lessonItems){
                            item.lessonId=id;
                            if(!lessonItemSources.containsKey(item.src)) {
                                Log.e(LOGTAG, "Item doesn't exist when loading lessons into db: "+item.src+", in "+lessonWithLessonItems.lesson.src);
                            } else {
                                long itemId = lessonItemSources.get(item.src);
                                item.lessonItemId = itemId;
                                lessonItem().insert(item);
                            }
                        }
                    }
                }

                if(model().count() == 0){

                    for(ModelWithForms<ModelAdjectiveForm> modelWithForms : Model.getInitialData(adjectiveModelDir)) {
                        long id = model().insert(modelWithForms.getModel());
                        for (ModelAdjectiveForm form : modelWithForms.getForms()) {
                            form.modelId = id;
                            modelAdjectiveForm().insert(form);
                        }
                    }
                    for(ModelWithForms<ModelNounForm> modelWithForms : Model.getInitialData(nounModelDir)) {
                        long id = model().insert(modelWithForms.getModel());
                        for (ModelNounForm form : modelWithForms.getForms()) {
                            form.modelId = id;
                            modelNounForm().insert(form);
                        }
                    }
                    for(ModelWithForms<ModelVerbForm> modelWithForms : Model.getInitialData(verbModelDir)) {
                        long id = model().insert(modelWithForms.getModel());
                        for (ModelVerbForm form : modelWithForms.getForms()) {
                            form.modelId = id;
                            modelVerbForm().insert(form);
                        }
                    }
                }

            }
        });

    }

}
