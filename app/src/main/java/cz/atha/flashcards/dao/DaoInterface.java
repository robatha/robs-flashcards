package cz.atha.flashcards.dao;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;

public interface DaoInterface {



    static File getDatadir(Context ctx){
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "data");
//        ContextWrapper ctxWrapper = new ContextWrapper(ctx);
//        return  new File(ctxWrapper.getFilesDir(), "activity_data.csv");
    }


    boolean onCreate();

    @Nullable
    Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) ;

    @Nullable
    String getType(@NonNull Uri uri);

    @Nullable
    Uri insert(@NonNull Uri uri, @Nullable ContentValues values);

    int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs);

    int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs);

}
