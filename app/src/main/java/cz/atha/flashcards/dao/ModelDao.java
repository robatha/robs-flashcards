package cz.atha.flashcards.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ModelDao {

    @Query("SELECT COUNT(*) FROM " + Model.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Model models);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(Model[] models);

    @Query("SELECT * FROM " + Model.TABLE_NAME)
    List<Model> selectAll();

    @Query("SELECT * FROM " + Model.TABLE_NAME + " WHERE " + Model.COLUMN_ID + " = :modelId")
    Model selectById(long modelId);

    @Query("SELECT * FROM " + Model.TABLE_NAME + " WHERE " + Model.COLUMN_SRC + " = :src")
    Model selectBySrc(String src);

    @Query("DELETE FROM " + Model.TABLE_NAME + " WHERE " + Model.COLUMN_ID + " = :id")
    int deleteById(long id);

    @Transaction
    @Query("SELECT * FROM " + Model.TABLE_NAME + " WHERE " + Model.COLUMN_PART_OF_SPEECH + " = 'verb'")
    List<ModelWithVerbForms> selectVerbModels();

    @Transaction
    @Query("SELECT * FROM " + Model.TABLE_NAME + " WHERE " + Model.COLUMN_PART_OF_SPEECH + " = 'noun'")
    List<ModelWithNounForms> selectNounModels();

    @Transaction
    @Query("SELECT * FROM " + Model.TABLE_NAME + " WHERE " + Model.COLUMN_PART_OF_SPEECH + " = 'adjective'")
    List<ModelWithAdjectiveForms> selectAdjectiveModels();


    @Update
    int update(Model wordList);
}
