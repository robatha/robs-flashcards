package cz.atha.flashcards.dao;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class LessonWithLessonItems {

    @Embedded
    public Lesson lesson;


    @Relation(parentColumn = Lesson.COLUMN_ID,
            entityColumn = LessonItem.COLUMN_LESSON_ID,
            entity = LessonItem.class)

    public List<LessonItem> lessonItems;
}
