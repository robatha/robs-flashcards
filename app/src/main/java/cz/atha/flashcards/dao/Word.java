package cz.atha.flashcards.dao;

import android.provider.BaseColumns;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = Word.TABLE_NAME)
public class Word {

    /**
     * The name of the table.
     */
    public static final String TABLE_NAME = "words";

    /**
     * The name of the ID column.
     */
    public static final String COLUMN_ID = BaseColumns._ID;

    /**
     * Relative location of source file
     */
    public static final String COLUMN_SRC = "src";

    public static final String COLUMN_WORDLIST_ID = "wordlistId";
    /**
     * Word in target language
     */
    public static final String COLUMN_TARGET = "target";

    /**
     * Meaning of word
     */
    public static final String COLUMN_MEANING = "meaning";

    /**
     * Image resource filename
     */
    public static final String COLUMN_IMAGE = "image";

    public static final String COLUMN_PARTOFSPEECH = "part_of_speech";

    /**
     * The unique ID oWordlistDao
     */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = COLUMN_ID)
    public long id;

    @ColumnInfo(index = true, name = COLUMN_WORDLIST_ID)
    public long wordlistId;

    @ColumnInfo(index = true, name = COLUMN_SRC)
    public String src;

    @ColumnInfo(name = COLUMN_PARTOFSPEECH)
    public String partOfSpeech;

    public String target;

    public String meaning;

    public String image;

    public String audio;

    @ColumnInfo(defaultValue = "unknown")
    public String animate;

    @ColumnInfo(defaultValue = "unknown")
    public String gender;

    public String extended_form;

    public String aspect;

    public String model;

}
