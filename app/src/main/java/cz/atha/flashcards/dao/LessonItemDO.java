package cz.atha.flashcards.dao;

import java.util.ArrayList;
import java.util.List;

public interface LessonItemDO {

    String getName();

    long getId();
}
