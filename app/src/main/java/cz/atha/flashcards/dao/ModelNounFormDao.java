package cz.atha.flashcards.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ModelNounFormDao {

    @Query("SELECT COUNT(*) FROM " + ModelNounForm.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(ModelNounForm form);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(ModelNounForm[] forms);

    @Query("SELECT * FROM " + ModelNounForm.TABLE_NAME)
    List<ModelNounForm> selectAll();

    @Query("SELECT * FROM " + ModelNounForm.TABLE_NAME + " WHERE " + ModelNounForm.COLUMN_ID + " = :formId")
    ModelNounForm selectById(long formId);

    @Query("DELETE FROM " + ModelNounForm.TABLE_NAME + " WHERE " + ModelNounForm.COLUMN_ID + " = :id")
    int deleteById(long id);

    @Update
    int update(ModelNounForm form);
}
