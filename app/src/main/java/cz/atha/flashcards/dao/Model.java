package cz.atha.flashcards.dao;

import android.provider.BaseColumns;
import android.util.Log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import cz.atha.flashcards.grammar.models.ModelWithForms;
import cz.atha.flashcards.lib.util.CSVReader;

@Entity(tableName = Model.TABLE_NAME)
public class Model {

    private static final String LOGTAG = "MODEL";

    /**
     * The name of the table.
     */
    public static final String TABLE_NAME = "models";

    /**
     * The name of the ID column.
     */
    public static final String COLUMN_ID = BaseColumns._ID;

    /**
     * Relative location of source file
     */
    public static final String COLUMN_SRC = "src";

    /**
     * The name of the name column.
     */
    public static final String COLUMN_NAME = "name";

    /**
     * The name of the name column.
     */
    public static final String COLUMN_PART_OF_SPEECH = "part_of_speech";

    /**
     * actively studying this article?
     */
    public static final String COLUMN_REGEX = "regex";

    /**
     * The unique ID o
     */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = COLUMN_ID)
    public long id;

    @ColumnInfo(index = true, name = COLUMN_SRC)
    public String src;

    @ColumnInfo(name = COLUMN_NAME)
    public String name;

    @ColumnInfo(name = COLUMN_PART_OF_SPEECH)
    public String partOfSpeach;

    @ColumnInfo(name = COLUMN_REGEX)
    public String regex;


    // TODO get word/wordlists from data files!
    public static List<ModelWithForms> getInitialData(File dir) {

        if (!dir.exists()) {
            Log.e(LOGTAG, "Dir doesn't exist in getInitialDataFile(File): " + dir.getAbsolutePath());
            return null;
        }
        List<ModelWithForms> ret = new ArrayList<>();

        for (File file : dir.listFiles()) {
            if (!file.getName().endsWith("csv"))
                continue;

            Model model = null;
            ModelWithForms modelWithForms = null;
            HashMap<String, String[]> recordTypes = new HashMap<>();

            try {
                CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(file)), ';', '"', 0);

                // read in lines setting metadata and words
                String[] fields;
                while ((fields = reader.readNext()) != null) {
                    // blank field
                    if (fields.length == 1 && fields[0].trim().length() == 0)
                        continue;

                    // if it starts with a #, this is a comment
                    if (fields[0].startsWith("#"))
                        continue;

                    // only key?
                    if (fields.length < 2) {
                        Log.w(LOGTAG, String.format("encountered record with only 1 field - %s/%s", file.getAbsolutePath(), fields[0]));
                        continue;
                    }

                    String recordType = fields[0];
                    String[] fieldList = Arrays.copyOfRange(fields, 1, fields.length);

                    if (recordType.equalsIgnoreCase("Flashcard")) {
                        continue;
                    } else if (!recordTypes.containsKey(recordType)) {
                        recordTypes.put(recordType, fieldList);
                        continue;
                    } else if (recordType.equalsIgnoreCase("model")) {
                        model = new Model();
                        model.src = file.getAbsolutePath();

                        for (int i = 0; i < fieldList.length; i++) {
                            String value = fieldList[i];
                            String key = recordTypes.get(recordType)[i];
                            switch (key) {
                                case "type":
                                    if (value.equalsIgnoreCase("adjective")) {
                                        modelWithForms = new ModelWithAdjectiveForms();
                                        model.partOfSpeach = "adjective";
                                    }
                                    if (value.equalsIgnoreCase("noun")) {
                                        modelWithForms = new ModelWithNounForms();
                                        model.partOfSpeach = "noun";
                                    }
                                    if (value.equalsIgnoreCase("verb")) {
                                        modelWithForms = new ModelWithVerbForms();
                                        model.partOfSpeach = "verb";
                                    }
                                    break;
                                case "name":
                                    model.name = value;
                                case "regex":
                                    model.regex = value;
                            }
                        }

                        modelWithForms.setModel(model);

                    } else if (recordType.equalsIgnoreCase("form")) {

                        switch(model.partOfSpeach){
                            case "adjective":
                                ModelAdjectiveForm aForm = new ModelAdjectiveForm(recordTypes.get("form"), fieldList);
                                modelWithForms.addForm(aForm);
                                break;
                            case "noun":
                                ModelNounForm nForm = new ModelNounForm(recordTypes.get("form"), fieldList);
                                modelWithForms.addForm(nForm);
                                break;
                            case "verb":
                                ModelVerbForm vForm = new ModelVerbForm(recordTypes.get("form"), fieldList);
                                modelWithForms.addForm(vForm);
                                break;
                        }

                    }
                }

            } catch (FileNotFoundException e) {
                Log.e(LOGTAG, "File not found, problem parsing model file");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(LOGTAG, "IO exception, problem parsing model file");
                e.printStackTrace();
            }
            ret.add(modelWithForms);
        }
        return ret;
    }

}

