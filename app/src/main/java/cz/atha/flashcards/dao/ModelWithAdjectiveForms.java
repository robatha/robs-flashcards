package cz.atha.flashcards.dao;

import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.Relation;

import java.util.ArrayList;
import java.util.List;

import cz.atha.flashcards.grammar.models.ModelWithForms;

public class ModelWithAdjectiveForms implements ModelWithForms<ModelAdjectiveForm> {

    @Embedded
    public Model model;

    @Relation(parentColumn = Model.COLUMN_ID,
            entityColumn = ModelAdjectiveForm.COLUMN_MODEL_ID,
            entity = ModelAdjectiveForm.class)

    public List<ModelAdjectiveForm> forms = new ArrayList<>();


    @Override
    public void setModel(Model m) {
        model = m;
    }

    @Ignore
    @Override
    public void addForm(ModelAdjectiveForm f) {
        forms.add(f);
    }

    @Ignore
    @Override
    public Model getModel() {
        return model;
    }

    @Ignore
    @Override
    public List<ModelAdjectiveForm> getForms() {
        return forms;
    }
}
