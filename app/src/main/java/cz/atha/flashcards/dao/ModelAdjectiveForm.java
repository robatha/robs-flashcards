package cz.atha.flashcards.dao;

import android.provider.BaseColumns;
import android.util.Log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = ModelAdjectiveForm.TABLE_NAME)
public class ModelAdjectiveForm {
    @Ignore
    private final String LOGTAG = " ModelAdjectiveForm";

    public static final String TABLE_NAME = "model_adjective_form";

    public static final String COLUMN_ID = BaseColumns._ID;

    public static final String COLUMN_SRC = "src";

    public static final String COLUMN_MODEL_ID = "model_id";

    public static final String COLUMN_GENDER = "gender";

    public static final String COLUMN_ANIMATE = "animate";

    public static final String COLUMN_CASE = "caze";

    public static final String COLUMN_NUMBER = "number";

    public static final String COLUMN_REPLACE_STRING = "replace_string";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = COLUMN_ID)
    public long id;

    @ColumnInfo(index = true, name = COLUMN_MODEL_ID)
    public long modelId;

    @ColumnInfo(index = true, name = COLUMN_SRC)
    public String src;

    @ColumnInfo(name = COLUMN_GENDER)
    public String gender;

    @ColumnInfo(name = COLUMN_ANIMATE, defaultValue="false")
    public String animate;

    @ColumnInfo(name = COLUMN_CASE)
    public String caze;

    @ColumnInfo(name = COLUMN_NUMBER)
    public String number;

    @ColumnInfo(name = COLUMN_REPLACE_STRING)
    public String replaceString;


    public ModelAdjectiveForm(){

    }

    @Ignore
    public ModelAdjectiveForm(String[] keys, String[] values)
    {

//        ModelAdjectiveForm form = new ModelAdjectiveForm();

        for (int i = 0; i < keys.length; i++) {
            if (keys.length < i) {
                Log.w(LOGTAG, String.format("Only one key in file?"));
                continue;
            }
            String key = keys[i];
            if (i >= keys.length) {
                Log.w(LOGTAG, String.format("length=%d; index=%d", values.length, i));
                continue;
            }

            String value = values[i];
            switch (key) {
                case "gender":
                    this.gender = value;
                    break;
                case "animate":
                    this.animate = value;
                    break;
                case "case":
                    this.caze = value;
                    break;
                case "number":
                    this.number = value;
                    break;
                case "replace_string":
                    this.replaceString = value;
                    break;
                default:
                    Log.w(LOGTAG, String.format("Unknown field: %s=%s", key, value));
            }
        }

    }

}

