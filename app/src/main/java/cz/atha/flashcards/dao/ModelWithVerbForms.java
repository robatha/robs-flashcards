package cz.atha.flashcards.dao;

import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.Relation;

import java.util.ArrayList;
import java.util.List;

import cz.atha.flashcards.grammar.models.ModelWithForms;

public class ModelWithVerbForms implements ModelWithForms<ModelVerbForm> {

    @Embedded
    public Model model;

    @Relation(parentColumn = Model.COLUMN_ID,
            entityColumn = ModelVerbForm.COLUMN_MODEL_ID,
            entity = ModelVerbForm.class)

    public List<ModelVerbForm> forms = new ArrayList<>();

    @Ignore
    @Override
    public void setModel(Model m) {
        model = m;
    }

    @Ignore
    @Override
    public void addForm(ModelVerbForm f) {
        forms.add(f);
    }

    @Ignore
    @Override
    public Model getModel() {
        return model;
    }

    @Ignore
    @Override
    public List<ModelVerbForm> getForms() {
        return forms;
    }

}
