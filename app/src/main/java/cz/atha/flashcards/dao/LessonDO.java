package cz.atha.flashcards.dao;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public class LessonDO implements Comparable<LessonDO>{

	private long id;
	private String key;
	private String seq;
	private String name;
	private String article;
	private String icon;
	private ArrayList<LessonItemDO> lessonItems;

	@Deprecated
	private Bitmap imageBitmap;

	public LessonDO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKey(){
		return this.key;
	}

	public void setKey(String k){
		this.key = k;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public ArrayList<LessonItemDO> getLessonItems() {
		return lessonItems;
	}

	public void setLessonItems(ArrayList<LessonItemDO> lessonItems) {
		this.lessonItems = lessonItems;
	}

	/**
	 * @return the image
	 */
	@Deprecated
	public Bitmap getImageBitmap() {
		return imageBitmap;
	}

	/**
	 * @param bitmap
	 *            the type to set
	 */
	@Deprecated
	public void setIimageBitmap(Bitmap bitmap) {
		this.imageBitmap = bitmap;
	}


	@Override
	public String toString() {
		return "LessonDO [name=" + name + ", key=" + key + "]";
	}

	public static LessonDO toLessonDO(Lesson lesson){
		LessonDO obj = new LessonDO();
		obj.id = lesson.id;
		obj.key = lesson.src;
		obj.seq = lesson.seq;
		obj.name = lesson.name;
		obj.icon = lesson.image;
		obj.article = lesson.article;
		obj.lessonItems = new ArrayList<>();

		return obj;
	}

	public static List<LessonDO> toLessonDO(List<Lesson> lessons){

		List<LessonDO>list = new ArrayList<>();
		for(Lesson lesson : lessons){
			list.add(toLessonDO(lesson));
		}

		return list;
	}

	@Override
	public int compareTo(LessonDO o) {
		return (seq+name).compareTo(o.seq+o.name);
	}
}
