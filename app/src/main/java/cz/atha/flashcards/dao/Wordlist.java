package cz.atha.flashcards.dao;

import android.provider.BaseColumns;
import android.util.Log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import cz.atha.flashcards.lib.util.CSVReader;

@Entity(tableName = Wordlist.TABLE_NAME)
public class Wordlist {

    private static final String LOGTAG = "WORDLIST";

    /**
     * The name of the table.
     */
    public static final String TABLE_NAME = "wordlists";

    /**
     * The name of the ID column.
     */
    public static final String COLUMN_ID = BaseColumns._ID;

    /**
     * Relative location of source file
     */
    public static final String COLUMN_SRC = "src";

    /**
     * The name of the name column.
     */
    public static final String COLUMN_NAME = "name";

    /**
     * The name of the name column.
     */
    public static final String COLUMN_DESC = "desc";

    /**
     * actively studying this article?
     */
    public static final String COLUMN_ACTIVE = "active";

    /**
     * The unique ID o
     */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = COLUMN_ID)
    public long id;

    @ColumnInfo(index = true, name = COLUMN_SRC)
    public String src;

    @ColumnInfo(name = COLUMN_NAME)
    public String name;

    @ColumnInfo(name = COLUMN_DESC)
    public String desc;

    @ColumnInfo(name = COLUMN_ACTIVE, defaultValue = "false")
    public boolean active;

    // TODO get word/wordlists from data files!
    public static List<WordlistWithWords> getInitialData(File dir) {

        if (!dir.exists()) {
            Log.e(LOGTAG, "Dir doesn't exist in getInitialDataFile(File): " + dir.getAbsolutePath());
            return null;
        }
        List<WordlistWithWords> ret = new ArrayList<>();

        for (File file : dir.listFiles()) {
            if(file.isDirectory())
                ret.addAll(getInitialData(file));
            if (!file.getName().endsWith("csv"))
                continue;

            WordlistWithWords wlistwwords = new WordlistWithWords();
            wlistwwords.words = new ArrayList<>();
            wlistwwords.wordlist = new Wordlist();

            wlistwwords.wordlist.src = file.getAbsolutePath();
            wlistwwords.wordlist.active = false;
            wlistwwords.wordlist.desc = "";
            HashMap<String, String[]> recordTypes = new HashMap<>();
            try {
                CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(file)), ';', '"', 0);

                // read in lines setting metadata and words
                String[] fields;
                while ((fields = reader.readNext()) != null) {
                    // blank field
                    if (fields.length == 1 && fields[0].trim().length() == 0)
                        continue;
                    // only key
                    if(fields.length < 2){
                        Log.w(LOGTAG, String.format("encountered record with only 1 field - %s/%s",file.getAbsolutePath(),fields[0]));
                        continue;
                    }

                    String recordType = fields[0];
                    String[] fieldList = Arrays.copyOfRange(fields, 1, fields.length);
                    if (recordType.equalsIgnoreCase("Flashcard") && fields.length >= 4){
                        wlistwwords.wordlist.name = fields[3];
                        continue;
                    }
                    else if (!recordTypes.containsKey(recordType)){
                        recordTypes.put(recordType,fieldList);
                        continue;
                    }

                    String[] fieldTypes = recordTypes.get(recordType);
                    if(recordType.equalsIgnoreCase("wordlist")){
                        for (int i = 0; i < fieldTypes.length; i++) {
                            String key = fieldTypes[i];
                            String value = fieldList[i];
                            switch (key) {
                                case "name":
                                    wlistwwords.wordlist.name=value; break;
                                case "desc":
                                    wlistwwords.wordlist.desc=value; break;
                                default:
                                    Log.w(LOGTAG, String.format("Unknown field: %s/%s=%s",file.getAbsolutePath(),key,value));
                            }
                        }
                        continue;
                    }
                    Word word = new Word();
                    wlistwwords.words.add(word);
                    word.partOfSpeech = recordType;

                    for (int i = 0; i < fieldTypes.length; i++) {
                        if (fieldList.length < i) {
                            Log.w(LOGTAG, "Malformed file" + file.getAbsolutePath());
                            continue;
                        }
                        String key = fieldTypes[i];
                        if (i >= fieldList.length)
                        {
                            Log.e(LOGTAG, String.format("%s length=%d; index=%d; field=%s", file.getAbsolutePath(), fieldList.length, i, fieldTypes[i]));
                            continue;
                        }

                        String value = fieldList[i];

                        switch (key) {
                            case "target":
                                word.target=value; break;
                            case "meaning":
                                word.meaning=value; break;
                            case "image":
                                word.image=value; break;
                            case "audio":
                                word.audio=value; break;
                            case "animate":
                                word.animate=value; break;
                            case "gender":
                                word.gender=value; break;
                            case "extended-form": case "extended_form":
                                word.extended_form=value; break;
                            case "model":
                                word.model=value; break;
                            case "aspect":
                                word.aspect=value; break;
                            default:
                                Log.w(LOGTAG, String.format("Unknown field: %s/%s=%s",file.getAbsolutePath(),key,value));
                        }
                    }


                }

            } catch (FileNotFoundException e) {
                Log.e(LOGTAG, "File not found, problem parsing vcab file");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(LOGTAG, "IO exception, problem parsing vcab file");
                e.printStackTrace();
            }
            ret.add(wlistwwords);
        }
        return ret;
    }

}

