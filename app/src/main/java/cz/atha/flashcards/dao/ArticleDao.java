package cz.atha.flashcards.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ArticleDao {

    @Query("SELECT COUNT(*) FROM " + Article.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Article article);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(Article[] articles);

    @Query("SELECT * FROM " + Article.TABLE_NAME)
    List<Article> selectAll();

    @Query("SELECT * FROM " + Article.TABLE_NAME + " WHERE " + Article.COLUMN_ID + " = :articleId")
    Article selectById(long articleId);

    @Query("SELECT * FROM " + Article.TABLE_NAME + " WHERE " + Article.COLUMN_SRC+ " = :src")
    Article selectBySrc(String src);


    @Query("DELETE FROM " + Article.TABLE_NAME + " WHERE " + Article.COLUMN_ID + " = :id")
    int deleteById(long id);

    @Update
    int update(Article article);

    @Query("UPDATE " + Article.TABLE_NAME + " SET " + Article.COLUMN_ACTIVE + "= :value WHERE " + Article.COLUMN_ID + " = :articleId")
    int setActive(long articleId, boolean value);
}
