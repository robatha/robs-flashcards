package cz.atha.flashcards.dao;

import android.provider.BaseColumns;
import android.util.Log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Entity(tableName = Article.TABLE_NAME)

public class Article implements Comparable<Article> {

    private static final String LOGTAG = "ARTICLE";

    /**
     * The name of the table.
     */
    public static final String TABLE_NAME = "articles";

    /**
     * The name of the ID column.
     */
    public static final String COLUMN_ID = BaseColumns._ID;

    /**
     * Relative location of source file
     */
    public static final String COLUMN_SRC = "src";

    /**
     * The name of the name column.
     */
    public static final String COLUMN_NAME = "name";

    /**
     * The name of the name column.
     */
    public static final String COLUMN_DESC = "desc";

    /**
     * The name of the name column.
     */
    public static final String COLUMN_CONTENT = "content";

    public static final String COLUMN_AUTHOR = "author";
    /**
     * actively studying this article?
     */
    public static final String COLUMN_ACTIVE = "active";

    /**
     * The unique ID o
     */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = COLUMN_ID)
    public long id;

    @ColumnInfo(index = true, name = COLUMN_SRC)
    public String src;

    @ColumnInfo(name = COLUMN_NAME)
    public String name;

    @ColumnInfo(name = COLUMN_DESC)
    public String desc;

    @ColumnInfo(name = COLUMN_CONTENT)
    public String content;

    @ColumnInfo(name = COLUMN_AUTHOR)
    public String author;

    @ColumnInfo(name = COLUMN_ACTIVE, defaultValue = "false")
    public boolean active;

    @Ignore
    @Override
    public int compareTo(Article o) {
        if (!this.active && o.active)
            return 1;

        if (this.active && !o.active)
            return -1;

        return name.compareTo(o.name);
    }

    @Ignore
    public static List<Article> getInitialData(File dir) {

        if (!dir.exists()) {
            Log.e(LOGTAG, "Dir doesn't exist in getInitialDataFile(File): "+dir.getAbsolutePath());
            return null;
        }

        List<Article> articles = new ArrayList<>();
        File[] files = dir.listFiles();
        for (File f : files) {
            Article article = new Article();
            if (!f.getName().endsWith("html")) continue;
            Document document = null;
            // NAME
            article.name = null;
            try {
                Document doc = Jsoup.parse(f, "UTF-8");
                article.name = doc.title();
                Elements metaTags = doc.getElementsByTag("meta");
                for (Element metaTag : metaTags) {
                    String content = metaTag.attr("content");
                    String name = metaTag.attr("name");

                    if("description".equals(name)) {
                        article.desc = content;
                    }
                    if("author".equals(name)) {
                        article.author = content;
                    }
//                    if("keywords".equals(name)) {
//                        something = content
//                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            // SRC
            article.src = f.getAbsolutePath();
            StringBuilder contentBuilder = new StringBuilder();
            try (BufferedReader br = new BufferedReader(new FileReader(f))) {

                String sCurrentLine;
                while ((sCurrentLine = br.readLine()) != null) {
                    contentBuilder.append(sCurrentLine).append("\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            // CONTENT
            article.content = contentBuilder.toString();
            articles.add(article);
        }
        return articles;
    }

    static Article toArticle(ArticleDO article){
        Article obj = new Article();
        obj.id = article.getId();
        obj.src = article.getSrc();
        obj.name = article.getName();
        obj.content = article.getContent();
        obj.active = article.isActive();

        return obj;
    }

    static List<Article> toArticle(List<ArticleDO> articles){
        List<Article> objs = new ArrayList<>();
        for(ArticleDO article : articles)
        {
            Article obj = toArticle(article);
            objs.add(obj);
        }

        return objs;
    }
}
