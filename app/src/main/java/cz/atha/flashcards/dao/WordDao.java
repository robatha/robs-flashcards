package cz.atha.flashcards.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface WordDao {

    @Query("SELECT COUNT(*) FROM " + Word.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Word word);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(Word[] words);

    @Query("SELECT * FROM " + Word.TABLE_NAME)
    List<Word> selectAll();

    @Query("SELECT * FROM " + Word.TABLE_NAME + " WHERE " + Word.COLUMN_ID + " = :wordId")
    Word selectById(long wordId);

    @Query("DELETE FROM " + Word.TABLE_NAME + " WHERE " + Word.COLUMN_ID + " = :id")
    int deleteById(long id);

    @Update
    int update(Word word);
}
