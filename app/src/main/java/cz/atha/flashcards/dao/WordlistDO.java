package cz.atha.flashcards.dao;

import java.util.ArrayList;
import java.util.List;

import cz.atha.flashcards.grammar.WordDO;

public class WordlistDO implements Comparable<WordlistDO>, LessonItemDO {


    private long id;

    private String src;

    private String name;

    private String desc;

    private boolean active;

    private List<WordDO>words;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<WordDO> getWords() {
        return words;
    }

    public void setWords(List<WordDO> words) {
        this.words = words;
    }

    static WordlistDO toWordlistDO(WordlistWithWords wordlistWithWords){

        WordlistDO obj = toWordlistDO(wordlistWithWords.wordlist);
        obj.words = WordDO.toWordDO(wordlistWithWords.words);

        return obj;
    }

    static WordlistDO toWordlistDO(Wordlist wordlist){
        WordlistDO obj = new WordlistDO();
        obj.id = wordlist.id;
        obj.src = wordlist.src;
        obj.name = wordlist.name;
        obj.desc = wordlist.desc;
        obj.active = wordlist.active;
        obj.words = new ArrayList<>();

        return obj;
    }

    static List<WordlistDO> toWordlistDO(List<Wordlist> wordlists){
        List<WordlistDO> objs = new ArrayList<>();
        for(Wordlist wordlist : wordlists)
        {
            WordlistDO obj = toWordlistDO(wordlist);
            objs.add(obj);
        }

        return objs;
    }

    @Override
    public int compareTo(WordlistDO o) {
        if (!this.active && o.active)
            return 1;

        if (this.active && !o.active)
            return -1;

        return name.compareTo(o.name);
    }
}

