package cz.atha.flashcards.dao;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.atha.flashcards.grammar.AdjectiveDO;
import cz.atha.flashcards.grammar.AdjectiveModifierDO;
import cz.atha.flashcards.grammar.Animation;
import cz.atha.flashcards.grammar.Case;
import cz.atha.flashcards.grammar.ExpressionDO;
import cz.atha.flashcards.grammar.Gender;
import cz.atha.flashcards.grammar.NounDO;
import cz.atha.flashcards.grammar.Number;
import cz.atha.flashcards.grammar.PronounDO;
import cz.atha.flashcards.grammar.VerbDO;
import cz.atha.flashcards.grammar.VerbModifierDO;
import cz.atha.flashcards.grammar.WordDO;
import cz.atha.flashcards.grammar.models.AdjectiveModelDO;
import cz.atha.flashcards.grammar.models.AdjectiveModelFormDO;
import cz.atha.flashcards.grammar.models.HasModel;
import cz.atha.flashcards.grammar.models.ModelDO;
import cz.atha.flashcards.grammar.models.NounModelDO;
import cz.atha.flashcards.grammar.models.NounModelFormDO;
import cz.atha.flashcards.grammar.models.VerbModelDO;
import cz.atha.flashcards.grammar.models.VerbModelFormDO;

import static cz.atha.flashcards.dao.DaoInterface.getDatadir;
import static cz.atha.flashcards.dao.LessonDO.toLessonDO;
import static cz.atha.flashcards.dao.Article.toArticle;
import static cz.atha.flashcards.dao.WordlistDO.toWordlistDO;

public class AppRepository {

    private static AppDatabase db;
    private static AppRepository me;
    private static String LOGTAG = "AppRepository";

    private final Executor background = new Executor() {
        @Override
        public void execute(Runnable command) {
            Thread t = new Thread(command);
            t.start();
        }
    };

    // cached data here
    private MutableLiveData<List<ArticleDO>> allArticles;
    private MutableLiveData<ArticleDO> selectedArticle;
    private MutableLiveData<List<WordlistDO>> allWordlists;
    private MutableLiveData<List<WordlistDO>> adjectiveWordlists;
    private MutableLiveData<List<WordlistDO>> nounWordlists;
    private MutableLiveData<List<WordlistDO>> adjectiveAndNounWordlists;
    private MutableLiveData<List<WordlistDO>> verbWordlists;
    private MutableLiveData<WordlistDO> selectedWordlist;
    private MutableLiveData<List<LessonDO>> allLessons;
    private MutableLiveData<LessonDO> selectedLesson;
    private List<AdjectiveModelDO> adjectiveModels;
    private List<NounModelDO> nounModels;
    private List<VerbModelDO> verbModels;

    public static AppRepository getInstance(Context context) {
        if (me == null) {
            me = new AppRepository();
            AppDatabase.switchToInMemory(context);
            db = AppDatabase.getInstance(context);
            new Thread() {
                @Override
                public void run() {
                    db.populateInitialData(getDatadir(context));
                    me.loadModels();
                }
            }.start();
        }
        return me;
    }

    private AppRepository() {

        allArticles = new MutableLiveData<>();
        selectedArticle = new MutableLiveData<>();
        allWordlists = new MutableLiveData<>();
        adjectiveWordlists = new MutableLiveData<>();
        nounWordlists = new MutableLiveData<>();
        adjectiveAndNounWordlists = new MutableLiveData<>();
        verbWordlists = new MutableLiveData<>();
        selectedWordlist = new MutableLiveData<>();
        allLessons = new MutableLiveData<>();
        selectedLesson = new MutableLiveData<>();
        adjectiveModels = new ArrayList<>();
        nounModels = new ArrayList<>();
        verbModels = new ArrayList<>();

    }

    public void insertAllArticles(final List<ArticleDO> articles) {

        final Article[] itemsArray = toArticle(articles).toArray(new Article[0]);

        background.execute(() -> {
            db.article().insertAll(itemsArray);
        });
        // reload articles
        allArticles = (MutableLiveData<List<ArticleDO>>) getAllArticles();
    }

    public void loadArticle(long id) {

        if ((selectedArticle.getValue() == null) || (selectedArticle.getValue().getId() != id)) {
            background.execute(() -> {
                Article article = db.article().selectById(id);
                selectedArticle.postValue(new ArticleDO(article));
            });
        }

    }

    public LiveData<ArticleDO> getArticle() {
        return selectedArticle;
    }

    public LiveData<List<ArticleDO>> getAllArticles() {

        background.execute(() -> {
            List<ArticleDO> articles = new ArrayList<>();
            for(Article article : db.article().selectAll())
            {
                articles.add(new ArticleDO(article));
            }
            allArticles.postValue(articles);

        });

        return allArticles;
    }

    public LiveData<List<WordlistDO>> getWordlistsWithAdjectives() {

        background.execute(() -> {
            adjectiveWordlists.postValue(toWordlistDO(db.wordlist().selectWordlistsWithAdjectives()));
        });
        return adjectiveWordlists;
    }

    public LiveData<List<WordlistDO>> getWordlistsWithNouns() {

        background.execute(() -> {
            nounWordlists.postValue(toWordlistDO(db.wordlist().selectWordlistsWithNouns()));
        });
        return nounWordlists;
    }

    public LiveData<List<WordlistDO>> getWordlistsWithAdjectivesAndNouns() {

        background.execute(() -> {
            adjectiveAndNounWordlists.postValue(toWordlistDO(db.wordlist().selectWordlistsWithAdjectivesAndNouns()));
        });
        return adjectiveAndNounWordlists;
    }

    public LiveData<List<WordlistDO>> getWordlistsWithVerbs() {

        background.execute(() -> {
            verbWordlists.postValue(toWordlistDO(db.wordlist().selectWordlistsWithVerbs()));
        });
        return verbWordlists;
    }


    public LiveData<List<WordlistDO>> getAllWordlists() {

        background.execute(() -> {
            allWordlists.postValue(toWordlistDO(db.wordlist().selectAll()));
        });
        return allWordlists;
    }

    public void setArticleActive(long id, boolean checked) {
        background.execute(() -> {
            Article article = db.article().selectById(id);
            article.active=true;
            db.article().update(article);
        });
    }

    public void setWordlistActive(long id, boolean checked) {
        background.execute(() -> {
            Wordlist wordlist = db.wordlist().selectById(id);
            wordlist.active=checked;
            db.wordlist().update(wordlist);
        });
    }

    public void loadWordlist(long id) {
            background.execute(() -> {
                WordlistWithWords wordlist = db.wordlist().selectWordlistWithWords(id);
                selectedWordlist.postValue(toWordlistDO(wordlist));
            });
    }

    public LiveData<WordlistDO> getSelectedWordlist() {
        return selectedWordlist;
    }

    public void loadVerblist(long id) {
            background.execute(() -> {
                WordlistWithWords wordlist = db.wordlist().selectWordlistWithWords(id);

                // just going to take out non-verbs here.
                WordlistDO wordlistDO = toWordlistDO(wordlist);
                Iterator<WordDO> it = wordlistDO.getWords().iterator();
                while (it.hasNext()){
                    WordDO word = it.next();
                    if(!(word instanceof VerbDO)) it.remove();
                }
                selectedWordlist.postValue(wordlistDO);
            });
    }

    public void loadConjugationWordlist(WordlistDO fromList, VerbModifierDO modifier) {

        background.execute(() -> {
            WordlistWithWords wordlist = db.wordlist().selectWordlistWithWords(fromList.getId());

            // just going to take out non-verbs here.
            WordlistDO wordlistDO = toWordlistDO(wordlist);
            Iterator<WordDO> it = wordlistDO.getWords().iterator();
            while (it.hasNext()){
                WordDO word = it.next();
                if(!(word instanceof VerbDO)) it.remove();
            }

            List<WordDO> list = new ArrayList<>();
            List<WordDO> words = wordlistDO.getWords();

            Random r = new Random();
            for(int i = 0; i < 20; i++){
                PronounDO pronoun = PronounDO.pronouns.get(r.nextInt(PronounDO.pronouns.size()));
                VerbDO verb = (VerbDO)words.get(r.nextInt(words.size()));

                // find and assign model if none
                if (verb.getModel() == null)
                {
                    ModelDO model = getModel(verb);
                    verb.setModel(model);
                }
                // grab conjugated verbs
                String conjugated = verb.getForm(modifier.fromWord(pronoun));
                // add aux verbs, if any
                conjugated = VerbDO.getAuxiliaryVerbs(modifier, verb, conjugated);
                // formulate flashcard
                ExpressionDO word = new ExpressionDO();
                word.setMeaning(String.format("%s / %s", pronoun.getTarget(), verb.getTarget()));
                word.setTarget(conjugated);
                word.setImage(verb.getImage());
                word.setAudio(verb.getAudio());

                list.add(word);
            }

            // i need to return a wordlist because of src for images and potential audio
            WordlistDO newList = new WordlistDO();
            newList.setWords(list);
            newList.setSrc(wordlistDO.getSrc());
            selectedWordlist.postValue(newList);
        });


    }

    // TODO this until I figure out a better way to do this
    List<Gender>genders = new ArrayList();
    {
        genders.add(Gender.male);
        genders.add(Gender.male);
        genders.add(Gender.female);
        genders.add(Gender.neuter);
    }
    List<Animation>animations = new ArrayList();
    {
        animations.add(Animation.animate);
        animations.add(Animation.inanimate);
    }

    // TODO create wordlist to decline!
    public void loadDeclensionWordlist(WordlistDO fromList,
                                       Set<Number> numberSet,
                                       Set<Case> caseSet,
                                       boolean nouns, boolean adjectives) {

        background.execute(() -> {
            WordlistWithWords wordlist = db.wordlist().selectWordlistWithWords(fromList.getId());

            // just going to take out non-nouns/adjs here.
            WordlistDO wordlistDO = toWordlistDO(wordlist);
            Iterator<WordDO> it = wordlistDO.getWords().iterator();
            while (it.hasNext()){
                WordDO word = it.next();
                if(!(word instanceof NounDO || word instanceof AdjectiveDO)) it.remove();
                else if(!nouns && word instanceof NounDO) it.remove();
                else if(!adjectives && word instanceof AdjectiveDO) it.remove();
            }

            List<WordDO> list = new ArrayList<>();
            List<WordDO> words = wordlistDO.getWords();
            List<Case> cases = new ArrayList(caseSet);
            List<Number> numbers = new ArrayList(numberSet);


            // we can use an adjective modifier for a noun if use the noun to set gender/animation
            AdjectiveModifierDO modifier = new AdjectiveModifierDO();

            Random r = new Random();
            for(int i = 0; i < 20; i++){

                HasModel word = (HasModel)words.get(r.nextInt(words.size()));
                // find and assign model if none
                if (word.getModel() == null) {
                    ModelDO model = getModel(word);
                    word.setModel(model);
                }

                // set modifier
                if (word instanceof NounDO){
                    modifier.modify((NounDO)word);
                } else if (word instanceof AdjectiveDO){
                    modifier.setGender(genders.get(r.nextInt(genders.size())));
                    modifier.setAnimate(animations.get(r.nextInt(animations.size())));
                }
                modifier.setCase(cases.get(r.nextInt(cases.size())));
                modifier.setNumber(numbers.get(r.nextInt(numbers.size())));

                // grab declination
                String declined = word.getForm(modifier);
                String genderString = "unknown";
                switch(modifier.getGender()){
                    case female: genderString = "feminine";
                    case neuter: genderString = "neuter";
                    case male:
                        genderString = (modifier.getAnimate() == Animation.animate)
                                ? "masc animate" : "masc inanimate";
                }

                // formulate flashcard
                ExpressionDO flashcard = new ExpressionDO();
                flashcard.setMeaning(String.format("%s / %s / %s",
                        genderString, modifier.getNumber(), modifier.getCase()));
                flashcard.setTarget(declined);
                flashcard.setImage(word.getImage());
                flashcard.setAudio(word.getAudio());

                list.add(flashcard);
            }

            // i need to return a wordlist because of src for images and potential audio
            WordlistDO newList = new WordlistDO();
            newList.setWords(list);
            newList.setSrc(wordlistDO.getSrc());
            selectedWordlist.postValue(newList);
        });


    }

    public LiveData<List<LessonDO>> getAllLessons() {

        background.execute(() -> {
            allLessons.postValue(toLessonDO(db.lesson().selectAll()));
        });
        return allLessons;
    }

    public LiveData<LessonDO> getSelectedLesson() {
        return selectedLesson;
    }

    public void loadLesson(long id) {
        if ((selectedLesson.getValue() == null) || (selectedLesson.getValue().getId() != id)) {
            background.execute(() -> {
                Log.e(LOGTAG, "ID HEREGOT HERE"+id);
                LessonWithLessonItems lesson = db.lesson().selectLessonWithItems(id);
                LessonDO lessonDO = toLessonDO(lesson.lesson);
                for(LessonItem item : lesson.lessonItems){
                    Log.e(LOGTAG, "ID HEREGOT HERE"+item.src);
                    Object obj = null;
                    LessonItemDO itemDO = null;
                    if(item.itemTable.equals(Article.TABLE_NAME)){
                        obj = db.article().selectById(item.lessonItemId);
                        if (obj != null)
                            itemDO = new ArticleDO((Article)obj);
                    } else if (item.itemTable.equals(Wordlist.TABLE_NAME)){
                        obj = db.wordlist().selectById(item.lessonItemId);
                        if (obj != null)
                            itemDO = toWordlistDO((Wordlist) obj);
                    }
                    if (obj != null)
                        lessonDO.getLessonItems().add(itemDO);
                }
                selectedLesson.postValue(lessonDO);
            });
        }
    }

    public ModelDO getModel(HasModel word){
        if (word instanceof AdjectiveDO)
            return getModel((AdjectiveDO)word);
        if (word instanceof NounDO)
            return getModel((NounDO)word);
        if (word instanceof VerbDO)
            return getModel((VerbDO)word);

        return null;
    }

    public AdjectiveModelDO getModel(AdjectiveDO adj){
        String dictionaryForm = adj.getTarget();
        for(AdjectiveModelDO model : getAdjectiveModels()){
            String regex = model.getRegex();
            if(Pattern.matches(regex, dictionaryForm))
                return model;
        }
        return null;
    }

    public NounModelDO getModel(NounDO word){
        String nominativeForm = word.getTarget();

        for(NounModelDO model : getNounModels()){
            String regex = model.getRegex();
            if(Pattern.matches(regex, nominativeForm))
            {
                // for noun we want to make sure at least one of the forms has the same gender
                // as the target verb
                for(NounModelFormDO form : model.getForms()) {
                    Log.d(LOGTAG, "found noun model, searching forms in "+model.getName());
                    Log.d(LOGTAG, "word{"+word.getGender()+","+word.getAnimate()+"}");
                    Log.d(LOGTAG, "form{"+form.getGender()+","+form.getAnimate()+"}");

                    // this is a quick check to make sure we can match any of the model's forms
                    if(form.isCorrectForm(word) < 0 )
                        continue;
                    // once we match at least one of them then we return the model
                    else {
                        Log.d(LOGTAG, "setting model!");
                        return model;
                    }
                }
            }
        }
        return null;
    }

    public VerbModelDO getModel(VerbDO verb){

        String infinitive = verb.getTarget();
        // sometimes we get a verb with a phrase attached to it, in that case we need to peel it off
        // so we can see the infinitive
        int index = verb.getTarget().indexOf(' ');
        if(index > 0) {
            infinitive = verb.getTarget().substring(0, index);
        }

        // look for the model with the highest score
        // score is determeined by # of non variable characters at the end of the pattern that is matched
        // that way we can comfortably match an -ovat when we can instead of an -at
        VerbModelDO matched = null;
        int score = 0;
        for(VerbModelDO model : verbModels){
            String pattern = model.getRegex();
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(infinitive);
            if(m.matches()) {
                int endGroups = 0;
                if (m.groupCount() > 0)
                    endGroups = m.end(m.groupCount());
                int newScore = infinitive.length() - endGroups;
                Log.d(LOGTAG, "model score "+model.getName()+"("+newScore+")");
                if (newScore > score) {
                    matched = model;
                    score = newScore;
                }
            }
        }
        return matched;
    }


    public void loadModels(){

        background.execute(() -> {
            for(ModelWithAdjectiveForms model : db.model().selectAdjectiveModels()){
                AdjectiveModelDO m = new AdjectiveModelDO(model);
                for(ModelAdjectiveForm form : model.getForms()){
                    AdjectiveModelFormDO f = new AdjectiveModelFormDO(form);
                    m.getForms().add(f);
                }
                adjectiveModels.add(m);
            }

            for(ModelWithNounForms model : db.model().selectNounModels()){
                NounModelDO m = new NounModelDO(model);
                for(ModelNounForm form : model.getForms()){
                    NounModelFormDO f = new NounModelFormDO(form);
                    m.getForms().add(f);
                }
                nounModels.add(m);
            }

            for(ModelWithVerbForms model : db.model().selectVerbModels()){
                VerbModelDO m = new VerbModelDO(model);
                for(ModelVerbForm form : model.getForms()){
                    VerbModelFormDO f = new VerbModelFormDO(form);
                    m.getForms().add(f);
                }
                verbModels.add(m);
            }

        });
    }
    public List<AdjectiveModelDO> getAdjectiveModels() {
        return adjectiveModels;
    }
    public List<NounModelDO> getNounModels() {
        return nounModels;
    }

    public List<VerbModelDO> getVerbModels() {
        return verbModels;
    }
}
