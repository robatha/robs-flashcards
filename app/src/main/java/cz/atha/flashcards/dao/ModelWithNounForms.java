package cz.atha.flashcards.dao;

import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.Relation;

import java.util.ArrayList;
import java.util.List;

import cz.atha.flashcards.grammar.models.ModelWithForms;

public class ModelWithNounForms implements ModelWithForms<ModelNounForm> {

    @Embedded
    public Model model;

    @Relation(parentColumn = Model.COLUMN_ID,
            entityColumn = ModelNounForm.COLUMN_MODEL_ID,
            entity = ModelNounForm.class)

    public List<ModelNounForm> forms = new ArrayList<>();

    @Ignore
    @Override
    public void setModel(Model m) {
        model = m;
    }

    @Ignore
    @Override
    public void addForm(ModelNounForm f) {
        forms.add(f);
    }

    @Ignore
    @Override
    public Model getModel() {
        return model;
    }

    @Ignore
    @Override
    public List<ModelNounForm> getForms() {
        return forms;
    }
}
