package cz.atha.flashcards.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface WordlistDao {

    @Query("SELECT COUNT(*) FROM " + Wordlist.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Wordlist wordlist);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(Wordlist[] wordlists);

    @Query("SELECT * FROM " + Wordlist.TABLE_NAME)
    List<Wordlist> selectAll();

    @Query("select wl.* from "+ Wordlist.TABLE_NAME +" wl, "
            +"(select * from "+Word.TABLE_NAME +" where "+Word.COLUMN_PARTOFSPEECH + " = 'adjective') w "
            +" where w.wordlistid = wl._id group by wl."+Wordlist.COLUMN_ID)
    List<Wordlist> selectWordlistsWithAdjectives();

    @Query("select wl.* from "+ Wordlist.TABLE_NAME +" wl, "
            +"(select * from "+Word.TABLE_NAME +" where "+Word.COLUMN_PARTOFSPEECH + " = 'noun' ) w "
            +" where w.wordlistid = wl._id group by wl."+Wordlist.COLUMN_ID)
    List<Wordlist> selectWordlistsWithNouns();

    @Query("select wl.* from "+ Wordlist.TABLE_NAME +" wl, "
            +"(select * from "+Word.TABLE_NAME +" where "+Word.COLUMN_PARTOFSPEECH + " = 'noun' OR "
            + Word.COLUMN_PARTOFSPEECH +  " = 'adjective') w "
            +" where w.wordlistid = wl._id group by wl."+Wordlist.COLUMN_ID)
    List<Wordlist> selectWordlistsWithAdjectivesAndNouns();

    @Query("select wl.* from "+ Wordlist.TABLE_NAME +" wl, (select * from "+Word.TABLE_NAME+" where "+Word.COLUMN_PARTOFSPEECH + " = 'verb') w where w.wordlistid = wl._id group by wl."+Wordlist.COLUMN_ID)
    List<Wordlist> selectWordlistsWithVerbs();

    @Query("SELECT * FROM " + Wordlist.TABLE_NAME + " WHERE " + Wordlist.COLUMN_ID + " = :wordlistId")
    Wordlist selectById(long wordlistId);

    @Query("SELECT * FROM " + Wordlist.TABLE_NAME + " WHERE " + Wordlist.COLUMN_SRC + " = :src")
    Wordlist selectBySrc(String src);

    @Query("DELETE FROM " + Wordlist.TABLE_NAME + " WHERE " + Wordlist.COLUMN_ID + " = :id")
    int deleteById(long id);

    @Transaction
    @Query("SELECT * FROM " + Wordlist.TABLE_NAME + " WHERE " + Wordlist.COLUMN_ID + " = :wordlistId")
    public WordlistWithWords selectWordlistWithWords(long wordlistId);

    @Update
    int update(Wordlist wordList);

}
