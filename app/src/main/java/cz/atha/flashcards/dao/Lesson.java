package cz.atha.flashcards.dao;

import android.provider.BaseColumns;
import android.util.Log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import cz.atha.flashcards.lib.util.CSVReader;

@Entity(tableName = Lesson.TABLE_NAME)
public class Lesson  {

    private static final String LOGTAG = "LESSON";

    public static final String TABLE_NAME = "lessons";

    public static final String COLUMN_ID = BaseColumns._ID;

    public static final String COLUMN_SRC = "src";

    public static final String COLUMN_NAME = "name";

    public static final String COLUMN_IMAGE = "image";


    /**
     * The unique ID o
     */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = COLUMN_ID)
    public long id;

    public String seq;

    public String article;

    @ColumnInfo(index = true, name=COLUMN_SRC)
    public String src;

    @ColumnInfo(name = COLUMN_NAME)
    public String name;

    @ColumnInfo(name = COLUMN_IMAGE)
    public String image;


    // TODO get word/wordlists from data files!
    public static List<LessonWithLessonItems> getInitialData(File dir) {

        if (!dir.exists()) {
            Log.e(LOGTAG, "Dir doesn't exist in getInitialDataFile(File): " + dir.getAbsolutePath());
            return null;
        }
        List<LessonWithLessonItems> ret = new ArrayList<>();

        for (File file : dir.listFiles()) {
            if(file.isDirectory())
                ret.addAll(getInitialData(file));
            if (!file.getName().endsWith("csv"))
                continue;

            Log.i(LOGTAG, "parsing: "+file.getAbsolutePath());

            LessonWithLessonItems lessonWithItems = new LessonWithLessonItems();
            lessonWithItems.lesson = new Lesson();
            lessonWithItems.lessonItems = new ArrayList<>();

            lessonWithItems.lesson.src = file.getAbsolutePath();
            HashMap<String, String[]> recordTypes = new HashMap<>();
            try {
                CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(file)), ';', '"', 0);

                // read in lines setting metadata and words
                String[] fields;
                while ((fields = reader.readNext()) != null) {
                    // blank field
                    if (fields.length == 1 && fields[0].trim().length() == 0)
                        continue;
                    // only key
                    if(fields.length < 2){
                        Log.w(LOGTAG, String.format("encountered record with only 1 field - %s/%s",file.getAbsolutePath(),fields[0]));
                        continue;
                    }

                    String recordType = fields[0];
                    String[] fieldList = Arrays.copyOfRange(fields, 1, fields.length);
                    if (recordType.equalsIgnoreCase("Flashcard") && fields.length >= 4){
                        lessonWithItems.lesson.name = fields[3];
                        if(fields.length >= 5){
                            String[] keyValuePair = fields[4].split("=");
                            if(keyValuePair.length >=2 ) {
                                File f = new File(dir, keyValuePair[1]);
                                if (!f.exists()) {
                                    Log.e(LOGTAG, "Cannot find image: " + f.getAbsolutePath());
                                } else
                                    lessonWithItems.lesson.image = f.getAbsolutePath();

                            }
                        }
                        continue;
                    }
                    else if (!recordTypes.containsKey(recordType)){
                        recordTypes.put(recordType,fieldList);
                        continue;
                    }
                    else if(recordType.equalsIgnoreCase("header")) {
                        String[] fieldTypes = recordTypes.get(recordType);
                        for (int i = 0; i < fieldTypes.length; i++) {
                            if (i >= fieldList.length)
                            {
                                Log.e(LOGTAG, String.format("%s length=%d; index=%d; field=%s", file.getAbsolutePath(), fieldList.length, i, fieldTypes[i]));
                                continue;
                            }
                            String value = fieldList[i];
                            String key = fieldTypes[i];
                            switch (key) {
                                case "name":
                                    lessonWithItems.lesson.name = value;
                                    break;
                                case "seq":
                                    lessonWithItems.lesson.seq = value;
                                    break;
                                case "article":
                                    lessonWithItems.lesson.article = value;
                                    break;
                                case "icon":
                                    File f = new File(dir, value);
                                    if (!f.exists()) {
                                        Log.e(LOGTAG, "Cannot find image: " + f.getAbsolutePath());
                                    } else
                                        lessonWithItems.lesson.image = f.getAbsolutePath();
                                    break;

                                default:
                                    Log.w(LOGTAG, String.format("Unknown field: %s/%s=%s", file.getAbsolutePath(), key, value));
                            }
                        }
                        continue;
                    }

                    else if(recordType.equalsIgnoreCase("item")) {
                        LessonItem item = new LessonItem();
                        Log.i(LOGTAG, "recordType=" + recordType);

                        String[] fieldTypes = recordTypes.get(recordType);
                        for (int i = 0; i < fieldTypes.length; i++) {
                            if (fieldList.length < i) {
                                Log.w(LOGTAG, "Malformed file" + file.getAbsolutePath());
                                continue;
                            }
                            if (i >= fieldList.length) {
                                Log.e(LOGTAG, String.format("%s length=%d; index=%d; field=%s", file.getAbsolutePath(), fieldList.length, i, fieldTypes[i]));
                                continue;
                            }
                            String value = fieldList[i];
                            String key = fieldTypes[i];

                            switch (key) {
                                case "type":
                                    if (value.startsWith("v"))
                                        item.itemTable = Wordlist.TABLE_NAME;
                                    else if (value.startsWith("g"))
                                        item.itemTable = Article.TABLE_NAME;
                                        // TODO this is for "conjugation", "conjugation-past" which we need to support!
                                    else if (value.startsWith("c"))
                                        item.itemTable = Wordlist.TABLE_NAME;
                                    else {
                                        Log.e(LOGTAG, "Unknown lessonItem.type: " + value + ",file=" + file.getAbsolutePath());
                                        continue;
                                    }
                                    break;
                                case "key":
                                    item.src = value;
                                    break;
                                default:
                                    Log.w(LOGTAG, String.format("Unknown field: %s/%s=%s", file.getAbsolutePath(), key, value));
                            }
                        }
                        lessonWithItems.lessonItems.add(item);
                        continue;
                    }
                    else
                        Log.w(LOGTAG, String.format("Unknown record type: %s/%s=%s", file.getAbsolutePath(), recordType));

                }
                Log.i(LOGTAG,"Flashcard;Lesson;20191012");
                Log.i(LOGTAG,"header;name;seq;article;icon");
                Log.i(LOGTAG,String.format("header;%s;%s;%s;%s",
                        lessonWithItems.lesson.name,
                        lessonWithItems.lesson.seq,
                        lessonWithItems.lesson.article,
                        lessonWithItems.lesson.image));
//,
//                lessonWithItems.lesson.name,
//                        dir.getName().substring(dir.getName().length()-2),
//                        dir.getName(),
//                        new File(lessonWithItems.lesson.image).getName()));
            } catch (FileNotFoundException e) {
                Log.e(LOGTAG, "File not found, problem parsing vcab file");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(LOGTAG, "IO exception, problem parsing vcab file");
                e.printStackTrace();
            }
            ret.add(lessonWithItems);


        }
        return ret;
    }

}

