package cz.atha.flashcards.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LessonItemDao {

    @Query("SELECT COUNT(*) FROM " + LessonItem.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(LessonItem lessonItem);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(LessonItem[] lessonItems);

    @Query("SELECT * FROM " + LessonItem.TABLE_NAME)
    List<LessonItem> selectAll();

    @Query("SELECT * FROM " + LessonItem.TABLE_NAME + " WHERE " + LessonItem.COLUMN_ID + " = :lessonItemId")
    LessonItem selectById(long lessonItemId);

    @Query("DELETE FROM " + LessonItem.TABLE_NAME + " WHERE " + LessonItem.COLUMN_ID + " = :id")
    int deleteById(long id);

    @Update
    int update(LessonItem lessonItem);
}
