package cz.atha.flashcards.dao;


public class ArticleDO implements Comparable<ArticleDO>, LessonItemDO{

    private long id;

    private String src;

    private String name;

    private String desc;

    private String content;

    private String author;

    private boolean active;

    @Override
    public int compareTo(ArticleDO o) {
        if (!this.active && o.active)
            return 1;

        if (this.active && !o.active)
            return -1;

        return name.compareTo(o.name);
    }

    public ArticleDO(Article article){
        this.id = article.id;
        this.src = article.src;
        this.name = article.name;
        this.desc = article.desc;
        this.author = article.author;
        this.content = article.content;
        this.active = article.active;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getSrc() {
        return src;
    }

}
