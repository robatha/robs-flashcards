package cz.atha.flashcards.datasync;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class UpdateMetadataSaxHandler extends DefaultHandler {

    final private String LOGTAG = "UpdateMetadataSaxHandler";

    // singleton
    private File infile;
    private UpdateMetadata currentUpdateFile;

    private boolean waitingOnTitle = false;

    private List<UpdateMetadata> updateFiles = new ArrayList<>();


    public List<UpdateMetadata> getUpdateFiles() {
        return updateFiles;
    }

    public UpdateMetadataSaxHandler(File f) {
        infile = f;
    }

    public void run() {

        SAXParser parser;
        try {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            parser = parserFactory.newSAXParser();

            parser.parse(infile, this);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (localName.equals("entry")) {
            currentUpdateFile = new UpdateMetadata();
        }

        // not in an entry, nothing else we're intersted in
        if (currentUpdateFile == null)
            return;

        if (localName.equals("title")) {
            waitingOnTitle = true;
        } else if(localName.equals("link")) {
            if(attributes.getValue("rel") != null && attributes.getValue("rel").equals("self"))
                currentUpdateFile.link = attributes.getValue("href");

        } else if (localName.equals("content")) {

            currentUpdateFile.src = attributes.getValue("src");
            currentUpdateFile.md5 = attributes.getValue("md5");
            String size = attributes.getValue("size");
            if (size != null)
                currentUpdateFile.size = Long.parseLong(size);
        }

    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {

        // get ready to receive data
        if (localName.equals("entry")) {
            // </entry> - save current entry and reset variable
            updateFiles.add(currentUpdateFile);
            currentUpdateFile = null;
        } else if (localName.equals("title")) {
            waitingOnTitle = false;
        }
    }

    @Override
    public void characters(char ch[], int start, int length) {

        String chars = new String(ch, start, length);
        chars = chars.trim();


//        Log.i(LOGTAG, chars);

        if (waitingOnTitle)
            currentUpdateFile.name = chars;

/*
        if (currentEntry != null) {
            currentEntry.id = chars;
        } else {
            feed.id = chars;
        }

 */
    }


}
