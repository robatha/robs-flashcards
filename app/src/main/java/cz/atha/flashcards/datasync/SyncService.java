package cz.atha.flashcards.datasync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class SyncService extends Service {

    final static String LOGTAG = "SyncService";

    // Storage for an instance of the sync adapter
    private static SyncAdapter adapter = null;
    // Object to use as a thread-safe lock
    private static final Object lock = new Object();

    public SyncService() {
        super();
    }

    /*
     * Instantiate the sync adapter object.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        /*
         * Create the sync adapter as a singleton.
         * Set the sync adapter as syncable
         * Disallow parallel syncs
         */
        synchronized (lock) {
            if (adapter == null) {
                adapter = new SyncAdapter(getApplicationContext(), true, false);
            }
        }
    }

    /**
     * Return an object that allows the system to invoke
     * the sync adapter.
     *
     */
    @Override
    public IBinder onBind(Intent intent) {
        /*
         * Get the object that allows external processes
         * to call onPerformSync(). The object is created
         * in the base class code when the SyncAdapter
         * constructors call super()
         */
        return adapter.getSyncAdapterBinder();

    }
}
