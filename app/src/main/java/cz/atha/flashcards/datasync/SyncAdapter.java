package cz.atha.flashcards.datasync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import java.io.File;

import static cz.atha.flashcards.dao.DaoInterface.getDatadir;

public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private static String LOGTAG = "SyncAdapter";
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.i(LOGTAG, "onPerformSync started");


        File outputDir = getContext().getCacheDir(); // context being the Activity pointer
        File tempFile;
//            tempFile = File.createTempFile("prefix", "extension", outputDir);
//            tempFile.deleteOnExit();

        UpdateMetadataFetcher updater = UpdateMetadataFetcher.getUpdateDownloader();
        updater.setUpdateURL("http://atha.asuscomm.com/~rob/atom_md5.cgi");
        updater.setTargetDir(getDatadir(getContext()));
        updater.run();

        Log.i(LOGTAG, "onPerformSync ended");

    }

}
