package cz.atha.flashcards.datasync;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class UpdateMetadata {
    final String LOGTAG = "UpdateMetadata";
    String name;
    String src;
    String md5;
    String link;
    long size;

    public UpdateMetadata(File f, File targetDir){

        name = "original file";
        src = f.getAbsolutePath().substring(targetDir.getParentFile().getAbsolutePath().length()+1);
        size = -1;

        try {
            md5 = md5(new FileInputStream(f));
            size = f.length();
        } catch (FileNotFoundException e) {
            Log.e(LOGTAG, "File not found: "+ f.getAbsolutePath());
            e.printStackTrace();
            md5 = null;
        }
        link = null;
    }

    public UpdateMetadata(){

    }

    public long getSize(){ return size; }
    public File getSrcFile(File targetDir){
        return new File(targetDir.getParent(), src);
    }

    public static List<UpdateMetadata>getRecursiveList(File targetDir){
        List<File> targetFiles = getListFiles(targetDir);
        List<UpdateMetadata> files = new ArrayList<>();

        for (File f : targetFiles){
            UpdateMetadata fileMD = new UpdateMetadata(f, targetDir);
            files.add(fileMD);
        }
        return files;
    }

    private static List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                inFiles.addAll(getListFiles(file));
            } else {
 //               if(file.getName().endsWith(".csv")){
                    inFiles.add(file);
 //               }
            }
        }
        return inFiles;
    }

    private char[] hexDigits = "0123456789abcdef".toCharArray();
    private String md5(InputStream is) {
        String md5 = "";

        try {
            byte[] bytes = new byte[4096];
            int read = 0;
            MessageDigest digest = MessageDigest.getInstance("MD5");

            while ((read = is.read(bytes)) != -1) {
                digest.update(bytes, 0, read);
            }

            byte[] messageDigest = digest.digest();

            StringBuilder sb = new StringBuilder(32);

            for (byte b : messageDigest) {
                sb.append(hexDigits[(b >> 4) & 0x0f]);
                sb.append(hexDigits[b & 0x0f]);
            }

            md5 = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return md5;
    }

    class CompareListResults {
        List<UpdateMetadata> addedList = new ArrayList<>();
        List<UpdateMetadata> deletedList = new ArrayList<>();
        List<UpdateMetadata> changedList = new ArrayList<>();
        Map<UpdateMetadata, UpdateMetadata> movedMap = new HashMap<>();
    }

    // This is where we figure out what needs to be downloaded, deleted, etc.
    // we do this by comparing what is on the server and what's local

    static CompareListResults compareUpdateMetadata(List<UpdateMetadata>localList, List<UpdateMetadata>remoteList){

        CompareListResults results = new UpdateMetadata().new CompareListResults();
        results.deletedList.addAll(localList);

        for (UpdateMetadata t : remoteList) {
            int origIndex = results.deletedList.indexOf(t);
            if (origIndex < 0) {
                results.addedList.add(t);
            } else {
                UpdateMetadata origT = results.deletedList.remove(origIndex);
                if (!t.md5.equals(origT.md5)) {
                    results.changedList.add(t);
                }
            }
        }

        Map<String, UpdateMetadata> addedMap = new HashMap();
        for(UpdateMetadata fmd : results.addedList) addedMap.put(fmd.md5,fmd);
        Iterator<UpdateMetadata> it = results.deletedList.iterator();
        while (it.hasNext()) {
            UpdateMetadata deleted = it.next();
            if (addedMap.containsKey(deleted.md5)) {
                UpdateMetadata added = addedMap.get(deleted.md5);
//                            Log.i(LOGTAG, "found moved: "+deleted.src + "->"+added.src);
                results.movedMap.put(deleted, added);
                results.addedList.remove(added);
                it.remove();
            }
        }
        return results;
    }

    @Override
    public boolean equals(Object obj){

        if (obj == null) return false;
        if (! (obj instanceof UpdateMetadata)) return false;

        return this.src.equals(((UpdateMetadata)obj).src);
    }

    @Override
    public int hashCode() {
        return src.hashCode();
    }

}

