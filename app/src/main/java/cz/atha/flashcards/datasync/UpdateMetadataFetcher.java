package cz.atha.flashcards.datasync;

import android.os.Environment;
import android.util.Log;
import android.widget.ProgressBar;

import androidx.room.Update;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class UpdateMetadataFetcher {

    final private String LOGTAG = "UpdateMetadataFetcher";

    // singleton
    private static UpdateMetadataFetcher me;
    private UpdateReadyInterface callme = null;


    private String updateURL;
    private Date lastUpdate = new Date();

    private final OkHttpClient client = new OkHttpClient();
    File cacheFile;
    private File targetDir;

    private UpdateMetadataFetcher() {
    }

    public static UpdateMetadataFetcher getUpdateDownloader() {
        if (me == null)
            me = new UpdateMetadataFetcher();

        return me;
    }

    public void setUpdateReadyListener(UpdateReadyInterface imwaiting) {
        callme = imwaiting;
    }

    public void setUpdateURL(String url) {
        updateURL = url;
    }


    public void run() {


//        if (true) return;
        if (callme != null)
            callme.onCheckForUpdates();

        Request request = new Request.Builder()
                .url(updateURL)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(LOGTAG, "OkHttp3Client.newCall(request).execute() exception: " + e.getMessage());
        }

        if (!response.isSuccessful()) {
            Log.e(LOGTAG, "Unexpected code " + response);
            callme.onUpdateReady(response.code(), null, null, null, null);
            return;
        }
//                    Headers responseHeaders = response.headers();
//                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
//                        System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
        //                   }

        // TODO no need for this test dir
        File downloadDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "test");
        File cacheFile = new File(downloadDir, "cache.xml");
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(cacheFile);
            stream.write(response.body().bytes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (Exception e) {
            }
            response.body().close();
        }
        Log.i(LOGTAG, "starting parser");
        UpdateMetadataSaxHandler parser = new UpdateMetadataSaxHandler(cacheFile);
        parser.run();

        List<UpdateMetadata> remoteList = parser.getUpdateFiles();
        List<UpdateMetadata> localList = UpdateMetadata.getRecursiveList(targetDir);

        UpdateMetadata.CompareListResults res = UpdateMetadata.compareUpdateMetadata(localList, remoteList);

        // return results to our object that's listening
        int uiResponse = UpdateReadyInterface.REQUEST_OK;
        if (callme != null)
            uiResponse = callme.onUpdateReady(UpdateReadyInterface.REQUEST_OK, res.addedList, res.changedList, res.deletedList, res.movedMap);
        Log.i(LOGTAG, "OnUpdateReady, got: " + uiResponse);

        ProgressBar progressBar = null;
        if (callme != null) {
            progressBar = callme.onUpdateStarted();
            progressBar.setMax(res.addedList.size() + res.changedList.size() + res.movedMap.size() + res.deletedList.size());
            progressBar.setProgress(0);
        }
        if (uiResponse == UpdateReadyInterface.REQUEST_OK) {
            Log.i(LOGTAG, "starting download and install here.");
            List<UpdateMetadata> downloadAndReplace = new ArrayList<>();
            downloadAndReplace.addAll(res.addedList);
            downloadAndReplace.addAll(res.changedList);
            for (UpdateMetadata md : downloadAndReplace) {
                String link = md.link;
                File f = md.getSrcFile(targetDir);
                if (progressBar != null)
                    progressBar.setProgress(progressBar.getProgress() + 1);
                Log.i(LOGTAG, "Downloading from " + link + ", moving to " + f.getAbsolutePath());

                if (!f.getParentFile().exists()) f.getParentFile().mkdirs();
                try {
                    if (f.exists()) f.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                OkHttpClient client = new OkHttpClient();

                Request req = new Request.Builder()
                        .url(link)
                        .build();
                try {
                    response = client.newCall(req).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (!response.isSuccessful())
                    Log.e(LOGTAG, "Unexpected code " + response);

                try {
                    stream = new FileOutputStream(f);
                    stream.write(response.body().bytes());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        stream.close();
                    } catch (Exception e) {
                    }
                    response.body().close();

                }

                // TODO download and install updates here.
            }
            for (UpdateMetadata md : res.movedMap.keySet()) {
                File from = md.getSrcFile(targetDir);
                File to = res.movedMap.get(md).getSrcFile(targetDir);
                if (progressBar != null)
                    progressBar.setProgress(progressBar.getProgress() + 1);
                Log.i(LOGTAG, "Moving " + from.getAbsolutePath() + "->" + to.getAbsolutePath());
                from.renameTo(to);

                // TODO move here
            }
            for (UpdateMetadata md : res.deletedList) {
                if (progressBar != null)
                    progressBar.setProgress(progressBar.getProgress() + 1);
                File deleteMe = md.getSrcFile(targetDir);
                Log.i(LOGTAG, "deleting- " + deleteMe);
                deleteMe.delete();

                // TODO delete here
            }
        }
        cacheFile.delete();

        if (callme != null)
            callme.onUpdatedFinished(uiResponse, "some msg");
    }


    public void setTargetDir(File dir) {
        Log.i(LOGTAG, "test: " + dir.getAbsolutePath());
        targetDir = dir;
    }


}
