package cz.atha.flashcards.datasync;

import android.widget.ProgressBar;

import java.util.List;
import java.util.Map;

public interface UpdateReadyInterface {

    int REQUEST_OK = 0;
    int REQUEST_CANCELLED = 1;
    int REQUEST_FAILED = 2;

    void onCheckForUpdates();

    int onUpdateReady(int status,
                      List<UpdateMetadata> addedList,
                       List<UpdateMetadata> changedList,
                       List<UpdateMetadata> deletedList,
                       Map<UpdateMetadata, UpdateMetadata> movedMap);

    ProgressBar onUpdateStarted();

    void onUpdatedFinished(int status, String msg);
}
