/**
 * Copyright (c) 2016, Robert Atha
 * All rights reserved.
 * <p>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package cz.atha.flashcards.activities.flashcard;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.atha.flashcards.R;
import cz.atha.flashcards.dao.WordlistDO;


/**
 * This view provides selection for individual flashcard lists.
 *
 * @author rob
 */
public class FlashcardSelectFragment extends Fragment {

    final private static String LOGTAG = "DragAndDropSelectFragment";
    private MyRecyclerViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // set up layout, grab params
        View view = inflater.inflate(R.layout.view_select_flashcard, container, false);

        // view model contains db calls
        FlashcardViewModel model = ViewModelProviders.of(getActivity()).get(FlashcardViewModel.class);
        model.getWordlists().observe(this, wordlists -> {
            Collections.sort(wordlists);
            adapter.setData(wordlists);
            adapter.notifyDataSetChanged();
        });

        // set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new MyRecyclerViewAdapter(getContext());
        adapter.setClickListener(new ItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                //Toast.makeText(parent.getContext(), "You Clicked " + lessons.get(position), Toast.LENGTH_LONG).show();
                Log.i(LOGTAG, "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);
                WordlistDO item = adapter.getItem(position);
                model.loadWordList(item.getId());

                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.your_placeholder, new FlashcardFragment(), "conjugation_activitys_fragment" )
                        .addToBackStack(null)
                        .commit();
            }

        });

        recyclerView.setAdapter(adapter);
        return view;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private List<WordlistDO> wordlists;
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;

        // data is passed into the constructor
        MyRecyclerViewAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            wordlists = new ArrayList<>();
        }

        // inflates the cell layout from xml when needed
        @Override
        @NonNull
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.view_select_flashcard_row, parent, false);
            return new ViewHolder(view);
        }

        // binds the data to the TextView in each cell
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            WordlistDO wordlistDO = wordlists.get(position);
            holder.textViewItem.setText(wordlistDO.getName());
            holder.textViewDesc.setText(wordlistDO.getDesc());

        }
        // total number of cells
        @Override
        public int getItemCount() {
            return wordlists.size();
        }

        public void setData(List<WordlistDO> data) {
            wordlists = data;
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView textViewItem;
            TextView textViewDesc;

            ViewHolder(View itemView) {
                super(itemView);
                textViewItem = itemView.findViewById(R.id.textViewItem);
                textViewDesc =  itemView.findViewById(R.id.textViewDesc);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        // convenience method for getting data at click position
        WordlistDO getItem(int id) {
            return wordlists.get(id);
        }

        // allows clicks events to be caught
        void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }


    }

}


