package cz.atha.flashcards.activities.lesson;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import cz.atha.flashcards.R;
import cz.atha.flashcards.databinding.FragmentLessonItemBinding;
import cz.atha.flashcards.dao.ArticleDO;
import cz.atha.flashcards.dao.LessonDO;
import cz.atha.flashcards.dao.LessonItemDO;

public class LessonViewFragment extends Fragment {

    final static String LOGTAG = "LessonViewFragment";
    private MyRecyclerViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);

        // set up layout, grab params
        View view = inflater.inflate(R.layout.fragment_lesson, parent, false);

        // view model contains db calls
        LessonViewModel model = ViewModelProviders.of(getActivity()).get(LessonViewModel.class);
        model.getSelectedLesson().observe(this, lesson -> {
            adapter.setData(lesson);
            adapter.notifyDataSetChanged();
            Log.i(LOGTAG, "got here in lessonviewfrag: " + lesson.getLessonItems().size());
            TextView title = view.findViewById(R.id.lesson_name);
            title.setText(lesson.getName());
        });

        adapter = new MyRecyclerViewAdapter(getContext());
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);

        // Removes blinks
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

        // Standard setup
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        return view;
    }

    class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private LessonDO lesson;
        private LayoutInflater mInflater;

        // data is passed into the constructor
        MyRecyclerViewAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        // inflates the cell layout from xml when needed
        @Override
        @NonNull
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.fragment_lesson_item, parent, false);
            return new ViewHolder(view);
        }

        // binds the data to the TextView in each cell
        @SuppressLint("ClickableViewAccessibility")
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            LessonItemDO item = lesson.getLessonItems().get(position);
            if (item instanceof ArticleDO) {
                ArticleDO article = (ArticleDO) item;
                holder.binding.activityLayout.setVisibility(View.GONE);
                holder.binding.articleLayout.setVisibility(View.VISIBLE);

                //                holder.binding.subItemBody.setText(article.getContent());
                Document doc = Jsoup.parse(article.getContent());
                doc.head().appendElement("link").attr("rel", "stylesheet")
                        .attr("type", "text/css")
                        .attr("href", "lesson_styles.css");
                holder.binding.webview.loadDataWithBaseURL("file:///android_asset/",
                        doc.outerHtml(), "text/html", "Utf-8", null);
                // Log.d(LOGTAG, doc.outerHtml());

                LinearLayout.LayoutParams layoutParams;
                if (holder.isExpanded()) {
                    layoutParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }
                else {
                    layoutParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, 300);
                }

                holder.binding.webview.setLayoutParams(layoutParams);


//                ;subItem.setVisibility(holder.isExpanded() ? View.VISIBLE : View.GONE);
                holder.binding.webview.setOnTouchListener(new WebView.OnTouchListener() {
                    final static int FINGER_RELEASED = 0;
                    final static int FINGER_TOUCHED = 1;
                    final static int FINGER_DRAGGING = 2;
                    final static int FINGER_UNDEFINED = 3;

                    private int fingerState = FINGER_RELEASED;

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        switch (event.getAction()) {


                            case MotionEvent.ACTION_DOWN:
                                if (fingerState == FINGER_RELEASED)
                                    fingerState = FINGER_TOUCHED;
                                else
                                    fingerState = FINGER_UNDEFINED;
                                break;

                            case MotionEvent.ACTION_UP:
                                if (fingerState != FINGER_DRAGGING) {


                                    Log.e(LOGTAG, "expanded: " + !holder.isExpanded());
                                    // Get the current state of the item
                                    boolean expanded = holder.isExpanded();
                                    // Change the state
                                    holder.setExpanded(!expanded);
                                    // Notify the adapter that item has changed
                                    notifyItemChanged(position);

                                }
                                fingerState = FINGER_RELEASED;

                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (fingerState == FINGER_TOUCHED || fingerState == FINGER_DRAGGING)
                                    fingerState = FINGER_DRAGGING;
                                else fingerState = FINGER_UNDEFINED;
                                break;

                            default:
                                fingerState = FINGER_UNDEFINED;

                        }

                        return false;
                    }

                });


            } else {
                holder.binding.activityLayout.setVisibility(View.VISIBLE);
                holder.binding.articleLayout.setVisibility(View.GONE);
                holder.binding.activityTitle.setText(item.getName());
            }

        }

        // total number of cells
        @Override
        public int getItemCount() {
            return lesson.getLessonItems().size();
        }

        public void setData(LessonDO data) {
            lesson = data;
        }

        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder {
            //implements View.OnClickListener {
            FragmentLessonItemBinding binding;
            boolean expanded = false;

            ViewHolder(View itemView) {
                super(itemView);
                binding = FragmentLessonItemBinding.bind(itemView);
//                itemView.setOnClickListener(this);
            }

            boolean isExpanded() {
                return expanded;
            }

            void setExpanded(boolean e) {
                this.expanded = e;
            }
//            @Override
//            public void onClick(View view) {
//                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            //           }
        }

        // convenience method for getting data at click position
        LessonItemDO getItem(int id) {
            return lesson.getLessonItems().get(id);
        }

        // allows clicks events to be caught
        //       void setClickListener(ItemClickListener itemClickListener) {
        //           this.mClickListener = itemClickListener;
        //       }


    }
}
