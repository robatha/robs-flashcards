package cz.atha.flashcards.activities.lesson;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import cz.atha.flashcards.R;

public class LessonActivity extends AppCompatActivity {

    LessonViewModel model;
    final static String LOGTAG = "LessonActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set up viewmodel for all our fragments
        model = ViewModelProviders.of(this).get(LessonViewModel.class);

        setContentView(R.layout.activity_wordlist);

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.your_placeholder, new LessonViewFragment(), "lesson_activitys_fragment").commit();
        } else {
            Fragment test = getSupportFragmentManager().findFragmentByTag("article_activitys_fragment");
        }

    }
}
