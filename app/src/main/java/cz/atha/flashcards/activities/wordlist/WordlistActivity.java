package cz.atha.flashcards.activities.wordlist;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import cz.atha.flashcards.R;

public class WordlistActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wordlist);

        if (savedInstanceState == null) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.your_placeholder, new WordlistSelectFragment(), "wordlist_activitys_fragment" ).commit();
        } else {
            Fragment test = getSupportFragmentManager().findFragmentByTag("wordlist_activitys_fragment");
        }

    }
}
