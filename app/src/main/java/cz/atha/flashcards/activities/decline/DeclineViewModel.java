package cz.atha.flashcards.activities.decline;


import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.atha.flashcards.grammar.Case;
import cz.atha.flashcards.grammar.Number;

import cz.atha.flashcards.dao.AppRepository;
import cz.atha.flashcards.dao.WordlistDO;

public class DeclineViewModel extends AndroidViewModel {
    private String LOGTAG = "DeclineViewModel";

    // chosen in interface
    private WordlistDO selectedWordList = null;
    private Set<Case> cases = new HashSet<>();
    private Set<Number> numbers = new HashSet<>();


    public enum PartOfSpeech { noun, adjective }
    private Set<PartOfSpeech>partsOfSpeech = new HashSet<>();

    private AppRepository repository;

    public DeclineViewModel(@NonNull Application application) {
        super(application);
        repository = AppRepository.getInstance(application);
    }

    LiveData<List<WordlistDO>> getWordlistsWithAdjectivesOnly() {

        return repository.getWordlistsWithAdjectives();
    }

    LiveData<List<WordlistDO>> getWordlistsWithNounsOnly() {

        return repository.getWordlistsWithNouns();
    }

    LiveData<List<WordlistDO>> getWordlistsWithAdjectivesAndNouns() {

        return repository.getWordlistsWithAdjectivesAndNouns();
    }

    void addCase(Case c) {
        Log.d(LOGTAG, "adding case: "+c.toString());
        cases.add(c);
    }

    void removeCase(Case c) {
        Log.d(LOGTAG, "removing case: "+c.toString());
        cases.remove(c);
    }

    Set<Case> getCases(){
        return cases;
    }

    void addNumber(Number n) {
        numbers.add(n);
    }

    void removeNumber(Number n) {
        numbers.remove(n);
    }

    boolean hasSingularAndPlural(){
        return numbers.contains(Number.singular) && numbers.contains(Number.plural);
    }

    boolean hasSingularOnly(){
        return numbers.contains(Number.singular) && !numbers.contains(Number.plural);
    }

    boolean hasPluralOnly() {
        return !numbers.contains(Number.singular) && numbers.contains(Number.plural);
    }

    void addPartofSpeech(PartOfSpeech p){
        partsOfSpeech.add(p);
    }

    void removePartofSpeech(PartOfSpeech p){
        partsOfSpeech.remove(p);
    }

    boolean hasNounsAndAdjectives() {
        return partsOfSpeech.contains(DeclineViewModel.PartOfSpeech.noun)
                && partsOfSpeech.contains(DeclineViewModel.PartOfSpeech.adjective);
    }

    boolean hasOnlyAdjectives() {
        return !partsOfSpeech.contains(DeclineViewModel.PartOfSpeech.noun)
                && partsOfSpeech.contains(DeclineViewModel.PartOfSpeech.adjective);
    }

    boolean hasOnlyNouns() {
        return partsOfSpeech.contains(DeclineViewModel.PartOfSpeech.noun)
                && !partsOfSpeech.contains(DeclineViewModel.PartOfSpeech.adjective);
    }

    void loadDeclensionWordlist() {
        repository.loadDeclensionWordlist(selectedWordList
                , numbers
                , cases
                , partsOfSpeech.contains(PartOfSpeech.noun)
                , partsOfSpeech.contains(PartOfSpeech.adjective));
    }

    WordlistDO getSelectedWordlist(){ return selectedWordList; }

    void setSelectedWordlist(WordlistDO list){ selectedWordList = list; }

    LiveData<WordlistDO> getSelectedWordist() {
        return repository.getSelectedWordlist();
    }



}
