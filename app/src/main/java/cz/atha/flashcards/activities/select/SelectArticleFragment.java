/**
 * Copyright (c) 2016, Robert Atha
 * All rights reserved.
 * <p>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package cz.atha.flashcards.activities.select;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.atha.flashcards.R;
import cz.atha.flashcards.activities.articles.ArticleActivity;

import cz.atha.flashcards.dao.ArticleDO;


/**
 * This view provides selection for individual flashcard lists.
 *
 * @author rob
 */
public class SelectArticleFragment extends Fragment {

    final private static String LOGTAG = "SelectArticleFragment";
    private MyRecyclerViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        // set up layout, grab params
        View view = inflater.inflate(R.layout.view_select_article, container, false);

        // view model contains db calls
        SelectViewModel model = ViewModelProviders.of(this).get(SelectViewModel.class);
        model.getArticles().observe(this, articles -> {
            Collections.sort(articles);
            adapter.setData(articles);
            adapter.notifyDataSetChanged();
        });

        // set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new MyRecyclerViewAdapter(getContext());
        adapter.setClickListener(new ItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {

                if (view instanceof CheckBox){
                    // get data
                    ArticleDO item = adapter.getItem(position);

                    // update UI
                    CheckBox cb = (CheckBox) view;
                    item.setActive(cb.isChecked());
                    Collections.sort(adapter.articles);
                    adapter.notifyDataSetChanged();

                    // write to disk
                    model.setArticleActive(item.getId(), cb.isChecked());
                    return;
                }

                //Toast.makeText(parent.getContext(), "You Clicked " + lessons.get(position), Toast.LENGTH_LONG).show();
                Log.i(LOGTAG, "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);

                ArticleDO item = adapter.getItem(position);
                model.loadWordList(item.getId());

                // start activity
                model.loadArticle(item.getId());
                Intent intent = new Intent(getActivity(), ArticleActivity.class);

                startActivity(intent);
            }

        });

        recyclerView.setAdapter(adapter);
        return view;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private List<ArticleDO> articles;
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;

        // data is passed into the constructor
        MyRecyclerViewAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            articles = new ArrayList<>();
        }

        // inflates the cell layout from xml when needed
        @Override
        @NonNull
        public MyRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.view_select_article_row, parent, false);
            return new MyRecyclerViewAdapter.ViewHolder(view);
        }

        // binds the data to the TextView in each cell
        @Override
        public void onBindViewHolder(@NonNull MyRecyclerViewAdapter.ViewHolder holder, int position) {
            ArticleDO article = articles.get(position);
            holder.textViewItem.setText(article.getName());
            holder.textViewDesc.setText(article.getDesc());
            holder.isActive.setChecked(article.isActive());

        }
        // total number of cells
        @Override
        public int getItemCount() {
            return articles.size();
        }

        public void setData(List<ArticleDO> data) {
            articles = data;
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView textViewItem;
            TextView textViewDesc;
            CheckBox isActive;

            ViewHolder(View itemView) {
                super(itemView);
                textViewItem = itemView.findViewById(R.id.textViewItem);
                textViewDesc =  itemView.findViewById(R.id.textViewDesc);
                isActive = itemView.findViewById(R.id.active_checkBox);
                isActive.setOnClickListener(this);
                itemView.setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }


        }

        // convenience method for getting data at click position
        ArticleDO getItem(int id) {
            return articles.get(id);
        }

        // allows clicks events to be caught
        void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }


    }

}


