/**
 * 
 */
package cz.atha.flashcards.activities.select;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import cz.atha.flashcards.R;

import java.io.IOException;
import java.util.Random;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


/**
 * @author rob
 * 
 */
public class TestFragment extends Fragment {

    private static final String LOGTAG = "TestFragment";
    public static final int RequestPermissionCode = 1;

	private View selectImageFragmentView;
    protected Activity activity;


    private ImageButton buttonAudioRecord, buttonAudioStop, buttonAudioPlay;
    private String AudioSavePathInDevice = null;
    private MediaRecorder mediaRecorder ;
    Random random ;
    private String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    MediaPlayer mediaPlayer ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        selectImageFragmentView = inflater.inflate(R.layout.images_test, container, false);


        buttonAudioRecord = (ImageButton) selectImageFragmentView.findViewById(R.id.record_audio);
        buttonAudioStop = (ImageButton) selectImageFragmentView.findViewById(R.id.stop_audio);
        buttonAudioPlay = (ImageButton) selectImageFragmentView.findViewById(R.id.play_audio);

        setImageButtonEnabled(getContext(), false, buttonAudioPlay, R.drawable.ic_play_arrow_black_24dp);
        setImageButtonEnabled(getContext(), false, buttonAudioStop, R.drawable.ic_stop_black_24dp);

        random = new Random();


        buttonAudioRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkPermission()) {

                    AudioSavePathInDevice =
                            Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                    CreateRandomAudioFileName(5) + "AudioRecording.3gp";

                    mediaRecorder=new MediaRecorder();
                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                    mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                    mediaRecorder.setOutputFile(AudioSavePathInDevice);

                    try {
                        mediaRecorder.prepare();
                        mediaRecorder.start();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    setImageButtonEnabled(getContext(), false, buttonAudioRecord, R.drawable.ic_fiber_manual_record_black_24dp);
                    setImageButtonEnabled(getContext(), false, buttonAudioPlay, R.drawable.ic_play_arrow_black_24dp);
                    setImageButtonEnabled(getContext(), true, buttonAudioStop, R.drawable.ic_stop_black_24dp);

                    Toast.makeText(getActivity(), "Recording started",
                            Toast.LENGTH_LONG).show();
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new
                            String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
                }

            }
        });

        buttonAudioStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mediaRecorder != null) {
                    mediaRecorder.stop();
                    mediaRecorder = null;
                }
                if(mediaPlayer != null) {
                    mediaPlayer.stop();
                    mediaPlayer = null;
                }
                setImageButtonEnabled(getContext(), true, buttonAudioRecord, R.drawable.ic_fiber_manual_record_black_24dp);
                setImageButtonEnabled(getContext(), true, buttonAudioPlay, R.drawable.ic_play_arrow_black_24dp);
                setImageButtonEnabled(getContext(), false, buttonAudioStop, R.drawable.ic_stop_black_24dp);

                Toast.makeText(getActivity(), "Recording Completed",
                        Toast.LENGTH_LONG).show();
            }
        });

        buttonAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {

                setImageButtonEnabled(getContext(), false, buttonAudioRecord, R.drawable.ic_fiber_manual_record_black_24dp);
                setImageButtonEnabled(getContext(), false, buttonAudioPlay, R.drawable.ic_play_arrow_black_24dp);
                setImageButtonEnabled(getContext(), true, buttonAudioStop, R.drawable.ic_stop_black_24dp);

                mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(AudioSavePathInDevice);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        setImageButtonEnabled(getContext(), true, buttonAudioRecord, R.drawable.ic_fiber_manual_record_black_24dp);
                        setImageButtonEnabled(getContext(), true, buttonAudioPlay, R.drawable.ic_play_arrow_black_24dp);
                        setImageButtonEnabled(getContext(), false, buttonAudioStop, R.drawable.ic_stop_black_24dp);
                        mediaPlayer = null;
                    }
                });
                mediaPlayer.start();
                Toast.makeText(getActivity(), "Recording Playing",
                        Toast.LENGTH_LONG).show();
            }
        });

        return selectImageFragmentView;
    }

    public static void setImageButtonEnabled(Context ctxt, boolean enabled, ImageButton item,
                                             int iconResId) {
        item.setEnabled(enabled);
        Drawable originalIcon = ContextCompat.getDrawable(ctxt, iconResId);
        Drawable icon = enabled ? originalIcon : convertDrawableToGrayScale(originalIcon);
        item.setImageDrawable(icon);
    }

    public static Drawable convertDrawableToGrayScale(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        Drawable res = drawable.mutate();
        res.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        return res;
    }

    public String CreateRandomAudioFileName(int string){
        StringBuilder stringBuilder = new StringBuilder( string );
        int i = 0 ;
        while(i < string ) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++ ;
        }
        return stringBuilder.toString();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length> 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(getActivity(), "Permission Granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(),"Permission Denied",Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

}


