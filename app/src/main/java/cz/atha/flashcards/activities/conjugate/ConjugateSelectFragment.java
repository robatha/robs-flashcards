
package cz.atha.flashcards.activities.conjugate;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.atha.flashcards.R;
import cz.atha.flashcards.databinding.ViewSelectConjugationBinding;
import cz.atha.flashcards.grammar.Mood;
import cz.atha.flashcards.grammar.Tense;
import cz.atha.flashcards.dao.WordlistDO;


public class ConjugateSelectFragment extends Fragment {

    final private static String LOGTAG = "DeclineSelectFragment";
    private MyRecyclerViewAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // set up layout, grab params
        View view = inflater.inflate(R.layout.view_select_conjugation, container, false);

        // view model contains db calls
        ConjugateViewModel model = ViewModelProviders.of(getActivity()).get(ConjugateViewModel.class);
        model.getWordlistsWithVerbs().observe(this, wordlists -> {
            Collections.sort(wordlists);
            adapter.setData(wordlists);
            adapter.setSelectedWordList(model.getSelectedWordlist());
            adapter.notifyDataSetChanged();
        });

        // update view from model
        ViewSelectConjugationBinding binding = ViewSelectConjugationBinding.bind(view);
        if(model.getTense() == Tense.present && model.getMood() == Mood.indicative)
            binding.present.setChecked(true);
        else if(model.getTense() == Tense.past && model.getMood() == Mood.indicative)
            binding.past.setChecked(true);
        if(model.getTense() == Tense.future && model.getMood() == Mood.indicative)
            binding.future.setChecked(true);


        // set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new MyRecyclerViewAdapter(getContext());
        adapter.setClickListener(new ItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                //Toast.makeText(parent.getContext(), "You Clicked " + lessons.get(position), Toast.LENGTH_LONG).show();
                Log.i(LOGTAG, "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);

                adapter.setSelectedWordList(adapter.getItem(position));
                model.setSelectedWordlist(adapter.getItem(position));
            }

        });
        recyclerView.setAdapter(adapter);

        binding.startButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(adapter.selectedWordList == null){
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Oops");
                    alertDialog.setMessage("Please select a wordlist first.");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return;
                }

                // load word list now
                model.loadConjugationWordlist();

                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.your_placeholder, new ConjugateFragment(), "conjugation_activitys_fragment" ).
                        addToBackStack(null)
                        .commit();
            }

        });

        binding.conjugationType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // get selected radio button from radioGroup
                switch(checkedId){
                    case R.id.present: model.setTense(Tense.present); break;
                    case R.id.past: model.setTense(Tense.past); break;
                    case R.id.future: model.setTense(Tense.future); break;

                }
            }

        });

        return view;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private List<WordlistDO> wordlists;
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;
        private ViewHolder checkedViewHolder;
        private WordlistDO selectedWordList;

        MyRecyclerViewAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            wordlists = new ArrayList<>();
        }

        public void setSelectedWordList(WordlistDO selectedWordList) {
            this.selectedWordList = selectedWordList;
        }

        public void setData(List<WordlistDO> data) {
            wordlists = data;
        }



        // inflates the cell layout from xml when needed
        @Override
        @NonNull
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.view_select_conjugation_row, parent, false);
            return new ViewHolder(view);
        }

        // binds the data to the TextView in each cell
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            WordlistDO wordlistDO = wordlists.get(position);
            holder.textViewItem.setText(wordlistDO.getName());
            holder.textViewDesc.setText(wordlistDO.getDesc());

            if(selectedWordList != null && wordlistDO.getId() == selectedWordList.getId()) {
                holder.setChecked(true);
            }
            else holder.setChecked(false);


        }
        // total number of cells
        @Override
        public int getItemCount() {
            return wordlists.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView textViewItem;
            TextView textViewDesc;

            ViewHolder(View itemView) {
                super(itemView);
                textViewItem = itemView.findViewById(R.id.textViewItem);
                textViewDesc =  itemView.findViewById(R.id.textViewDesc);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

                // check new
                setChecked(true);
            }

            void setChecked(boolean b){

                if (!b)
                    textViewItem.setCompoundDrawablesWithIntrinsicBounds(0, 0,0,0);
                else {
                    if(checkedViewHolder != null)
                        checkedViewHolder.setChecked(false);
                    checkedViewHolder = this;
                    textViewItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_black_24dp, 0, 0, 0);
                }

            }
        }

        // convenience method for getting data at click position
        WordlistDO getItem(int id) {
            return wordlists.get(id);
        }

        // allows clicks events to be caught
        void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }


    }



}


