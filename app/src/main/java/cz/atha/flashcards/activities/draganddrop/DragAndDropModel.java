package cz.atha.flashcards.activities.draganddrop;


import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import cz.atha.flashcards.grammar.AdjectiveDO;
import cz.atha.flashcards.grammar.AdjectiveModifierDO;
import cz.atha.flashcards.grammar.Animation;
import cz.atha.flashcards.grammar.Case;
import cz.atha.flashcards.grammar.Gender;
import cz.atha.flashcards.grammar.Mood;
import cz.atha.flashcards.grammar.NounDO;
import cz.atha.flashcards.grammar.NounModifierDO;
import cz.atha.flashcards.grammar.Number;
import cz.atha.flashcards.grammar.Person;
import cz.atha.flashcards.grammar.Tense;
import cz.atha.flashcards.grammar.VerbDO;
import cz.atha.flashcards.grammar.VerbModifierDO;
import cz.atha.flashcards.grammar.Voice;
import cz.atha.flashcards.grammar.WordDO;
import cz.atha.flashcards.dao.AppRepository;
import cz.atha.flashcards.dao.WordlistDO;

public class DragAndDropModel extends AndroidViewModel {

    private String LOGTAG = "DeclineViewModel";
    private AppRepository repository;
    private AdjectiveModifierDO adjectiveModifier;
    private NounModifierDO nounModifier;
    private VerbModifierDO verbModifier;
    enum ConjugationType { infinitive, present, lform, imperative }
    enum GramaticalGender { maleAnimate, maleInanimate, female, neuter }
    enum ImperativeForm { my, ty, vy}

    public DragAndDropModel(@NonNull Application application) {
        super(application);
        repository = AppRepository.getInstance(application);

        adjectiveModifier = new AdjectiveModifierDO();
        adjectiveModifier.setCase(Case.nominative);
        adjectiveModifier.setGender(Gender.male);
        adjectiveModifier.setAnimate(Animation.inanimate);
        adjectiveModifier.setNumber(Number.singular);

        nounModifier = new NounModifierDO();
        nounModifier.setCase(Case.nominative);
        nounModifier.setNumber(Number.singular);

        verbModifier = new VerbModifierDO();
        verbModifier.setMood(Mood.unknown);
        verbModifier.setNumber(Number.singular);
        verbModifier.setPerson(Person.first);
        verbModifier.setGender(Gender.male);
        verbModifier.setAnimate(Animation.inanimate);

    }

    public GramaticalGender getGramaticalGender() {

        switch (adjectiveModifier.getGender()){
            case male:
                return (adjectiveModifier.getAnimate() == Animation.animate)
                        ? GramaticalGender.maleAnimate
                        : GramaticalGender.maleInanimate;
            case female: return GramaticalGender.female;
            default: return GramaticalGender.neuter;
        }

    }

    public void setGramaticalGender(GramaticalGender gender) {
        switch(gender){
            case maleAnimate:
                adjectiveModifier.setGender(Gender.male);
                adjectiveModifier.setAnimate(Animation.animate);
                break;
            case maleInanimate:
                adjectiveModifier.setGender(Gender.male);
                adjectiveModifier.setAnimate(Animation.inanimate);
                break;
            case female:
                adjectiveModifier.setGender(Gender.female);
                break;
            case neuter:
                adjectiveModifier.setGender(Gender.neuter);
                break;
        }
    }

    public Case getCase() {
        return adjectiveModifier.getCase();
    }

    public void setCase(Case _case) {
        Log.d(LOGTAG, "setting case: "+_case);
        adjectiveModifier.setCase(_case);
        nounModifier.setCase(_case);
    }

    public Number getNumber() {
        return adjectiveModifier.getNumber();
    }

    public void setNumber(Number number) {
        Log.d(LOGTAG, "setting number: "+ number);
        adjectiveModifier.setNumber(number);
        nounModifier.setNumber(number);
        verbModifier.setNumber(number);
    }

    public void setPerson(Person p){
        verbModifier.setPerson(p);
    }

    public Person getPerson(){
        return verbModifier.getPerson();
    }

    public void setVerbConjugationType(ConjugationType type){
        switch (type){
            case infinitive:
                verbModifier.setMood(Mood.unknown);
                break;
            case present:
                verbModifier.setVoice(Voice.active);
                verbModifier.setMood(Mood.indicative);
                verbModifier.setTense(Tense.present);
                break;
            case lform:
                verbModifier.setVoice(Voice.active);
                verbModifier.setMood(Mood.indicative);
                verbModifier.setTense(Tense.past);
                verbModifier.setGender(Gender.male);
                verbModifier.setAnimate(Animation.animate);
                break;
            case imperative:
                verbModifier.setVoice(Voice.active);
                verbModifier.setMood(Mood.imperative);
                break;
        }

    }

    public ConjugationType getVerbConjugationType(){
        if(verbModifier.getMood() == Mood.unknown) return ConjugationType.infinitive;
        else if(verbModifier.getMood() == Mood.imperative) return ConjugationType.imperative;
        else if(verbModifier.getTense() == Tense.present) return ConjugationType.present;
        else if(verbModifier.getTense() == Tense.past) return ConjugationType.lform;

        return null;
    }

    public void setImperativeForm(ImperativeForm form){
        switch (form){
            case my:
                verbModifier.setPerson(Person.first);
                verbModifier.setNumber(Number.plural);
                break;
            case ty:
                verbModifier.setPerson(Person.second);
                verbModifier.setNumber(Number.singular);
                break;
            case vy:
                verbModifier.setPerson(Person.second);
                verbModifier.setNumber(Number.plural);
                break;
        }
    }

    public ImperativeForm getImperativeForm(){
        if(verbModifier.getPerson() == Person.first)
            return ImperativeForm.my;
        else if(verbModifier.getNumber() == Number.singular)
            return ImperativeForm.ty;
        else if(verbModifier.getNumber() == Number.plural)
            return ImperativeForm.vy;
        return null;
    }

    public LiveData<List<WordlistDO>> getWordlists() {

        return repository.getAllWordlists();
    }


    String getForm(WordDO word){

        String result = null;

        if(word instanceof AdjectiveDO) {
            AdjectiveDO adj = (AdjectiveDO)word;
            if(adj.getModel() == null)
                adj.setModel(repository.getModel(adj));
            result = adj.getForm(adjectiveModifier);
        }
        if(word instanceof NounDO){
            NounDO noun = (NounDO)word;
            if (noun.getModel() == null)
                noun.setModel(repository.getModel(noun));
            result = noun.getForm(nounModifier);
        }
        if(word instanceof VerbDO){

            // if mood is unknown then we want infinitive
            if(verbModifier.getMood() == Mood.unknown)
                return word.getTarget();

            VerbDO verb = (VerbDO)word;
            if(verb.getModel() == null)
                verb.setModel(repository.getModel(verb));
            result = verb.getForm(verbModifier);
        }

        return (result == null) ? word.getTarget() : result;
    }

    public void loadWordList(long id) {
        repository.loadWordlist(id);
    }

    public LiveData<WordlistDO> getWordlist() {
        return repository.getSelectedWordlist();
    }


}
