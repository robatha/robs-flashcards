package cz.atha.flashcards.activities.articles;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import cz.atha.flashcards.dao.ArticleDO;
import cz.atha.flashcards.dao.AppRepository;


public class ArticleViewModel extends AndroidViewModel {

    private AppRepository repository;

    public ArticleViewModel(@NonNull Application application) {
        super(application);
        repository = AppRepository.getInstance(application);
    }


    public void loadArticle(long id) {
        repository.loadArticle(id);
    }

    public LiveData<ArticleDO> getArticle() {
        return repository.getArticle();
    }

}
