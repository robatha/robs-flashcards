package cz.atha.flashcards.activities.articles;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import cz.atha.flashcards.R;

public class ArticleActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set up viewmodel for all our fragments
        ArticleViewModel model = ViewModelProviders.of(this).get(ArticleViewModel.class);

        setContentView(R.layout.activity_article);

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.placeholder, new ArticleViewFragment(), "article_activitys_fragment").commit();
        } else {
            Fragment test = getSupportFragmentManager().findFragmentByTag("article_activitys_fragment");
        }
    }
}
