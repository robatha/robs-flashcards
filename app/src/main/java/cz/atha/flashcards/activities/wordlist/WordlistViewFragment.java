package cz.atha.flashcards.activities.wordlist;


import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import cz.atha.flashcards.R;

import cz.atha.flashcards.grammar.AdjectiveDO;
import cz.atha.flashcards.grammar.NounDO;
import cz.atha.flashcards.grammar.VerbDO;
import cz.atha.flashcards.grammar.WordDO;

public class WordlistViewFragment extends Fragment  {

    final static private String LOGTAG = "WordlistViewFragment";
    private MyRecyclerViewAdapter adapter;

    static class ViewHolderItem {
        TextView textViewItem;
        ImageButton audioButton;
        int position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater,parent,savedInstanceState);

        // set up layout, grab params
        View view = inflater.inflate(R.layout.fragment_wordlist_start, parent, false);
        TextView nameView = view.findViewById(R.id.nameTextView);
        TextView descriptionView = view.findViewById(R.id.descriptionTextView);

        // get data, observe
        WordlistViewModel model = ViewModelProviders.of(getActivity()).get(WordlistViewModel.class);
        model.getSelectedWordlist().observe(this, wordlist -> {

            // set up wordlist itself
            nameView.setText(wordlist.getName());
            if (wordlist.getDesc() != null)
                descriptionView.setText(wordlist.getDesc());
            else
                descriptionView.setText("");

            // adapter here for words
//            Collections.sort(articles);
            adapter.setData(wordlist.getWords());
            adapter.notifyDataSetChanged();
        });
        // set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new MyRecyclerViewAdapter(getContext());
        adapter.setClickListener(new ItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {

                WordDO item = adapter.getItem(position);
                if (view instanceof ImageButton){
                    try {
                        // get data
                        FileInputStream stream = new FileInputStream(new File(item.getAudio()));
                        FileDescriptor fd = stream.getFD();

                        MediaPlayer mp = new MediaPlayer();
                        mp.setDataSource(fd);
                        mp.prepare();
                        mp.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }

                //Toast.makeText(parent.getContext(), "You Clicked " + lessons.get(position), Toast.LENGTH_LONG).show();
                Log.i(LOGTAG, "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);

                // TODO i'm sure I caould simplify the below
                if (item instanceof AdjectiveDO) {
                    WordlistViewModel model = ViewModelProviders.of(getActivity()).get(WordlistViewModel.class);
                    model.loadAdjectiveForms((AdjectiveDO)item);
                    // Create the fragment and show it as a dialog.
                    DialogFragment newFragment = AdjectiveViewFragment.newInstance();
                    newFragment.setCancelable(false);
                    newFragment.show(getFragmentManager(), "dialog");
                }
                else if (item instanceof NounDO) {
                    WordlistViewModel model = ViewModelProviders.of(getActivity()).get(WordlistViewModel.class);
                    model.loadNounForms((NounDO)item);
                    // Create the fragment and show it as a dialog.
                    DialogFragment newFragment = NounViewFragment.newInstance();
                    newFragment.setCancelable(false);
                    newFragment.show(getFragmentManager(), "dialog");
                }
                else if (item instanceof VerbDO) {
                    WordlistViewModel model = ViewModelProviders.of(getActivity()).get(WordlistViewModel.class);
                    model.loadVerbForms((VerbDO)item);
                    // Create the fragment and show it as a dialog.
                    DialogFragment newFragment = VerbViewFragment.newInstance();
                    newFragment.setCancelable(false);
                    newFragment.show(getFragmentManager(), "dialog");
                }
            }

        });

        recyclerView.setAdapter(adapter);
        return view;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private List<WordDO> words;
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;

        // data is passed into the constructor
        MyRecyclerViewAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            words = new ArrayList<>();
        }

        // inflates the cell layout from xml when needed
        @Override
        @NonNull
        public MyRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.fragment_wordlist_start_listrow, parent, false);
            return new MyRecyclerViewAdapter.ViewHolder(view);
        }

        // binds the data to the TextView in each cell
        @Override
        public void onBindViewHolder(@NonNull MyRecyclerViewAdapter.ViewHolder holder, int position) {
            WordDO word = words.get(position);
            final String text = String.format("%s - %s", word.getTarget(), word.getMeaning());
            holder.textViewItem.setText(text);

            //TODO when audio is implemented, we turn it on here
            // this will be all the time
//            if(word.getAudio() == null)
                holder.audioButton.setVisibility(View.GONE);

        }
        // total number of cells
        @Override
        public int getItemCount() {
            return words.size();
        }

        public void setData(List<WordDO> data) {

            // TODO - sort with hooks and accents properly
            Collections.sort(data);
            words = data;
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView textViewItem;
            ImageButton audioButton;

            ViewHolder(View itemView) {
                super(itemView);
                textViewItem = itemView.findViewById(R.id.textViewItem);
                audioButton =  itemView.findViewById(R.id.audio_button);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }


        }

        // convenience method for getting data at click position
        WordDO getItem(int id) {
            return words.get(id);
        }

        // allows clicks events to be caught
        void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }


    }
/*
    class WordListAdapter extends ArrayAdapter<WordDO> {

        private List<WordDO> groups;
        private LayoutInflater inflater;
        private Context context;


        WordListAdapter(Context ctx, List<WordDO> values) {
            super(ctx, R.layout.fragment_wordlist_start_listrow, values);
            this.groups = values;
            this.context = ctx;
            //Collections.sort(this.groups);

            inflater = (LayoutInflater) ctx.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public View getView(final int groupPosition, View convertView, ViewGroup parent) {

            final WordlistViewFragment.ViewHolderItem viewHolder;
            //           final WordDO word = (WordDO) groups.get(viewHolder.position);

            if (convertView == null) {
                // inflate view
                convertView = inflater.inflate(R.layout.fragment_wordlist_start_listrow, parent, false);

                // well set up the ViewHolder
                viewHolder = new WordlistViewFragment.ViewHolderItem();
                viewHolder.textViewItem = convertView.findViewById(R.id.textViewItem);
                viewHolder.audioButton = convertView.findViewById(R.id.audio_button);

                // store the holder with the view.
                convertView.setTag(viewHolder);

                // the clicked the activity, go to it
                viewHolder.textViewItem.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // set the custom dialog component.
                        WordDO word = groups.get(viewHolder.position);

                        // TODO i'm sure I caould simplify the below
                        if (word instanceof AdjectiveDO) {
                            WordlistViewModel model = ViewModelProviders.of(getActivity()).get(WordlistViewModel.class);
                            model.loadAdjectiveForms((AdjectiveDO)word);
                            // Create the fragment and show it as a dialog.
                            DialogFragment newFragment = AdjectiveViewFragment.newInstance();
                            newFragment.setCancelable(false);
                            newFragment.show(getFragmentManager(), "nounDialog");
                        }
                        else if (word instanceof NounDO) {
                            WordlistViewModel model = ViewModelProviders.of(getActivity()).get(WordlistViewModel.class);
                            model.loadNounForms((NounDO)word);
                            // Create the fragment and show it as a dialog.
                            DialogFragment newFragment = NounViewFragment.newInstance();
                            newFragment.setCancelable(false);
                            newFragment.show(getFragmentManager(), "nounDialog");
                        }
                        else if (word instanceof VerbDO) {
                            WordlistViewModel model = ViewModelProviders.of(getActivity()).get(WordlistViewModel.class);
                            model.loadVerbForms((VerbDO)word);
                            // Create the fragment and show it as a dialog.
                            DialogFragment newFragment = VerbViewFragment.newInstance();
                            newFragment.setCancelable(false);
                            newFragment.show(getFragmentManager(), "verbDialog");
                        }
                    }
                });


            } else {
                // we've just avoided calling findViewById() on resource everytime
                // just use the viewHolder
                viewHolder = (WordlistViewFragment.ViewHolderItem) convertView.getTag();
            }

            final WordDO word = (WordDO) groups.get(groupPosition);
            WordDO group = groups.get(groupPosition);

            final String text = String.format("%s - %s", word.getTarget(), word.getMeaning());
            viewHolder.position = groupPosition;
            viewHolder.textViewItem.setText(text);

            // well set up the ViewHolder

            Log.e(LOGTAG, "adding word- " + word);

            // remove audio button if no audio for word
            if (word.getAudio() == null) {
                ViewGroup layout = (ViewGroup) convertView.findViewById(R.id.word_layout);
                layout.removeView(viewHolder.audioButton);
            } else {
                viewHolder.audioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO - this is going to change!!!!
                        FileDescriptor fd = null;
                                // WordListDao.getInstance().getAudioFD(word.getAudio());

                        try {
                            MediaPlayer mp = new MediaPlayer();
                            mp.setDataSource(fd);
                            mp.prepare();
                            mp.start();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }



            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

 */
}
