package cz.atha.flashcards.activities.wordlist;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import cz.atha.flashcards.R;
import cz.atha.flashcards.databinding.FragmentWordAdjectiveDialogueBinding;
import cz.atha.flashcards.databinding.FragmentWordNounDialogueBinding;

public class AdjectiveViewFragment extends DialogFragment {


    static AdjectiveViewFragment newInstance() {
        return new AdjectiveViewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View dialogView = inflater.inflate(R.layout.fragment_word_adjective_dialogue, container, false);
        FragmentWordAdjectiveDialogueBinding binding = FragmentWordAdjectiveDialogueBinding.bind(dialogView);
        WordlistViewModel model = ViewModelProviders.of(getActivity()).get(WordlistViewModel.class);
        binding.setViewModel(model);

        // if button is clicked, close the custom dialog
        binding.dialogButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdjectiveViewFragment.this.dismiss();
            }
        });

        return dialogView;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
//            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
