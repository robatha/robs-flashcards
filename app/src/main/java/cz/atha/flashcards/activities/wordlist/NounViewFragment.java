package cz.atha.flashcards.activities.wordlist;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import cz.atha.flashcards.R;
import cz.atha.flashcards.databinding.FragmentWordNounDialogueBinding;
import cz.atha.flashcards.databinding.FragmentWordVerbDialogueBinding;

public class NounViewFragment extends DialogFragment {


    static NounViewFragment newInstance() {
        return new NounViewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View dialogView = inflater.inflate(R.layout.fragment_word_noun_dialogue, container, false);
        FragmentWordNounDialogueBinding binding = FragmentWordNounDialogueBinding.bind(dialogView);
        WordlistViewModel model = ViewModelProviders.of(getActivity()).get(WordlistViewModel.class);
        binding.setViewModel(model);

        // if button is clicked, close the custom dialog
        binding.dialogButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NounViewFragment.this.dismiss();
            }
        });

        return dialogView;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
//            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
