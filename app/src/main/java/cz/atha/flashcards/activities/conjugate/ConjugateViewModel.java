package cz.atha.flashcards.activities.conjugate;


import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import cz.atha.flashcards.grammar.Mood;
import cz.atha.flashcards.grammar.Tense;
import cz.atha.flashcards.grammar.VerbModifierDO;
import cz.atha.flashcards.grammar.Voice;
import cz.atha.flashcards.dao.AppRepository;
import cz.atha.flashcards.dao.WordlistDO;

public class ConjugateViewModel extends AndroidViewModel {
    private String LOGTAG = "DeclineViewModel";

    // chosen in interface
    final private VerbModifierDO modifier = new VerbModifierDO();;
    private WordlistDO selectedWordList = null;

    private AppRepository repository;

    public ConjugateViewModel(@NonNull Application application) {
        super(application);
        repository = AppRepository.getInstance(application);
        modifier.setMood(Mood.indicative);
        modifier.setVoice(Voice.active);
        modifier.setTense(Tense.present);
    }

    public LiveData<List<WordlistDO>> getWordlistsWithVerbs() {

        return repository.getWordlistsWithVerbs();
    }

    public void setTense(Tense t) {
        Log.d(LOGTAG, "setting tense: "+t.toString());
        modifier.setTense(t);
    }

    public Tense getTense(){
        return modifier.getTense();
    }

    public void setMood() { modifier.setMood(Mood.conditional);}

    public WordlistDO getSelectedWordlist(){ return selectedWordList; }

    public void setSelectedWordlist(WordlistDO list){ selectedWordList = list; }

    public Mood getMood() { return modifier.getMood(); }

    public void loadConjugationWordlist() {
        repository.loadConjugationWordlist(selectedWordList, modifier);
    }

    public LiveData<WordlistDO> getSelectedWordist() {
        return repository.getSelectedWordlist();
    }



}
