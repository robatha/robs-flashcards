package cz.atha.flashcards.activities.articles;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import cz.atha.flashcards.R;
import cz.atha.flashcards.databinding.FragmentArticleViewBinding;

public class ArticleViewFragment extends DialogFragment {

    final private static String LOGTAG = "ArticleViewFragment";
    final private Object lock = new Object();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_article_view, parent, false);
        WebView webView = view.findViewById(R.id.webview);

        // view model contains db calls
        FragmentArticleViewBinding binding = FragmentArticleViewBinding.bind(view);
        ArticleViewModel model = ViewModelProviders.of(this).get(ArticleViewModel.class);
        model.getArticle().observe(this, article -> {
            Document doc = Jsoup.parse(article.getContent());
            doc.head().appendElement("link").attr("rel", "stylesheet")
                    .attr("type", "text/css")
                    .attr("href","lesson_styles.css");

            Log.d(LOGTAG, doc.outerHtml());
            binding.webview.loadDataWithBaseURL("file:///android_asset/",
                    doc.outerHtml(), "text/html", "Utf-8", null);
//            binding.webview.loadUrl("file:///android_asset/test.html");
        });



        return view;
    }


}
