package cz.atha.flashcards.activities.select;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cz.atha.flashcards.R;
import cz.atha.flashcards.activities.conjugate.ConjugateActivity;
import cz.atha.flashcards.activities.decline.DeclineActivity;
import cz.atha.flashcards.activities.draganddrop.DragAndDropActivity;
import cz.atha.flashcards.activities.flashcard.FlashcardActivity;
import cz.atha.flashcards.activities.wordlist.WordlistActivity;

public class SelectActivityFragment extends Fragment {

    final static private String LOGTAG = "SelectActivityFragment";
    MyRecyclerViewAdapter adapter;

    class WordlistChoice {
        String name;
        int color;
        Class<? extends Activity> activity;
        WordlistChoice(String n, int c, Class<? extends Activity> a){
            name = n; color = c; activity = a;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_select_activity, parent, false);

        List<WordlistChoice> choices = new ArrayList();
        choices.add(new WordlistChoice("View wordlist", Color.rgb(0xFD, 0xBC, 0x5F), WordlistActivity.class));
        choices.add(new WordlistChoice("Flashcards", Color.rgb(242, 101, 49), FlashcardActivity.class));
        choices.add(new WordlistChoice("Conjugate", Color.rgb(233, 119, 175), ConjugateActivity.class));
        choices.add(new WordlistChoice("Decline", Color.rgb(117, 129, 191), DeclineActivity.class));
        choices.add(new WordlistChoice("Drag and Drop", Color.rgb(114, 204, 210), DragAndDropActivity.class));
        choices.add(new WordlistChoice("Flashcards", Color.rgb(200, 223, 142), FlashcardActivity.class));

        // set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.rvNumbers);
        int numberOfColumns = calculateNoOfColumns(getContext(), 180);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
        adapter = new MyRecyclerViewAdapter(getContext(), choices);
        adapter.setClickListener(new ItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                // Begin the transaction

                // Replace the contents of the container with the new fragment
                WordlistChoice choice = adapter.getItem(position);
                try {
                    Intent intent = new Intent(getActivity(), choice.activity);
                    getActivity().startActivity(intent);
                } catch (Exception e){
                    Log.e(LOGTAG, "Cannot instantiate "+ choice.name);
                    e.printStackTrace();
                }
//                Log.i(LOGTAG, "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);
            }
        });
        recyclerView.setAdapter(adapter);

        return view;
    }


    private int calculateNoOfColumns(Context context, float columnWidthDp) { // For example columnWidthdp=180
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (screenWidthDp / columnWidthDp + 0.5); // +0.5 for correct rounding to int.
        return noOfColumns;
    }

    class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private List<WordlistChoice> choices;
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;

        // data is passed into the constructor
        MyRecyclerViewAdapter(Context context, List<WordlistChoice> c) {
            this.mInflater = LayoutInflater.from(context);
            this.choices = c;
        }

        // inflates the cell layout from xml when needed
        @Override
        @NonNull
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.fragment_select_activity_item, parent, false);
            return new ViewHolder(view);
        }

        // binds the data to the TextView in each cell
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.myTextView.setText(choices.get(position).name);
            holder.myTextView.setBackgroundColor(choices.get(position).color);
        }

        // total number of cells
        @Override
        public int getItemCount() {
            return choices.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView myTextView;

            ViewHolder(View itemView) {
                super(itemView);
                myTextView = itemView.findViewById(R.id.info_text);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        // convenience method for getting data at click position
        WordlistChoice getItem(int id) {
            return choices.get(id);
        }

        // allows clicks events to be caught
        void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }

    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

