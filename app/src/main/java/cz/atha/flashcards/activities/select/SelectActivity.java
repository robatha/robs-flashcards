package cz.atha.flashcards.activities.select;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import androidx.viewpager.widget.ViewPager;

import android.Manifest;

import android.accounts.Account;
import android.accounts.AccountManager;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;

import android.content.DialogInterface;
import android.content.pm.PackageManager;

import android.os.Build;
import android.os.Bundle;

import android.util.AttributeSet;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ProgressBar;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cz.atha.flashcards.R;
import cz.atha.flashcards.datasync.AuthenticatorService;
import cz.atha.flashcards.datasync.UpdateMetadataFetcher;
import cz.atha.flashcards.datasync.UpdateMetadata;
import cz.atha.flashcards.datasync.UpdateReadyInterface;


/**
 * Created by rob
 */
public class SelectActivity extends AppCompatActivity implements UpdateReadyInterface {

    private static String LOGTAG = "SelectActivity";

    CustomPagerAdapter mCustomPagerAdapter;
    ViewPager mViewPager;
    int NUM_FRAGMENTS = 3;

    // Below stuff for data syncing.
    // Constants
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "cz.atha.flashcards.provider";

    // Instance fields
    Account syncAccount;
    ContentResolver resolver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_old);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mCustomPagerAdapter = new CustomPagerAdapter(this.getSupportFragmentManager(), this);

        mViewPager = findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mViewPager.setOffscreenPageLimit(NUM_FRAGMENTS);

//        ActionBar actionBar = getActionBar();
 //       actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME
 //               | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);


        // Create the dummy account for data syncing
        // Create the account type and default account
        syncAccount = AuthenticatorService.GetAccount();
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) this.getSystemService(ACCOUNT_SERVICE);
//        accountManager.get
//                .getAccountVisibility(syncAccount,AuthenticatorService.ACCOUNT_TYPE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(syncAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            Bundle extras = new Bundle();
            resolver = this.getContentResolver();
            ContentResolver.setIsSyncable(syncAccount, AUTHORITY, 1);
            ContentResolver.setSyncAutomatically(syncAccount, AUTHORITY, true);
            ContentResolver.requestSync(syncAccount, AUTHORITY, extras);
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
            Log.e(LOGTAG, "ERRRORRRRR here");
        }

        UpdateMetadataFetcher.getUpdateDownloader().setUpdateReadyListener(this);

        // stupid permissions
        isStoragePermissionGranted();

    }

    private boolean updateRequestCancelled = false;

    public static class CheckForUpdatesDialogFragment extends DialogFragment{

        SelectActivity activity;

        public void setActivity(SelectActivity a){ activity = a; }

        @NotNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle("Downloading updates");
            alertDialogBuilder.setMessage(getContext().getString(R.string.downloading_file_metadata));
            alertDialogBuilder.setCancelable(false);
            activity.setUpdateRequestCancelled(false);

            alertDialogBuilder.setNegativeButton(getContext().getString(R.string.button_cancel),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            activity.setUpdateRequestCancelled(true);
                            dialog.dismiss();
                        }
                    });

            return alertDialogBuilder.create();
        }
    }

    CheckForUpdatesDialogFragment checkForUpdatesDialog;
    @Override
    public void onCheckForUpdates(){

        FragmentManager fm = getSupportFragmentManager();

        checkForUpdatesDialog = null;
        checkForUpdatesDialog = new CheckForUpdatesDialogFragment();
        checkForUpdatesDialog.setCancelable(false);
        checkForUpdatesDialog.setActivity(this);
        checkForUpdatesDialog.show(fm, "fragment_alert");

    }

    public void setUpdateRequestCancelled(boolean p){ updateRequestCancelled = p;}

    public static class UpdatesFoundDialogFragment extends DialogFragment{

        SelectActivity activity;
        List<UpdateMetadata> addedList;
        List<UpdateMetadata> changedList;
        List<UpdateMetadata> deletedList;
        Map<UpdateMetadata, UpdateMetadata> movedMap;
        int responseCode;

        int response = UpdateReadyInterface.REQUEST_CANCELLED;
        private final static Object lock = new Object();


        public void setActivity(SelectActivity a){ activity = a; }

        void setUpdateMetadata(int response, List<UpdateMetadata> added,
                                      List<UpdateMetadata> changed,
                                      List<UpdateMetadata> deleted,
                                      Map<UpdateMetadata, UpdateMetadata> moved){
            addedList = added;
            changedList = changed;
            deletedList = deleted;
            movedMap = moved;
            responseCode = response;
        }

        private String getFileSize(long size) {
            if (size <= 0)
                return "0";

            final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
            int digitGroups = (int) (Math.log10(size) / Math.log10(1024));

            return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
        }

        @NotNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            activity.setUpdateRequestCancelled(false);
            String msg = "";

            if (responseCode != UpdateReadyInterface.REQUEST_OK){
                    alertDialogBuilder.setTitle(String.format(Locale.US, "Error! (code %d)", responseCode));
                    alertDialogBuilder.setMessage("Problem finding updates, please try again later.");
                    alertDialogBuilder.setNegativeButton(getContext().getString(R.string.button_ok),
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    synchronized (UpdatesFoundDialogFragment.lock) {
                                        response = UpdateReadyInterface.REQUEST_CANCELLED;
                                        lock.notify();
                                    }
                                    dialog.dismiss();
                                }
                            });

                alertDialogBuilder.setCancelable(false);
                activity.setUpdateRequestCancelled(false);

                return alertDialogBuilder.create();
            }

            long size = 0;
            for (UpdateMetadata md: addedList) {
                size += md.getSize();
            }
            for (UpdateMetadata md: changedList) {
                size += md.getSize();
            }
            if (addedList.size() > 0 || changedList.size() > 0 || deletedList.size() > 0 || movedMap.size() > 0) {
                alertDialogBuilder.setTitle("Updates found");

                if (addedList.size() > 0)
                    msg += String.format(Locale.US, "%d file%s added.\n",
                            addedList.size(), (addedList.size()==1)?"":"s");
                if (changedList.size() > 0)
                    msg += String.format(Locale.US, "%d file%s changed\n"
                            , changedList.size(), (changedList.size()==1)?"":"s");
                if (deletedList.size() > 0)
                    msg += String.format(Locale.US, "%d file%s deleted\n",
                            deletedList.size(), (deletedList.size()==1)?"":"s");
                if (movedMap.size() > 0)
                    msg += String.format(Locale.US, "%d files%s moved\n",
                            movedMap.size(), (movedMap.size()==1)?"":"s");
                if (size > 0)
                    msg += String.format("\nA total of %s will need to be downloaded.\n", getFileSize(size));
                msg += "\nContinue update?";

                alertDialogBuilder.setMessage(msg);
                alertDialogBuilder.setPositiveButton(getContext().getString(R.string.button_update),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                synchronized (UpdatesFoundDialogFragment.lock) {
                                    response = UpdateReadyInterface.REQUEST_OK;
                                    lock.notify();
                                }
                                dialog.dismiss();
                            }
                        });
                alertDialogBuilder.setNegativeButton(getContext().getString(R.string.button_cancel),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                synchronized (UpdatesFoundDialogFragment.lock) {
                                    response = UpdateReadyInterface.REQUEST_CANCELLED;
                                    lock.notify();
                                }
                                dialog.dismiss();
                            }
                        });

            } else {
                alertDialogBuilder.setTitle("No Updates found");
                alertDialogBuilder.setMessage("No updates found.");
                alertDialogBuilder.setNegativeButton(getContext().getString(R.string.button_ok),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                synchronized (UpdatesFoundDialogFragment.lock) {
                                    response = UpdateReadyInterface.REQUEST_CANCELLED;
                                    lock.notify();
                                }
                                dialog.dismiss();
                            }
                        });
            }
//            alertDialogBuilder.setMessage(msg);
            alertDialogBuilder.setCancelable(false);
            activity.setUpdateRequestCancelled(false);

            return alertDialogBuilder.create();
        }

        int getResponse() {
            return response;
        }
    }

    @Override
    public int onUpdateReady(int status, List<UpdateMetadata> addedList,
                              List<UpdateMetadata> changedList,
                              List<UpdateMetadata> deletedList,
                              Map<UpdateMetadata, UpdateMetadata> movedMap){

        // need to make this an array b/c it needs to be final and changeable
        if (updateRequestCancelled) {
            Log.i(LOGTAG, "Check update cancelled.");
            return UpdateReadyInterface.REQUEST_CANCELLED;
        }
        checkForUpdatesDialog.dismiss();


        FragmentManager fm = getSupportFragmentManager();

        UpdatesFoundDialogFragment updatesFoundDialogFragment = new UpdatesFoundDialogFragment();
        updatesFoundDialogFragment.setCancelable(false);
        updatesFoundDialogFragment.setActivity(this);
        updatesFoundDialogFragment.setUpdateMetadata( status, addedList, changedList, deletedList, movedMap);
        updatesFoundDialogFragment.show(fm, "fragment_alert2");

        int response;
        synchronized (UpdatesFoundDialogFragment.lock){
            try {
                UpdatesFoundDialogFragment.lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            response = updatesFoundDialogFragment.getResponse();
        }
        return response;
    }

    private UpdatingDialogFragment updatingDialogFragment = null;


    public static class UpdatingDialogFragment extends DialogFragment{

        Dialog dialog;
        final static Object lock = new Object();
        ProgressBar bar = null;

        ProgressBar getProgressBar(){
            return bar;
        }

        @NotNull
        @Override
        public Dialog onCreateDialog(Bundle savedInsantanceState) {
            super.onCreateDialog(savedInsantanceState);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//            View view = getLayoutInflater().inflate(R.layout.dialog_update_progress, null);
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View v = layoutInflater.inflate(R.layout.dialog_update_progress, null);

            //            alertDialogBuilder.
            alertDialogBuilder.setView(v);
///            alertDialogBuilder.setView(R.layout.dialog_update_progress);
            synchronized (lock) {

                dialog = alertDialogBuilder.create();
                bar = v.findViewById(R.id.progressBar);
                Log.i(LOGTAG, "notify."+bar);

//                bar = new ProgressBar(getActivity());
                lock.notifyAll();
            }

            return dialog;
        }
    }

    @Override
    public ProgressBar onUpdateStarted() {

        ProgressBar progressBar;

        FragmentManager fm = getSupportFragmentManager();
        updatingDialogFragment = new UpdatingDialogFragment();
        updatingDialogFragment.setCancelable(false);
//        checkForUpdatesDialog.setActivity(this);
        updatingDialogFragment.show(fm, "fragment_alert3");

        synchronized(UpdatingDialogFragment.lock) {
                Log.i(LOGTAG, "wait.");
            try {
                UpdatingDialogFragment.lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            progressBar = updatingDialogFragment.getProgressBar();
        }
        Log.i(LOGTAG, "not blcoking");

//        Log.i(LOGTAG, "test."+ret[0].toString());
        return progressBar;
    }

    @Override
    public void onUpdatedFinished(int status, String msg){
        Log.i(LOGTAG, "onUpdateFinished called with: "+ status+", msg="+msg);
        updatingDialogFragment.dismiss();
    }

    boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(LOGTAG,"Permission is granted");
                return true;
            } else {

                Log.v(LOGTAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(LOGTAG,"Permission is granted");
            return true;
        }
    }

    @Override
    public View onCreateView(@NotNull String name, @NotNull Context context, @NotNull AttributeSet attrs){
        return super.onCreateView(name,context,attrs);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.select_menubar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action with ID action_refresh was selected
        if ( item.getItemId() == R.id.action_refresh) {

            // Pass the settings flags by inserting them in a bundle
            Bundle settingsBundle = new Bundle();
            settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

            /*
             * Request the sync for the default account, authority, and
             * manual sync settings
             */

            Log.i(LOGTAG, "Requesting sync.");
            ContentResolver.requestSync(syncAccount, AUTHORITY, settingsBundle);
        }

        return true;
    }



//    @Override
//    public void onBackPressed(){
//
//        Log.d(LOGTAG, "back pressed.");
//        // if the fragment support manager of this activity is already, we need to
//        // start emptying the children's fragment manager.
//        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
//            // get top of stack
//            for(int i = 0; i < mCustomPagerAdapter.getCount(); i++) {
//                Fragment frag = mCustomPagerAdapter.getItem(i);
//                FragmentManager fm = frag.getChildFragmentManager();
//                if (fm.getBackStackEntryCount() > 1) {
//                    fm.popBackStack();
//                    return;
//                }
//            }
//        }
//        super.onBackPressed();
//    }

    class CustomPagerAdapter extends FragmentPagerAdapter {

        Context mContext;

        CustomPagerAdapter(FragmentManager fm, Context context) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
            mContext = context;
        }


        @NotNull
        @Override
        public Fragment getItem(int position) {


            // Create fragment object
            Fragment fragment;

            switch(position){
                case 0:
                    fragment = new SelectLessonFragment();
                    break;


                case 1:
                    fragment = new SelectActivityFragment();
                    break;

                default:
                    fragment = new SelectArticleFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_FRAGMENTS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch(position){
                case 0:  return "Lessons";
                case 1:  return "Activities";
                default: return "Articles";
            }
        }
    }
}
