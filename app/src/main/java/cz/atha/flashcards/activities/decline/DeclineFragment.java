package cz.atha.flashcards.activities.decline;


import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import cz.atha.flashcards.R;
import cz.atha.flashcards.databinding.FragmentFlashcardBinding;
import cz.atha.flashcards.grammar.AdjectiveDO;
import cz.atha.flashcards.grammar.NounDO;
import cz.atha.flashcards.grammar.VerbDO;
import cz.atha.flashcards.grammar.WordDO;
import cz.atha.flashcards.dao.WordlistDO;


public class DeclineFragment extends Fragment {

    private Activity mActivity;
    private static final String LOGTAG = "DeclineFragment";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_main, parent, false);
        RecyclerView rv = root.findViewById(R.id.recycler_view);

        rv.setLayoutManager(new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false));

        DeclineViewModel model = ViewModelProviders.of(getActivity()).get(DeclineViewModel.class);
        model.getSelectedWordist().observe(this, wordlist -> {
            rv.setAdapter(new MyRecyclerViewAdapter(wordlist));
        });

        PagerSnapHelperVerbose verbose = new PagerSnapHelperVerbose(rv, new RVPagerStateListener() {

            @Override
            public void onPageSelected(int index) {
                SelectablePage vh = (SelectablePage) rv.findViewHolderForAdapterPosition(index);

                if (vh != null)
                    vh.onSelected();
            }
        });
        verbose.attachToRecyclerView(rv);


        return root;
    }

    interface RVPagerStateListener {
        void onPageSelected(int index);
    }

    interface SelectablePage {
        void onSelected();
    }


    class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private LayoutInflater mInflater;
        private WordlistDO wordlist;

        MyRecyclerViewAdapter(WordlistDO wordlistDO) {
            this.wordlist = wordlistDO;
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            mInflater = LayoutInflater.from(recyclerView.getContext());
        }

        @Override
        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
            mInflater = null;
        }

        class ViewHolder extends RecyclerView.ViewHolder implements SelectablePage {

            private TextView mPageIndexText;
            private FragmentFlashcardBinding binding;
            private WordDO word;


            public ViewHolder(@NonNull FragmentFlashcardBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }

            public void bind(WordDO word) {
                this.word = word;
                binding.textViewQuestion.setText(word.getMeaning());
                binding.lastResult.setText(word.getTarget());

                if (wordlist.getSrc() == null) {
                    Log.w(LOGTAG, "WordList.src is null, cannot find image.");
                } else if (word.getImage() == null) {
                    Log.w(LOGTAG, "Word.image is null so can't retrieve image.");
                } else {
                    File resourceDir = new File(wordlist.getSrc()).getParentFile();
                    File imageFile = new File(resourceDir, word.getImage());
                    try {
                        InputStream istream = new BufferedInputStream(new FileInputStream(imageFile));
                        Drawable drawable = new BitmapDrawable(getResources(), BitmapFactory.decodeStream(istream));
                        binding.imageViewQuestion.setImageDrawable(drawable);
                    } catch (IOException e) {
                        Log.e(LOGTAG, "Cannot open bitmap: " + imageFile.getAbsolutePath());
                    }
                }
                binding.lastResult.setVisibility(View.INVISIBLE);
                binding.showButton.setChecked(false);
                binding.showButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked)
                            binding.lastResult.setVisibility(View.VISIBLE);
                        else binding.lastResult.setVisibility(View.INVISIBLE);
                    }
                });
                // TODO support audio
                if (word.getAudio() == null)
                    binding.imageAudio.setVisibility(View.GONE);
                showGrammarOptions(false);
                binding.optionsButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        showGrammarOptions(isChecked);
                    }
                });
            }

            public void showGrammarOptions(boolean show) {
                // I know I can simplify the logic but it's done consistency here for readability
                // if (instance of <PartOfSpeachDO>) and if(show) else ...

                if (word instanceof VerbDO) {
                    binding.optionsButton.setVisibility(View.VISIBLE);
                    binding.tableRowGender.setVisibility(View.GONE);
                    binding.tableRowCase1.setVisibility(View.GONE);
                    binding.tableRowCase2.setVisibility(View.GONE);
                    if (show) {
                        if (binding.presentButton.isChecked()) {
                            binding.tableRowPerson.setVisibility(View.VISIBLE);
                            binding.tableRowImperative.setVisibility(View.GONE);
                        }
                        else if(binding.imperativeButton.isChecked()) {
                            binding.tableRowPerson.setVisibility(View.GONE);
                            binding.tableRowImperative.setVisibility(View.VISIBLE);
                        }
                        else {
                            binding.tableRowPerson.setVisibility(View.GONE);
                            binding.tableRowImperative.setVisibility(View.GONE);
                        }
                        binding.tableRowTense.setVisibility(View.VISIBLE);
                    } else {
                        binding.tableRowPerson.setVisibility(View.GONE);
                        binding.tableRowImperative.setVisibility(View.GONE);
                        binding.tableRowTense.setVisibility(View.GONE);
                    }
                } else if (word instanceof AdjectiveDO) {
                    binding.optionsButton.setVisibility(View.VISIBLE);
                    binding.tableRowPerson.setVisibility(View.GONE);
                    binding.tableRowImperative.setVisibility(View.GONE);
                    binding.tableRowTense.setVisibility(View.GONE);
                    if (show) {
                        binding.tableRowGender.setVisibility(View.VISIBLE);
                        binding.tableRowCase1.setVisibility(View.VISIBLE);
                        binding.tableRowCase2.setVisibility(View.VISIBLE);
                    } else {
                        binding.tableRowGender.setVisibility(View.GONE);
                        binding.tableRowCase1.setVisibility(View.GONE);
                        binding.tableRowCase2.setVisibility(View.GONE);
                    }
                } else if (word instanceof NounDO) {
                    binding.optionsButton.setVisibility(View.VISIBLE);
                    binding.tableRowPerson.setVisibility(View.GONE);
                    binding.tableRowImperative.setVisibility(View.GONE);
                    binding.tableRowTense.setVisibility(View.GONE);
                    binding.tableRowGender.setVisibility(View.GONE);
                    if (show) {
                        binding.tableRowCase1.setVisibility(View.VISIBLE);
                        binding.tableRowCase2.setVisibility(View.VISIBLE);
                    } else {
                        binding.tableRowCase1.setVisibility(View.GONE);
                        binding.tableRowCase2.setVisibility(View.GONE);
                    }
                } else {
                    binding.optionsButton.setVisibility(View.GONE);
                    binding.tableRowPerson.setVisibility(View.GONE);
                    binding.tableRowImperative.setVisibility(View.GONE);
                    binding.tableRowTense.setVisibility(View.GONE);
                    binding.tableRowGender.setVisibility(View.GONE);
                    binding.tableRowCase1.setVisibility(View.GONE);
                    binding.tableRowCase2.setVisibility(View.GONE);
                }

            }

            @Override
            public void onSelected() {

                Log.i(LOGTAG, "OnSelected()");
/*
                ObjectAnimator scaleXAnim = ObjectAnimator.ofFloat(mPageIndexText, View.SCALE_X, 0.75f, 1.8f, 1f, 0.8f, 1f);
                ObjectAnimator scaleYAnim = ObjectAnimator.ofFloat(mPageIndexText, View.SCALE_Y, 0.75f, 1.8f, 1f, 0.8f, 1f);


                AnimatorSet animation = new AnimatorSet();
                animation.playTogether(scaleXAnim, scaleYAnim);
                animation.setDuration(600);

                animation.start();
*/
            }


        }

        @Override
        @NonNull
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ViewGroup vg = (ViewGroup) mInflater.inflate(R.layout.fragment_flashcard, parent, false);
            FragmentFlashcardBinding binding = FragmentFlashcardBinding.bind(vg);
            return new ViewHolder(binding);
        }

        @Override
        public int getItemCount() {
            return wordlist.getWords().size();
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            WordDO word = wordlist.getWords().get(position);
            holder.bind(word);
        }
    }

    class PagerSnapHelperVerbose extends PagerSnapHelper implements ViewTreeObserver.OnGlobalLayoutListener {

        RecyclerView recyclerView;
        RVPagerStateListener externalListener;
        int lastPage = RecyclerView.NO_POSITION;

        PagerSnapHelperVerbose(RecyclerView recyclerView, RVPagerStateListener externalListener) {
            super();
            this.recyclerView = recyclerView;
            this.externalListener = externalListener;

            recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        }

        @Override
        public void onGlobalLayout() {
            int position = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
            if (position != RecyclerView.NO_POSITION) {
                notifyNewPageIfNeeded(position);
                recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        }

        @Override
        public View findSnapView(RecyclerView.LayoutManager layoutManager) {
            View view = super.findSnapView(layoutManager);
            notifyNewPageIfNeeded(recyclerView.getChildAdapterPosition(view));
            return view;
        }

        @Override
        public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
            int position = super.findTargetSnapPosition(layoutManager, velocityX, velocityY);

            if (position < recyclerView.getAdapter().getItemCount()) { // Making up for a "bug" in the original snap-helper.
                notifyNewPageIfNeeded(position);
            }
            return position;
        }

        void notifyNewPageIfNeeded(int page) {
            if (page != lastPage) {
                this.externalListener.onPageSelected(page);
                lastPage = page;
            }
        }
    }


}
