package cz.atha.flashcards.activities.wordlist;


import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import cz.atha.flashcards.grammar.AdjectiveDO;
import cz.atha.flashcards.grammar.AdjectiveFormsDO;
import cz.atha.flashcards.grammar.NounDO;
import cz.atha.flashcards.grammar.NounFormsDO;
import cz.atha.flashcards.grammar.VerbDO;
import cz.atha.flashcards.grammar.VerbFormsDO;
import cz.atha.flashcards.dao.AppRepository;
import cz.atha.flashcards.grammar.models.ModelDO;
import cz.atha.flashcards.grammar.models.NounModelDO;
import cz.atha.flashcards.dao.WordlistDO;

public class WordlistViewModel extends AndroidViewModel {

    private AppRepository repository;
    private String LOGTAG = "WordlistViewModel";

    public WordlistViewModel(@NonNull Application application) {
        super(application);
        repository = AppRepository.getInstance(application);
    }


    public LiveData<List<WordlistDO>> getWordlists() {

        return repository.getAllWordlists();
    }

    public LiveData<WordlistDO> getWordlist() {
        return repository.getSelectedWordlist();
    }

    public LiveData<WordlistDO> getSelectedWordlist() {
        return repository.getSelectedWordlist();
    }

    public void loadWordList(long id) {
        repository.loadWordlist(id);
    }

    public void setWordlistActive(long id, boolean checked) {
        repository.setWordlistActive(id, checked);
    }

    AdjectiveFormsDO adjectiveFormsDO = null;

    public void loadAdjectiveForms(AdjectiveDO word){
        if (word.getModel() == null)
        {
            word.setModel(repository.getModel(word));
        }
        if(word.getModel() == null)
            Log.w(LOGTAG, "null model form"+word.getTarget());

        adjectiveFormsDO = new AdjectiveFormsDO(word);
    }

    public AdjectiveFormsDO getAdjectiveFormsDO() {
        return adjectiveFormsDO;
    }

    NounFormsDO nounFormsDO = null;

    public void loadNounForms(NounDO word){
        if (word.getModel() == null)
        {
            NounModelDO model = repository.getModel(word);
            word.setModel(model);
        }
        if(word.getModel() == null)
            Log.w(LOGTAG, "null model form- "+word.getTarget());

        nounFormsDO = new NounFormsDO(word);
    }

    public NounFormsDO getNounFormsDO(){
        return nounFormsDO;
    }

    VerbFormsDO verbFormsDO = null;

    public void loadVerbForms(VerbDO word){
        if (word.getModel() == null)
        {
            ModelDO model = repository.getModel(word);
            word.setModel(model);
        }
        if(word.getModel() == null)
            Log.w(LOGTAG, "null model form"+word.getTarget());

        verbFormsDO = new VerbFormsDO(word);
    }

    public VerbFormsDO getVerbFormsDO() {
        return verbFormsDO;
    }
}
