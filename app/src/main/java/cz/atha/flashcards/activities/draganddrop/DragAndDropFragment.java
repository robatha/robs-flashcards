package cz.atha.flashcards.activities.draganddrop;

import android.content.ClipData;
import android.content.ClipDescription;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import cz.atha.flashcards.R;
import cz.atha.flashcards.databinding.FragmentDragAndDropBinding;


public class DragAndDropFragment extends Fragment implements View.OnDragListener, View.OnTouchListener {

    private static final String LOGTAG = "DragAndDropFragment";

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_drag_and_drop, parent, false);

        FragmentDragAndDropBinding binding = FragmentDragAndDropBinding.bind(root);

        //Find all views and set Tag to all draggable views
        TextView txtVw = binding.lbl;
        //findViewById(R.id.lbl);
        txtVw.setTag("DRAGGABLE TEXTVIEW");
        txtVw.setOnTouchListener(this);
        ImageView imgVw = binding.ingvw;
        imgVw.setTag("ANDROID ICON");
        imgVw.setOnTouchListener(this);
        Button btn = binding.btnDrag;
        btn.setTag("DRAGGABLE BUTTON");
        btn.setOnTouchListener(this);


        binding.letter00.setText("A");
        binding.letter01.setText("B");
        binding.letter02.setText("C");
        binding.letter03.setText("D");
        binding.letter04.setText("E");
        binding.letter05.setText("F");
        binding.letter06.setText("G");
        binding.letter07.setText("H");
        binding.letter08.setText("I");
        binding.letter09.setText("J");

        binding.letter00.setTag("A");
        binding.letter01.setTag("B");
        binding.letter02.setTag("C");
        binding.letter03.setTag("D");
        binding.letter04.setTag("E");
        binding.letter05.setTag("F");
        binding.letter06.setTag("G");
        binding.letter07.setTag("H");
        binding.letter08.setTag("I");
        binding.letter09.setTag("J");

        binding.letter00.setOnTouchListener(this);
        binding.letter01.setOnTouchListener(this);
        binding.letter02.setOnTouchListener(this);
        binding.letter03.setOnTouchListener(this);
        binding.letter04.setOnTouchListener(this);
        binding.letter05.setOnTouchListener(this);
        binding.letter06.setOnTouchListener(this);
        binding.letter07.setOnTouchListener(this);
        binding.letter08.setOnTouchListener(this);
        binding.letter09.setOnTouchListener(this);

        //Set Drag Event Listeners for defined layouts
        binding.layout1.setOnDragListener(this);
        binding.layout2.setOnDragListener(this);
        binding.layout3.setOnDragListener(this);

        return root;
    }

    @Override
    public boolean onTouch(View v, MotionEvent e) {
        if (e.getAction() == MotionEvent.ACTION_DOWN) {
        // Create a new ClipData.Item from the ImageView object's tag
        ClipData.Item item = new ClipData.Item((CharSequence) v.getTag());
        // Create a new ClipData using the tag as a label, the plain text MIME type, and
        // the already-created item. This will create a new ClipDescription object within the
        // ClipData, and set its MIME type entry to "text/plain"
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
        ClipData data = new ClipData(v.getTag().toString(), mimeTypes, item);
        // Instantiates the drag shadow builder.
        View.DragShadowBuilder dragshadow = new View.DragShadowBuilder(v);
        // Starts the drag
        v.startDrag(data        // data to be dragged
                , dragshadow   // drag shadow builder
                , v           // local data about the drag and drop operation
                , 0          // flags (not currently used, set to 0)
        );
        return true;

        } else {
            return false;
        }
    }
    // This is the method that the system calls when it dispatches a drag event to the listener.
    @Override
    public boolean onDrag(View v, DragEvent event) {
        // Defines a variable to store the action type for the incoming event
        int action = event.getAction();
        // Handles each of the expected events
        switch (action) {

            case DragEvent.ACTION_DRAG_STARTED:
                // Determines if this View can accept the dragged data
                if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    // if you want to apply color when drag started to your view you can uncomment below lines
                    // to give any color tint to the View to indicate that it can accept data.
                    // v.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
                    // Invalidate the view to force a redraw in the new tint
                    //  v.invalidate();
                    // returns true to indicate that the View can accept the dragged data.
                    return true;
                }
                // Returns false. During the current drag and drop operation, this View will
                // not receive events again until ACTION_DRAG_ENDED is sent.
                return false;

            case DragEvent.ACTION_DRAG_ENTERED:
                // Applies a GRAY or any color tint to the View. Return true; the return value is ignored.
                v.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                // Invalidate the view to force a redraw in the new tint
                v.invalidate();
                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
                // Ignore the event
                return true;

            case DragEvent.ACTION_DRAG_EXITED:
                // Re-sets the color tint to blue. Returns true; the return value is ignored.
                // view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
                //It will clear a color filter .
                v.getBackground().clearColorFilter();
                // Invalidate the view to force a redraw in the new tint
                v.invalidate();
                return true;

            case DragEvent.ACTION_DROP:
                // Gets the item containing the dragged data
                ClipData.Item item = event.getClipData().getItemAt(0);
                // Gets the text data from the item.
                String dragData = item.getText().toString();
                // Displays a message containing the dragged data.
                Toast.makeText(getActivity(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();
                // Turns off any color tints
                v.getBackground().clearColorFilter();
                // Invalidates the view to force a redraw
                v.invalidate();

                View vw = (View) event.getLocalState();
                ViewGroup owner = (ViewGroup) vw.getParent();
                owner.removeView(vw); //remove the dragged view
                //caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                LinearLayout container = (LinearLayout) v;
                container.addView(vw);//Add the dragged view
                vw.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE
                // Returns true. DragEvent.getResult() will return true.
                return true;

            case DragEvent.ACTION_DRAG_ENDED:
                // Turns off any color tinting
                v.getBackground().clearColorFilter();
                // Invalidates the view to force a redraw
                v.invalidate();
                // Does a getResult(), and displays what happened.
                if (event.getResult())
                    Toast.makeText(getActivity(), "The drop was handled.", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getActivity(), "The drop didn't work.", Toast.LENGTH_SHORT).show();
                // returns true; the value is ignored.
                return true;
            // An unknown action type was received.
            default:
                Log.e(LOGTAG, "Unknown action type received by OnDragListener.");
                break;
        }
        return false;
    }
}
