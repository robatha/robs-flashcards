
package cz.atha.flashcards.activities.decline;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.atha.flashcards.R;
import cz.atha.flashcards.databinding.ViewSelectDeclinationBinding;
import cz.atha.flashcards.grammar.Case;
import cz.atha.flashcards.grammar.Number;
import cz.atha.flashcards.dao.WordlistDO;


public class DeclineSelectFragment extends Fragment {

    final private static String LOGTAG = "DeclineSelectFragment";
    private MyRecyclerViewAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // set up layout, grab params
        View view = inflater.inflate(R.layout.view_select_declination, container, false);

        // view model contains db calls
        DeclineViewModel model = ViewModelProviders.of(getActivity()).get(DeclineViewModel.class);
        ViewSelectDeclinationBinding binding = ViewSelectDeclinationBinding.bind(view);

        model.getWordlistsWithAdjectivesOnly().observe(this, wordlists -> {
            Collections.sort(wordlists);
            if (model.hasOnlyAdjectives()) {
                binding.adjectivesOnly.setChecked(true);
                adapter.setData(wordlists);
                adapter.setSelectedWordList(model.getSelectedWordlist());
                adapter.notifyDataSetChanged();
            }
        });
        model.getWordlistsWithNounsOnly().observe(this, wordlists -> {
            Collections.sort(wordlists);
            if (model.hasOnlyNouns()) {
                binding.nounsOnly.setChecked(true);
                adapter.setData(wordlists);
                adapter.setSelectedWordList(model.getSelectedWordlist());
                adapter.notifyDataSetChanged();
            }
        });
        model.getWordlistsWithAdjectivesAndNouns().observe(this, wordlists -> {
            Collections.sort(wordlists);
            if (model.hasNounsAndAdjectives()) {
                binding.nounsAndAdjectives.setChecked(true);
                adapter.setData(wordlists);
                adapter.setSelectedWordList(model.getSelectedWordlist());
                adapter.notifyDataSetChanged();
            }
        });

        // update view from model
        binding.declensionGen.setChecked(model.getCases().contains(Case.genitive));
        binding.declensionDat.setChecked(model.getCases().contains(Case.dative));
        binding.declensionAcc.setChecked(model.getCases().contains(Case.accusative));
        binding.declensionVoc.setChecked(model.getCases().contains(Case.vocative));
        binding.declensionLoc.setChecked(model.getCases().contains(Case.locative));
        binding.declensionIns.setChecked(model.getCases().contains(Case.instrumental));

        binding.singularAndPlural.setChecked(model.hasSingularAndPlural());
        binding.singularOnly.setChecked(model.hasSingularOnly());
        binding.pluralOnly.setChecked(model.hasPluralOnly());

        // set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new MyRecyclerViewAdapter(getContext());
        adapter.setClickListener(new ItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                //Toast.makeText(parent.getContext(), "You Clicked " + lessons.get(position), Toast.LENGTH_LONG).show();
                Log.i(LOGTAG, "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);

                adapter.setSelectedWordList(adapter.getItem(position));
                model.setSelectedWordlist(adapter.getItem(position));
            }

        });
        recyclerView.setAdapter(adapter);

        binding.startButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(adapter.selectedWordList == null){
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Oops");
                    alertDialog.setMessage("Please select a wordlist first.");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return;
                }

                // load word list now
                model.loadDeclensionWordlist();

                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.your_placeholder, new DeclineFragment(), "declension_activitys_fragment" ).
                        addToBackStack(null)
                        .commit();
            }

        });

        binding.declensionGen.setOnCheckedChangeListener( (buttonView, isChecked) ->{
                if (isChecked) model.addCase(Case.genitive);
                else model.removeCase(Case.genitive);
            }
        );
        binding.declensionDat.setOnCheckedChangeListener( (buttonView, isChecked) ->{
                    if (isChecked) model.addCase(Case.dative);
                    else model.removeCase(Case.dative);
                }
        );
        binding.declensionAcc.setOnCheckedChangeListener( (buttonView, isChecked) ->{
                    if (isChecked) model.addCase(Case.accusative);
                    else model.removeCase(Case.accusative);
                }
        );
        binding.declensionVoc.setOnCheckedChangeListener( (buttonView, isChecked) ->{
                    if (isChecked) model.addCase(Case.vocative);
                    else model.removeCase(Case.vocative);
                }
        );
        binding.declensionLoc.setOnCheckedChangeListener( (buttonView, isChecked) ->{
                    if (isChecked) model.addCase(Case.locative);
                    else model.removeCase(Case.locative);
                }
        );
        binding.declensionIns.setOnCheckedChangeListener( (buttonView, isChecked) ->{
                    if (isChecked) model.addCase(Case.instrumental);
                    else model.removeCase(Case.instrumental);
                }
        );

        binding.numberGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // get selected radio button from radioGroup
                switch(checkedId){
                    case R.id.singular_and_plural:
                        model.addNumber(Number.singular);
                        model.addNumber(Number.plural);
                        break;
                    case R.id.singular_only:
                        model.addNumber(Number.singular);
                        model.removeNumber(Number.plural);
                        break;
                    case R.id.plural_only:
                        model.removeNumber(Number.singular);
                        model.addNumber(Number.plural);
                        break;

                }
            }

        });

        binding.partOfSpeech.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // get selected radio button from radioGroup
                switch(checkedId){
                    case R.id.nouns_and_adjectives:
                        model.addPartofSpeech(DeclineViewModel.PartOfSpeech.noun);
                        model.addPartofSpeech(DeclineViewModel.PartOfSpeech.adjective);

                        adapter.setData(model.getWordlistsWithAdjectivesAndNouns().getValue());
                        adapter.setSelectedWordList(model.getSelectedWordlist());
                        adapter.notifyDataSetChanged();

                        break;

                    case R.id.nouns_only:
                        model.addPartofSpeech(DeclineViewModel.PartOfSpeech.noun);
                        model.removePartofSpeech(DeclineViewModel.PartOfSpeech.adjective);

                        adapter.setData(model.getWordlistsWithNounsOnly().getValue());
                        adapter.setSelectedWordList(model.getSelectedWordlist());
                        adapter.notifyDataSetChanged();

                        break;

                    case R.id.adjectives_only:
                        model.removePartofSpeech(DeclineViewModel.PartOfSpeech.noun);
                        model.addPartofSpeech(DeclineViewModel.PartOfSpeech.adjective);

                        adapter.setData(model.getWordlistsWithAdjectivesOnly().getValue());
                        adapter.setSelectedWordList(model.getSelectedWordlist());
                        adapter.notifyDataSetChanged();

                        break;

                }
            }

        });

        return view;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private List<WordlistDO> wordlists;
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;
        private ViewHolder checkedViewHolder;
        private WordlistDO selectedWordList;

        MyRecyclerViewAdapter(Context context) {
            Log.e(LOGTAG, "GOT TO ADAPTER CONSTRUCTOR");
            this.mInflater = LayoutInflater.from(context);
            wordlists = new ArrayList<>();
        }

        public void setSelectedWordList(WordlistDO selectedWordList) {
            this.selectedWordList = selectedWordList;
        }

        public void setData(List<WordlistDO> data) {
            Log.e(LOGTAG,"setting to data"+data);
            wordlists = data;
        }



        // inflates the cell layout from xml when needed
        @Override
        @NonNull
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.view_select_conjugation_row, parent, false);
            return new ViewHolder(view);
        }

        // binds the data to the TextView in each cell
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            WordlistDO wordlistDO = wordlists.get(position);
            holder.textViewItem.setText(wordlistDO.getName());
            holder.textViewDesc.setText(wordlistDO.getDesc());

            if(selectedWordList != null && wordlistDO.getId() == selectedWordList.getId()) {
                holder.setChecked(true);
            }
            else holder.setChecked(false);


        }
        // total number of cells
        @Override
        public int getItemCount() {
            return wordlists.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView textViewItem;
            TextView textViewDesc;

            ViewHolder(View itemView) {
                super(itemView);
                textViewItem = itemView.findViewById(R.id.textViewItem);
                textViewDesc =  itemView.findViewById(R.id.textViewDesc);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

                // check new
                setChecked(true);
            }

            void setChecked(boolean b){

                if (!b)
                    textViewItem.setCompoundDrawablesWithIntrinsicBounds(0, 0,0,0);
                else {
                    if(checkedViewHolder != null)
                        checkedViewHolder.setChecked(false);
                    checkedViewHolder = this;
                    textViewItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_black_24dp, 0, 0, 0);
                }

            }
        }

        // convenience method for getting data at click position
        WordlistDO getItem(int id) {
            return wordlists.get(id);
        }

        // allows clicks events to be caught
        void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }


    }



}


