package cz.atha.flashcards.activities.draganddrop;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import cz.atha.flashcards.R;

public class DragAndDropActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wordlist);

        if (savedInstanceState == null) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.your_placeholder, new DragAndDropFragment(), "flashcard_activitys_fragment" )
                    .commit();
        } else {
            Fragment test = getSupportFragmentManager().findFragmentByTag("flashcard_activitys_fragment");
        }

    }
}
