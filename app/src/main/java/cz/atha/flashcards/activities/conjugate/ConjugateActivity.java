package cz.atha.flashcards.activities.conjugate;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import cz.atha.flashcards.R;

public class ConjugateActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wordlist);

        if (savedInstanceState == null) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.your_placeholder, new ConjugateSelectFragment(), "declension_activitys_fragment" )
//                    .addToBackStack(null)
                    .commit();
        } else {
            Fragment test = getSupportFragmentManager().findFragmentByTag("declension_activitys_fragment");
        }

    }
}
