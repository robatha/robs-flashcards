package cz.atha.flashcards.activities.flashcard;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import cz.atha.flashcards.R;
import cz.atha.flashcards.databinding.FragmentFlashcardBinding;
import cz.atha.flashcards.grammar.AdjectiveDO;
import cz.atha.flashcards.grammar.Case;
import cz.atha.flashcards.grammar.NounDO;
import cz.atha.flashcards.grammar.Number;
import cz.atha.flashcards.grammar.Person;
import cz.atha.flashcards.grammar.VerbDO;
import cz.atha.flashcards.grammar.WordDO;

import cz.atha.flashcards.dao.WordlistDO;

import cz.atha.flashcards.activities.flashcard.FlashcardViewModel.ConjugationType;
import cz.atha.flashcards.activities.flashcard.FlashcardViewModel.GramaticalGender;
import cz.atha.flashcards.activities.flashcard.FlashcardViewModel.ImperativeForm;

public class FlashcardFragment extends Fragment {

    private static final String LOGTAG = "DeclineFragment";


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_main, parent, false);
        RecyclerView rv = root.findViewById(R.id.recycler_view);

        rv.setLayoutManager(new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false));

        FlashcardViewModel model = ViewModelProviders.of(getActivity()).get(FlashcardViewModel.class);
        model.getWordlist().observe(this, wordlist -> {
            rv.setAdapter(new MyRecyclerViewAdapter(wordlist));
        });

        PagerSnapHelperVerbose verbose = new PagerSnapHelperVerbose(rv, new RVPagerStateListener() {

            @Override
            public void onPageSelected(int index) {
                SelectablePage vh = (SelectablePage) rv.findViewHolderForAdapterPosition(index);

                if (vh != null)
                    vh.onSelected();
            }
        });
        verbose.attachToRecyclerView(rv);

        return root;
    }

    interface RVPagerStateListener {
        void onPageSelected(int index);
    }

    interface SelectablePage {
        void onSelected();
    }

    interface ButtonGroupListener<T> {
        public void onButtonGroupValueChanged(T value);
    }

    class ButtonGroup<T> {

        private Map<T, CompoundButton> buttons;
        private T currentValue;
        private ButtonGroupListener<T> listener;

        ButtonGroup() {
            buttons = new HashMap<>();
            currentValue = null;
        }

        void setButtonGroupListener(ButtonGroupListener<T> l) {
            listener = l;
        }

        void put(T obj, CompoundButton button) {
            buttons.put(obj, button);
            button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    T valueOfButton = null;

                    // get case of button
                    for (T v : buttons.keySet()) {
                        if (buttons.get(v) == buttonView) {
                            valueOfButton = v;
                            break;
                        }
                    }

                    // prevent toggle off
                    if ((!isChecked) && valueOfButton == currentValue) {
                        buttonView.setChecked(true);
                        return;
                    }

                    // legit button press
                    if (isChecked) {
                        // set modifier(s)
                        currentValue = valueOfButton;

                        // switch off other buttons
                        for (CompoundButton b : buttons.values()) {
                            if (!b.equals(buttonView)) b.setChecked(false);
                        }

                        // change answer
                        if (listener != null)
                            listener.onButtonGroupValueChanged(currentValue);
                    }
                }
            });
        }

        void setValue(T value) {
            for (T v : buttons.keySet()) {

                CompoundButton b = buttons.get(v);
                if (b != null) {
                    b.setChecked(v.equals(value));
                }
            }
            currentValue = value;
        }

    }

    class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private LayoutInflater mInflater;
        private WordlistDO wordlist;
        FlashcardViewModel model;

        MyRecyclerViewAdapter(WordlistDO wordlistDO) {
            model = ViewModelProviders.of(getActivity()).get(FlashcardViewModel.class);
            this.wordlist = wordlistDO;
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            mInflater = LayoutInflater.from(recyclerView.getContext());
        }

        @Override
        public void onDetachedFromRecyclerView(@NotNull RecyclerView recyclerView) {
            mInflater = null;
        }

        class ViewHolder extends RecyclerView.ViewHolder implements SelectablePage {

            private FragmentFlashcardBinding binding;
            private WordDO word;

            public ViewHolder(@NonNull FragmentFlashcardBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }


            public void bind(WordDO word) {
                this.word = word;
                binding.textViewQuestion.setText(word.getMeaning());
                binding.lastResult.setText(model.getForm(word));

                if (wordlist.getSrc() == null) {
                    Log.w(LOGTAG, "WordList.src is null, cannot find image.");
                } else if (word.getImage() == null) {
                    Log.w(LOGTAG, "Word.image is null so can't retrieve image.");
                } else {
                    File resourceDir = new File(wordlist.getSrc()).getParentFile();
                    File imageFile = new File(resourceDir, word.getImage());
                    try {
                        InputStream istream = new BufferedInputStream(new FileInputStream(imageFile));
                        Drawable drawable = new BitmapDrawable(getResources(), BitmapFactory.decodeStream(istream));
                        binding.imageViewQuestion.setImageDrawable(drawable);
                    } catch (IOException e) {
                        Log.e(LOGTAG, "Cannot open bitmap: " + imageFile.getAbsolutePath());
                    }
                }
                binding.lastResult.setVisibility(View.INVISIBLE);
                binding.showButton.setChecked(false);
                binding.showButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked)
                            binding.lastResult.setVisibility(View.VISIBLE);
                        else binding.lastResult.setVisibility(View.INVISIBLE);
                    }
                });
                // TODO support audio
                if (word.getAudio() == null)
                    binding.imageAudio.setVisibility(View.GONE);
                showGrammarOptions(false);
                binding.optionsButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
                        showGrammarOptions(isChecked);
                });

                ButtonGroup<Case> caseButtonGroup = new ButtonGroup();
                caseButtonGroup.put(Case.nominative, binding.nominativeButton);
                caseButtonGroup.put(Case.genitive, binding.genitiveButton);
                caseButtonGroup.put(Case.dative, binding.dativeButton);
                caseButtonGroup.put(Case.accusative, binding.accusativeButton);
                caseButtonGroup.put(Case.vocative, binding.vocativeButton);
                caseButtonGroup.put(Case.locative, binding.locativeButton);
                caseButtonGroup.put(Case.instrumental, binding.instrumentalButton);
                caseButtonGroup.setValue(model.getCase());
                caseButtonGroup.setButtonGroupListener(value -> {
                    // set modifier(s)
                    model.setCase(value);
                    // change answer
                    binding.lastResult.setText(model.getForm(word));
                });

                // listener used for two different buttons
                final CompoundButton.OnCheckedChangeListener numberChangeListener
                        = (buttonView, isChecked) -> {
                    // change model
                    if (isChecked)
                        model.setNumber(Number.plural);
                    else
                        model.setNumber(Number.singular);
                    // change answer
                    binding.lastResult.setText(model.getForm(word));
                };

                binding.pluralCaseButton.setOnCheckedChangeListener(numberChangeListener);
                binding.pluralCaseButton.setChecked(model.getNumber() == Number.plural);
                binding.pluralVerbButton.setOnCheckedChangeListener(numberChangeListener);
                binding.pluralVerbButton.setChecked(model.getNumber() == Number.plural);

                ButtonGroup<ConjugationType> conjugationTypeButtonGroup = new ButtonGroup<>();
                conjugationTypeButtonGroup.put(ConjugationType.infinitive, binding.infinitiveButton);
                conjugationTypeButtonGroup.put(ConjugationType.present, binding.presentButton);
                conjugationTypeButtonGroup.put(ConjugationType.lform, binding.lformButton);
                conjugationTypeButtonGroup.put(ConjugationType.imperative, binding.imperativeButton);
                conjugationTypeButtonGroup.setValue(model.getVerbConjugationType());
                conjugationTypeButtonGroup.setButtonGroupListener(value -> {

                    // set modifier(s)
                    model.setVerbConjugationType(value);

                    // change to valid types of buttons
                    if (value == ConjugationType.lform) {
                        binding.pluralVerbButton.setChecked(false);
                        binding.maleGenderInanimateButton.setChecked(true);
                    }
                    if (value == ConjugationType.imperative) {
                        binding.secondButton.setChecked(true);
                        binding.pluralVerbButton.setChecked(false);
                        binding.tyButton.setChecked(true);
                    }

                    // change answer
                    binding.lastResult.setText(model.getForm(word));
                    showGrammarOptions(true);

                });

                ButtonGroup<GramaticalGender> genderButtonGroup = new ButtonGroup<>();
                genderButtonGroup.put(GramaticalGender.maleAnimate, binding.maleGenderAnimateButton);
                genderButtonGroup.put(GramaticalGender.maleInanimate, binding.maleGenderInanimateButton);
                genderButtonGroup.put(GramaticalGender.female, binding.femaleGenderButton);
                genderButtonGroup.put(GramaticalGender.neuter, binding.neuterGenderButton);
                genderButtonGroup.setValue(model.getGramaticalGender());
                genderButtonGroup.setButtonGroupListener(value -> {
                    // set modifier
                    model.setGramaticalGender(value);
                    // change answer
                    binding.lastResult.setText(model.getForm(word));
                });

                ButtonGroup<Person> personButtonGroup = new ButtonGroup<>();
                personButtonGroup.put(Person.first, binding.firstButton);
                personButtonGroup.put(Person.second, binding.secondButton);
                personButtonGroup.put(Person.third, binding.thirdButton);
                personButtonGroup.setValue(model.getPerson());
                personButtonGroup.setButtonGroupListener(value -> {
                    // set modifier
                    model.setPerson(value);
                    // change answer
                    binding.lastResult.setText(model.getForm(word));
                });

                ButtonGroup<ImperativeForm> imperativeButtonGroup = new ButtonGroup<>();
                imperativeButtonGroup.put(ImperativeForm.my, binding.myButton);
                imperativeButtonGroup.put(ImperativeForm.ty, binding.tyButton);
                imperativeButtonGroup.put(ImperativeForm.vy, binding.vyButton);
                imperativeButtonGroup.setValue(model.getImperativeForm());
                imperativeButtonGroup.setButtonGroupListener(value -> {
                    // set modifier
                    model.setImperativeForm(value);
                    // change answer
                    binding.lastResult.setText(model.getForm(word));
                });

            }

            void showGrammarOptions(boolean show) {
                // I know I can simplify the logic but it's done consistency here for readability
                // if (instance of <PartOfSpeachDO>) and if(show) else ...

                if (word instanceof VerbDO) {
                    binding.optionsButton.setVisibility(View.VISIBLE);
                    binding.tableRowGender.setVisibility(View.GONE);
                    binding.tableRowCase1.setVisibility(View.GONE);
                    binding.tableRowCase2.setVisibility(View.GONE);
                    if (show) {
                        if (binding.presentButton.isChecked()) {
                            binding.tableRowPerson.setVisibility(View.VISIBLE);
                            binding.tableRowImperative.setVisibility(View.GONE);
                        } else if (binding.imperativeButton.isChecked()) {
                            binding.tableRowPerson.setVisibility(View.GONE);
                            binding.tableRowImperative.setVisibility(View.VISIBLE);
                        } else {
                            binding.tableRowPerson.setVisibility(View.GONE);
                            binding.tableRowImperative.setVisibility(View.GONE);
                        }
                        binding.tableRowTense.setVisibility(View.VISIBLE);
                    } else {
                        binding.tableRowPerson.setVisibility(View.GONE);
                        binding.tableRowImperative.setVisibility(View.GONE);
                        binding.tableRowTense.setVisibility(View.GONE);
                    }
                } else if (word instanceof AdjectiveDO) {
                    binding.optionsButton.setVisibility(View.VISIBLE);
                    binding.tableRowPerson.setVisibility(View.GONE);
                    binding.tableRowImperative.setVisibility(View.GONE);
                    binding.tableRowTense.setVisibility(View.GONE);
                    if (show) {
                        binding.tableRowGender.setVisibility(View.VISIBLE);
                        binding.tableRowCase1.setVisibility(View.VISIBLE);
                        binding.tableRowCase2.setVisibility(View.VISIBLE);
                    } else {
                        binding.tableRowGender.setVisibility(View.GONE);
                        binding.tableRowCase1.setVisibility(View.GONE);
                        binding.tableRowCase2.setVisibility(View.GONE);
                    }
                } else if (word instanceof NounDO) {
                    binding.optionsButton.setVisibility(View.VISIBLE);
                    binding.tableRowPerson.setVisibility(View.GONE);
                    binding.tableRowImperative.setVisibility(View.GONE);
                    binding.tableRowTense.setVisibility(View.GONE);
                    binding.tableRowGender.setVisibility(View.GONE);
                    if (show) {
                        binding.tableRowCase1.setVisibility(View.VISIBLE);
                        binding.tableRowCase2.setVisibility(View.VISIBLE);
                    } else {
                        binding.tableRowCase1.setVisibility(View.GONE);
                        binding.tableRowCase2.setVisibility(View.GONE);
                    }
                } else {
                    binding.optionsButton.setVisibility(View.GONE);
                    binding.tableRowPerson.setVisibility(View.GONE);
                    binding.tableRowImperative.setVisibility(View.GONE);
                    binding.tableRowTense.setVisibility(View.GONE);
                    binding.tableRowGender.setVisibility(View.GONE);
                    binding.tableRowCase1.setVisibility(View.GONE);
                    binding.tableRowCase2.setVisibility(View.GONE);
                }

            }

            @Override
            public void onSelected() {

                Log.i(LOGTAG, "OnSelected()");
/*
                ObjectAnimator scaleXAnim = ObjectAnimator.ofFloat(mPageIndexText, View.SCALE_X, 0.75f, 1.8f, 1f, 0.8f, 1f);
                ObjectAnimator scaleYAnim = ObjectAnimator.ofFloat(mPageIndexText, View.SCALE_Y, 0.75f, 1.8f, 1f, 0.8f, 1f);


                AnimatorSet animation = new AnimatorSet();
                animation.playTogether(scaleXAnim, scaleYAnim);
                animation.setDuration(600);

                animation.start();
*/
            }


        }

        @Override
        @NonNull
        public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            ViewGroup vg = (ViewGroup) mInflater.inflate(R.layout.fragment_flashcard, parent, false);
            FragmentFlashcardBinding binding = FragmentFlashcardBinding.bind(vg);
            return new ViewHolder(binding);
        }

        @Override
        public int getItemCount() {
            return wordlist.getWords().size();
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            WordDO word = wordlist.getWords().get(position);
            holder.bind(word);
        }
    }

    class PagerSnapHelperVerbose extends PagerSnapHelper implements ViewTreeObserver.OnGlobalLayoutListener {

        RecyclerView recyclerView;
        RVPagerStateListener externalListener;
        int lastPage = RecyclerView.NO_POSITION;

        PagerSnapHelperVerbose(RecyclerView recyclerView, RVPagerStateListener externalListener) {
            super();
            this.recyclerView = recyclerView;
            this.externalListener = externalListener;

            recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        }

        @Override
        public void onGlobalLayout() {
            int position = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
            if (position != RecyclerView.NO_POSITION) {
                notifyNewPageIfNeeded(position);
                recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        }

        @Override
        public View findSnapView(RecyclerView.LayoutManager layoutManager) {
            View view = super.findSnapView(layoutManager);
            notifyNewPageIfNeeded(recyclerView.getChildAdapterPosition(view));
            return view;
        }

        @Override
        public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
            int position = super.findTargetSnapPosition(layoutManager, velocityX, velocityY);

            if (position < recyclerView.getAdapter().getItemCount()) { // Making up for a "bug" in the original snap-helper.
                notifyNewPageIfNeeded(position);
            }
            return position;
        }

        void notifyNewPageIfNeeded(int page) {
            if (page != lastPage) {
                this.externalListener.onPageSelected(page);
                lastPage = page;
            }
        }
    }


}
