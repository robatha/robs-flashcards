package cz.atha.flashcards.activities.lesson;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import cz.atha.flashcards.dao.AppRepository;
import cz.atha.flashcards.dao.LessonDO;

public class LessonViewModel extends AndroidViewModel {

    private static final String LOGTAG = "LessonViewModel";
    private AppRepository repository;

    public LessonViewModel(@NonNull Application application) {
        super(application);
        repository = AppRepository.getInstance(application);
    }


    public LiveData<LessonDO> getSelectedLesson() {
        return repository.getSelectedLesson();
    }

}
