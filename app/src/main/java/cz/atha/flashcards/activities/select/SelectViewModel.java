package cz.atha.flashcards.activities.select;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import cz.atha.flashcards.dao.AppRepository;
import cz.atha.flashcards.dao.LessonDO;
import cz.atha.flashcards.dao.ArticleDO;
import cz.atha.flashcards.dao.WordlistDO;

public class SelectViewModel extends AndroidViewModel {

    private AppRepository repository;

    public SelectViewModel(@NonNull Application application) {
        super(application);
        repository = AppRepository.getInstance(application);
    }

    public LiveData<List<ArticleDO>> getArticles() {
        return repository.getAllArticles();
    }

    public LiveData<List<LessonDO>> getLessons() {
        return repository.getAllLessons();
    }


    public LiveData<List<WordlistDO>> getWordlists() {

        return repository.getAllWordlists();
    }


    public void setArticleActive(long id, boolean checked) {
        repository.setArticleActive(id, checked);
    }

    public void loadArticle(long id) {
        repository.loadArticle(id);
    }


    public void loadLesson(long id) {
        repository.loadLesson(id);
    }

    public void loadWordList(long id) {
        repository.loadWordlist(id);
    }
}
