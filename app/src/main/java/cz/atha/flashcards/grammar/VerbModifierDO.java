/**
 * 
 */
package cz.atha.flashcards.grammar;

/**
 * @author rob
 * 
 */
public class VerbModifierDO implements Mood.HasMood, Tense.HasTense, Person.HasPerson,
		Number.HasNumber, Gender.HasGender, Animation.HasAnimation, Voice.HasVoice  {

	// dynamic properties of this word, causes it to mutate form
	private Voice voice = Voice.unknown;
	private Mood mood = Mood.unknown;
	private Tense tense = Tense.unknown;
	private Person person = Person.unknown;
	private Number number = Number.unknown;
	private Gender gender = Gender.unknown;
	private Animation animate = Animation.unknown;


	@Override
	public Voice getVoice() {
		return this.voice;
	}

	@Override
	public void setVoice(Voice v) {
		this.voice = v;
	}

	@Override
	public Mood getMood() {
		return this.mood;
	}

	@Override
	public void setMood(Mood m) {
		this.mood = m;
	}

	@Override
	public Tense getTense() {
		return this.tense;
	}

    @Override
    public void setTense(Tense t) {
		this.tense = t;
	}

	@Override
	public Person getPerson() {
		return person;
	}

    @Override
    public void setPerson(Person p) {
		this.person = p;
	}

	public VerbModifierDO withPerson(Person p){
		setPerson(p);
		return this;
	}

	@Override
	public Number getNumber() {
		return number;
	}

    @Override
	public void setNumber(Number n) {
		this.number = n;
	}

	@Override
	public Gender getGender() {
		return this.gender;
	}

    @Override
	public void setGender(Gender g) {
		this.gender = g;
	}

	@Override
	public Animation getAnimate(){ return this.animate; }

    @Override
	public void setAnimate(Animation a) {
		this.animate = a;
	}

	// so far we can modify this using a pronoun or possibly a noun
	public VerbModifierDO fromWord(WordDO word){
		if(word instanceof Gender.HasGender){
			this.gender = ((Gender.HasGender)word).getGender();
		}
		if(word instanceof Animation.HasAnimation){
			this.animate = ((Animation.HasAnimation)word).getAnimate();
		}
		if(word instanceof Person.HasPerson){
			this.person = ((Person.HasPerson)word).getPerson();
		}
		if(word instanceof Number.HasNumber){
			this.number = ((Number.HasNumber)word).getNumber();
		}
		return this;
	}

	@Override
	public String toString() {
		return "VerbModifierDO{" +
				" voice=" + voice +
				", mood=" + mood +
				", tense=" + tense +
				", person=" + person +
				", number=" + number +
				", gender=" + gender +
				", animate=" + animate +
				'}';
	}
}
