package cz.atha.flashcards.grammar;

import android.os.Parcel;
import android.util.Log;

import cz.atha.flashcards.grammar.models.AdjectiveModelDO;
import cz.atha.flashcards.grammar.models.AdjectiveModelFormDO;
import cz.atha.flashcards.grammar.models.HasModel;
import cz.atha.flashcards.grammar.models.ModelDO;


import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author rob
 */
public class AdjectiveDO implements WordDO, HasModel, Cloneable {

    final private String LOGTAG = "AdjectiveDO";

    // used for every word
    private String target = null;
    private String meaning = null;
    private String image = null;
    private String audio = null;
    private List<String> relatedWords = null;
    private List<String> examples = null;

    // TODO should hardcode model in adjectives?
    private AdjectiveModelDO model = null;

    public static final Creator CREATOR = new Creator() {
        public AdjectiveDO createFromParcel(Parcel in) {
            AdjectiveDO vo = new AdjectiveDO();
            vo.target = in.readString();
            vo.meaning = in.readString();
            vo.image = in.readString();
            vo.audio = in.readString();
            vo.relatedWords = in.createStringArrayList();
            vo.examples = in.createStringArrayList();

            return vo;
        }

        public AdjectiveDO[] newArray(int size) {
            return new AdjectiveDO[size];
        }

    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.target);
        dest.writeString(this.meaning);
        dest.writeString(this.image);
        dest.writeString(this.audio);
        dest.writeStringList(this.relatedWords);
        dest.writeStringList(this.examples);
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public String getTarget() {
        return this.target;
    }

    @Override
    public void setTarget(String t) {
        this.target = t;
    }

    @Override
    public String getMeaning() {
        return this.meaning;
    }

    @Override
    public void setMeaning(String m) {
        this.meaning = m;
    }

    @Override
    public String getImage() {
        return this.image;
    }

    @Override
    public void setImage(String uri) {
        this.image = uri;
    }

    @Override
    public String getAudio() {
        return this.audio;
    }

    @Override
    public void setAudio(String a) {
        this.audio = a;
    }

    @Override
    public List<String> getRelatedWordIds() {
        return relatedWords;
    }

    @Override
    public void setRelatedWordIds(List<String> relatedWords) {
        this.relatedWords = relatedWords;
    }

    @Override
    public List<String> getExamples() {
        return examples;
    }

    @Override
    public void setExamples(List<String> examples) {
        this.examples = examples;
    }

    @Override
    public ModelDO getModel() {
        return model;
    }

    @Override
    public void setModel(ModelDO model) {
        this.model = (AdjectiveModelDO)model;
    }

    @Override
    public String getForm(Object modifier) {

        String dictionaryForm = getTarget();

        // in case we try to get form from word with no model!
        if (model == null)
            return null;

        Log.d(LOGTAG, "Calling getForm() dictionaryForm=" + dictionaryForm);
        Log.d(LOGTAG, "Calling getForm() modifier=" + modifier.toString());

        AdjectiveModelFormDO foundForm = null;
        for (AdjectiveModelFormDO form : model.getForms()) {
            if (form.isCorrectForm(modifier)) {
                foundForm = form;
                break;
            }
        }

        // didn't find appropriate form
        if (foundForm == null)
            return null;
        Log.d(LOGTAG, "found form=" + foundForm.toString());

        // get replace string
        String replaceString = foundForm.getReplaceString();
        if (replaceString == null) {
            Log.e(LOGTAG, "No replace string: " + modifier.toString());
            return null;
        }
        Log.d(LOGTAG, "found replace string: " + replaceString);

        // replace with stem
        Pattern p = Pattern.compile(model.getRegex());
        Matcher m = p.matcher(dictionaryForm);

        // if no match or not enough groups to make new form, keep looking
        if (!m.matches()) {
            Log.e(LOGTAG, "no match: " + p.pattern());
            return null;
        }
        int replaceGroupsCount = replaceString.split("$").length - 1;
        if (m.groupCount() < replaceGroupsCount) {
            Log.e(LOGTAG, "no groupcountmatch: " + m.groupCount() + " vs "
                    + replaceGroupsCount);
            return null;
        }
        return m.replaceAll(replaceString);
    }

    @Override
    public String toString() {
        return "AdjectiveDO{" +
                "LOGTAG='" + LOGTAG + '\'' +
                ", target='" + target + '\'' +
                ", meaning='" + meaning + '\'' +
                ", image='" + image + '\'' +
                ", audio='" + audio + '\'' +
                ", relatedWords=" + relatedWords +
                ", examples=" + examples +
                ", model=" + model +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        WordDO copy = (WordDO)super.clone();
        copy.setRelatedWordIds(new ArrayList<>(copy.getRelatedWordIds()));
        copy.setExamples(new ArrayList<>(copy.getExamples()));
        return copy;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        AdjectiveDO compare = (AdjectiveDO)obj;

        if (target == null){
            if (compare.target != null) return false;
        } else if (!target.equals(compare.target)) return false;

        if (meaning == null){
            if (compare.meaning != null) return false;
        } else if (!meaning.equals(compare.meaning)) return false;

        if (image == null){
            if (compare.image != null) return false;
        } else if (!image.equals(compare.image)) return false;

        if (audio == null){
            if (compare.audio != null) return false;
        } else if (!audio.equals(compare.audio)) return false;
        // TODO disregard model for now?

        return true;
    }

    @Override
    public int hashCode(){

        int result = 17;
        if(target != null)
        result = 31 * result + target.hashCode();
        if(meaning != null)
        result = 31 * result + meaning.hashCode();
        if (image != null)
        result = 31 * result + image.hashCode();
        if (audio != null)
            result = 31 * result + audio.hashCode();
        // TODO disregard model for now?

        return result;

    }

    @Override
    public int compareTo(WordDO o) {
        Collator coll = Collator.getInstance(Locale.forLanguageTag("cs-CZ"));
        coll.setStrength(Collator.PRIMARY);

        return coll.compare(this.target, o.getTarget());
    }
}
