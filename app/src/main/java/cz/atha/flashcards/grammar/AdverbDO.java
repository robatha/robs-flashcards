package cz.atha.flashcards.grammar;

import android.os.Parcel;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author rob
 */
public class AdverbDO implements WordDO {

    private String target = null;
    private String meaning = null;
    private String image = null;
    private String audio = null;
    private List<String> relatedWords = null;
    private List<String> examples = null;

    public static final Creator CREATOR = new Creator() {
        public AdverbDO createFromParcel(Parcel in) {
            AdverbDO vo = new AdverbDO();
            vo.target = in.readString();
            vo.meaning = in.readString();
            vo.image = in.readString();
            vo.audio = in.readString();
            vo.relatedWords = in.createStringArrayList();
            vo.examples = in.createStringArrayList();

            return vo;
        }

        public AdverbDO[] newArray(int size) {
            return new AdverbDO[size];
        }

    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.target);
        dest.writeString(this.meaning);
        dest.writeString(this.image);
        dest.writeString(this.audio);
        dest.writeStringList(this.relatedWords);
        dest.writeStringList(this.examples);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String getTarget() {
        return this.target;
    }

    @Override
    public void setTarget(String t) {
        this.target = t;
    }

    @Override
    public String getMeaning() {
        return this.meaning;
    }

    @Override
    public void setMeaning(String m) {
        this.meaning = m;
    }

    @Override
    public String getImage() {
        return this.image;
    }

    @Override
    public void setImage(String i) {
        this.image = i;
    }

    @Override
    public String getAudio() {
        return this.audio;
    }

    @Override
    public void setAudio(String a) {
        this.audio = a;
    }

    @Override
    public List<String> getRelatedWordIds() {
        return relatedWords;
    }

    @Override
    public void setRelatedWordIds(List<String> relatedWords) {
        this.relatedWords = relatedWords;
    }

    @Override
    public List<String> getExamples() {
        return examples;
    }

    @Override
    public void setExamples(List<String> examples) {
        this.examples = examples;
    }

    @Override
    public String toString() {
        return String.format("ExpressionDO [ target=%s, meaning=%s, image=%s, audio=%s ]",
                target, meaning, image, audio);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        WordDO copy = (WordDO)super.clone();
        copy.setRelatedWordIds(new ArrayList<>(copy.getRelatedWordIds()));
        copy.setExamples(new ArrayList<>(copy.getExamples()));
        return copy;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        AdverbDO compare = (AdverbDO) obj;

        if (target == null) {
            if (compare.target != null) return false;
        } else if (!target.equals(compare.target)) return false;

        if (meaning == null) {
            if (compare.meaning != null) return false;
        } else if (!meaning.equals(compare.meaning)) return false;

        if (image == null) {
            if (compare.image != null) return false;
        } else if (!image.equals(compare.image)) return false;

        if (audio == null) {
            if (compare.audio != null) return false;
        } else if (!audio.equals(compare.audio)) return false;

        return true;
    }

    @Override
    public int hashCode() {

        int result = 17;
        if (target != null)
            result = 31 * result + target.hashCode();
        if (meaning != null)
            result = 31 * result + meaning.hashCode();
        if (image != null)
            result = 31 * result + image.hashCode();
        if (audio != null)
            result = 31 * result + audio.hashCode();

        return result;

    }

    @Override
    public int compareTo(WordDO o) {
        Collator coll = Collator.getInstance(Locale.forLanguageTag("cs-CZ"));
        coll.setStrength(Collator.PRIMARY);

        return coll.compare(this.target, o.getTarget());
    }
}
