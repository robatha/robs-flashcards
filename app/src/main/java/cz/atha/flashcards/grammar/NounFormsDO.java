package cz.atha.flashcards.grammar;

public class NounFormsDO {

    private NounDO noun;
    private String nominitiveSingular;
    private String nominitivePlural;
    private String genitiveSingular;
    private String genitivePlural;
    private String dativeSingular;
    private String dativePlural;
    private String accusitiveSingular;
    private String accusitivePlural;
    private String vocativeSingular;
    private String vocativePlural;
    private String locativeSingular;
    private String locativePlural;
    private String instrumentalSingular;
    private String instrumentalPlural;

    public NounDO getNoun() {
        return noun;
    }

    public String getNominitiveSingular() {
        return nominitiveSingular;
    }

    public String getNominitivePlural() {
        return nominitivePlural;
    }

    public String getGenitiveSingular() {
        return genitiveSingular;
    }

    public String getGenitivePlural() {
        return genitivePlural;
    }

    public String getDativeSingular() {
        return dativeSingular;
    }

    public String getDativePlural() {
        return dativePlural;
    }

    public String getAccusitiveSingular() {
        return accusitiveSingular;
    }

    public String getAccusitivePlural() {
        return accusitivePlural;
    }

    public String getVocativeSingular() {
        return vocativeSingular;
    }

    public String getVocativePlural() {
        return vocativePlural;
    }

    public String getLocativeSingular() {
        return locativeSingular;
    }

    public String getLocativePlural() {
        return locativePlural;
    }

    public String getInstrumentalSingular() {
        return instrumentalSingular;
    }

    public String getInstrumentalPlural() {
        return instrumentalPlural;
    }

    public NounFormsDO(NounDO noun){
        this.noun = noun;
        NounModifierDO modifier = new NounModifierDO();

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.nominative);
        this.nominitiveSingular = noun.getForm(modifier);
        modifier.setNumber(Number.plural);
        this.nominitivePlural = noun.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.genitive);
        this.genitiveSingular = noun.getForm(modifier);
        modifier.setNumber(Number.plural);
        this.genitivePlural = noun.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.dative);
        this.dativeSingular = noun.getForm(modifier);
        modifier.setNumber(Number.plural);
        this.dativePlural = noun.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.accusative);
        this.accusitiveSingular = noun.getForm(modifier);
        modifier.setNumber(Number.plural);
        this.accusitivePlural = noun.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.vocative);
        this.vocativeSingular = noun.getForm(modifier);
        modifier.setNumber(Number.plural);
        this.vocativePlural = noun.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.locative);
        this.locativeSingular = noun.getForm(modifier);
        modifier.setNumber(Number.plural);
        this.locativePlural = noun.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.instrumental);
        this.instrumentalSingular = noun.getForm(modifier);
        modifier.setNumber(Number.plural);
        this.instrumentalPlural = noun.getForm(modifier);
    }
}
