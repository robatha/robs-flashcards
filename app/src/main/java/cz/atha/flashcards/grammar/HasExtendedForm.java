package cz.atha.flashcards.grammar;

public interface HasExtendedForm {
	/**
	 * @return the animate
	 */
	String getExtendedForm();
    void setExtendedForm(String e);
}
