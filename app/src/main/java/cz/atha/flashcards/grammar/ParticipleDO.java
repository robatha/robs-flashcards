/**
 * 
 */
package cz.atha.flashcards.grammar;

/**
 * @author rob
 * 
 */
public class ParticipleDO implements Gender.HasGender, Number.HasNumber {

	private Gender gender = Gender.unknown;
	private Number number = Number.unknown;

	private String defaultForm;
	private String maleSingularForm;
	private String femaleSingularForm;
	private String neuterSingularForm;
	private String malePluralForm;
	private String femalePluralForm;
	private String neuterPluralForm;

	public String toString() {
		switch (this.getGender()) {

		case female:
			if (this.number.equals(Number.plural)
					&& this.femalePluralForm != null) {
				return this.femalePluralForm;
			} else if (this.femaleSingularForm != null) {
				return this.femaleSingularForm;
			} else
				return this.defaultForm;
		case neuter:
			if (this.number.equals(Number.plural)
					&& this.neuterPluralForm != null) {
				return this.neuterPluralForm;
			} else if (this.neuterSingularForm != null) {
				return this.neuterSingularForm;
			} else
				return this.defaultForm;
		case male:
			if (this.number.equals(Number.plural)
					&& this.malePluralForm != null) {
				return this.malePluralForm;
			} else if (this.maleSingularForm != null) {
				return this.maleSingularForm;
			} else
				return this.defaultForm;
		default:
			return defaultForm;
		}
	}

	@Override
	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender g) {
		this.gender = g;
	}

	@Override
	public Number getNumber() {
		return number;
	}

	public void setNumber(Number n) {
		this.number = n;
	}

	/**
	 * @return the defaultForm
	 */
	public String getDefaultForm() {
		return this.defaultForm;
	}

	/**
	 * @param defaultForm
	 *            the defaultForm to set
	 */
	public void setDefaultForm(String defaultForm) {
		this.defaultForm = defaultForm;
	}

	/**
	 * @return the maleSingularForm
	 */
	public String getMaleSingularForm() {
		return maleSingularForm;
	}

	/**
	 * @param maleSingularForm
	 *            the maleSingularForm to set
	 */
	public void setMaleSingularForm(String maleSingularForm) {
		this.maleSingularForm = maleSingularForm;
	}

	/**
	 * @return the femaleSingularForm
	 */
	public String getFemaleSingularForm() {
		return femaleSingularForm;
	}

	/**
	 * @param femaleSingularForm
	 *            the femaleSingularForm to set
	 */
	public void setFemaleSingularForm(String femaleSingularForm) {
		this.femaleSingularForm = femaleSingularForm;
	}

	/**
	 * @return the neuterSingularForm
	 */
	public String getNeuterSingularForm() {
		return neuterSingularForm;
	}

	/**
	 * @param neuterSingularForm
	 *            the neuterSingularForm to set
	 */
	public void setNeuterSingularForm(String neuterSingularForm) {
		this.neuterSingularForm = neuterSingularForm;
	}

	/**
	 * @return the malePluralForm
	 */
	public String getMalePluralForm() {
		return malePluralForm;
	}

	/**
	 * @param malePluralForm
	 *            the malePluralForm to set
	 */
	public void setMalePluralForm(String malePluralForm) {
		this.malePluralForm = malePluralForm;
	}

	/**
	 * @return the femalePluralForm
	 */
	public String getFemalePluralForm() {
		return femalePluralForm;
	}

	/**
	 * @param femalePluralForm
	 *            the femalePluralForm to set
	 */
	public void setFemalePluralForm(String femalePluralForm) {
		this.femalePluralForm = femalePluralForm;
	}

	/**
	 * @return the neuterPluralForm
	 */
	public String getNeuterPluralForm() {
		return neuterPluralForm;
	}

	/**
	 * @param neuterPluralForm
	 *            the neuterPluralForm to set
	 */
	public void setNeuterPluralForm(String neuterPluralForm) {
		this.neuterPluralForm = neuterPluralForm;
	}

}
