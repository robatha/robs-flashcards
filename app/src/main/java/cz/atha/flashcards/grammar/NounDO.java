/**
 * 
 */
package cz.atha.flashcards.grammar;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import cz.atha.flashcards.grammar.models.HasModel;
import cz.atha.flashcards.grammar.models.ModelDO;
import cz.atha.flashcards.grammar.models.NounModelDO;
import cz.atha.flashcards.grammar.models.NounModelFormDO;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author rob
 * 
 */
public class NounDO implements WordDO, Gender.HasGender, Animation.HasAnimation, HasModel {

	private final String LOGTAG = "NounDO";

	// used for every word
	private String target;
	private String meaning;
	private String image;
	private String audio;
	private List<String> relatedWords = null;
	private List<String> examples = null;

	// static properties of this word - used for declination
	private NounModelDO model = null;
	private Gender gender = Gender.unknown;
	private Animation animate = Animation.unknown;

	// dynamic properties of this word, causes it to mutate form
	private Case _case = Case.unknown;
	private Number number = Number.unknown;

	public NounDO(){}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public NounDO createFromParcel(Parcel in) {
			NounDO vo = new NounDO();
			vo.target = in.readString();
			vo.meaning = in.readString();
			vo.image = in.readString();
			vo.audio = in.readString();
			vo.relatedWords = in.createStringArrayList();
			vo.examples = in.createStringArrayList();
			//TODO model
			vo.gender = Gender.valueOf(in.readString());
			vo.animate = Animation.valueOf(in.readString());
			vo._case = Case.valueOf(in.readString());
			vo.number = Number.valueOf(in.readString());

			return vo;
		}

		public NounDO[] newArray(int size) {
			return new NounDO[size];
		}

	};

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.target);
		dest.writeString(this.meaning);
		dest.writeString(this.image);
		dest.writeString(this.audio);
		dest.writeStringList(this.relatedWords);
		dest.writeStringList(this.examples);
		//todo model
		dest.writeString(this.gender.name());
		dest.writeString(this.animate.name());
		dest.writeString(this._case.name());
		dest.writeString(this.number.name());
	}

	@Override
	public int describeContents(){
		return 0;
	}

	/**
	 * @return the gender
	 */
    @Override
	public Gender getGender() {
		if (gender == null || gender == Gender.unknown) {
			Log.v("NounDO", "Guessing gender of \"" + this.getTarget() + "\"");
			if (this.getTarget().endsWith("a"))
				return Gender.female;
			if (this.getTarget().endsWith("e"))
				return Gender.female;
			if (this.getTarget().endsWith("o"))
				return Gender.neuter;
			if (this.getTarget().endsWith("í"))
				return Gender.neuter;
			if (this.getTarget().endsWith("um"))
				return Gender.neuter;
			if (this.getTarget().endsWith("ě"))
				return Gender.neuter;

			else
				return Gender.male;
		}
		return gender;
	}

	/**
	 * @param g
	 *            the gender to set
	 */
    @Override
	public void setGender(Gender g) {
		this.gender = g;
	}

	/**
	 * @return the animate
	 */
    @Override
	public Animation getAnimate() {
		return animate;
	}

	/**
	 * @param a
	 *            the animate to set
	 */
    @Override
	public void setAnimate(Animation a) {
		this.animate = a;
	}

	/**
	 * @return the target
	 */
	@Override
	public String getTarget() {
		return target;
	}

	/**
	 * @param target
	 *            the target to set
	 */
	@Override
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * @return the meaning
	 */
	@Override
	public String getMeaning() {
		return meaning;
	}

	/**
	 * @param meaning
	 *            the meaning to set
	 */
	@Override
	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

	/**
	 * @return the imageURI
	 */
	@Override
	public String getImage() {
		return image;
	}

	/**
	 * @param i
	 *            the image to set
	 */
	@Override
	public void setImage(String i) {
		this.image = i;
	}

	/**
	 * @return the model
	 */
	@Override
	public ModelDO getModel() {
		return model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	@Override
	public void setModel(ModelDO model) {
		this.model = (NounModelDO)model;
	}

	@Override
	public String getAudio(){ return this.audio; }

	@Override
	public void setAudio(String a){this.audio = a; }

	@Override
	public List<String> getRelatedWordIds() {
		return relatedWords;
	}

	@Override
	public void setRelatedWordIds(List<String> relatedWords) {
		this.relatedWords = relatedWords;
	}

	@Override
	public List<String> getExamples() {
		return examples;
	}

	@Override
	public void setExamples(List<String> examples) {
		this.examples = examples;
	}


	@Override
	public String getForm(Object modifier){

		String dictionaryForm = getTarget();

		// in case we try to get form from word with no model!
		if (model == null)
			return null;

		Log.d(LOGTAG, "Calling getForm() dictionaryForm=" + dictionaryForm);
		Log.d(LOGTAG, "Calling getForm() modifier=" + modifier.toString());

		NounModelFormDO foundForm = null;
		// find form with the best score
		int score = -1;
		for (NounModelFormDO form : model.getForms()) {
			int newScore = form.isCorrectForm(modifier);
			if (newScore > score) {
				score = newScore;
				foundForm = form;
			}
		}

		// didn't find appropriate form
		if (foundForm == null)
			return null;
		Log.d(LOGTAG, "found form=" + foundForm.toString());

		// get replace string
		String replaceString = foundForm.getReplaceString();
		if (replaceString == null) {
			Log.e(LOGTAG, "No replace string: " + modifier.toString());
			return null;
		}
		Log.d(LOGTAG, "found replace string: " + replaceString);

		// replace with stem
		Pattern p = Pattern.compile(model.getRegex());
		Matcher m = p.matcher(dictionaryForm);

		// if no match or not enough groups to make new form, keep looking
		if (!m.matches()) {
			Log.e(LOGTAG, "no match: " + p.pattern());
			return null;
		}
		int replaceGroupsCount = replaceString.split("$").length - 1;
		if (m.groupCount() < replaceGroupsCount) {
			Log.e(LOGTAG, "no groupcountmatch: " + m.groupCount() + " vs "
					+ replaceGroupsCount);
			return null;
		}
		return m.replaceAll(replaceString);
	}

	@Override
	public String toString() {
		return "NounDO{" +
				"target='" + target + '\'' +
				", meaning='" + meaning + '\'' +
				", image='" + image + '\'' +
				", audio='" + audio + '\'' +
				", relatedWords=" + relatedWords +
				", examples=" + examples +
				", model=" + model +
				", gender=" + gender +
				", animate=" + animate +
				", _case=" + _case +
				", number=" + number +
				'}';
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		WordDO copy = (WordDO)super.clone();
		copy.setRelatedWordIds(new ArrayList<>(copy.getRelatedWordIds()));
		copy.setExamples(new ArrayList<>(copy.getExamples()));
		return copy;
	}

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        NounDO compare = (NounDO)obj;

        if (target == null){
            if (compare.target != null) return false;
        } else if (!target.equals(compare.target)) return false;

        if (meaning == null){
            if (compare.meaning != null) return false;
        } else if (!meaning.equals(compare.meaning)) return false;

        if (image == null){
            if (compare.image != null) return false;
        } else if (!image.equals(compare.image)) return false;

        if (audio == null){
            if (compare.audio != null) return false;
        } else if (!audio.equals(compare.audio)) return false;

        if (model == null){
            if (compare.model != null) return false;
        } else if (!model.equals(compare.model)) return false;


        if (gender == null){
            if (compare.gender != null) return false;
        } else if (!gender.equals(compare.gender)) return false;

        if (animate != compare.animate)
            return false;

        return true;
    }

    @Override
    public int hashCode(){

        int result = 17;
        if (target != null)
            result = 31 * result + target.hashCode();
        if (meaning != null)
            result = 31 * result + meaning.hashCode();
        if (image != null)
            result = 31 * result + image.hashCode();
        if (audio != null)
            result = 31 * result + audio.hashCode();
        if (model != null)
            result = 31 * result + model.hashCode();
        if (gender != null)
            result = 31 * result + gender.hashCode();
        // TODO animate???

        return result;

    }

	@Override
	public int compareTo(WordDO o) {
		Collator coll = Collator.getInstance(Locale.forLanguageTag("cs-CZ"));
		coll.setStrength(Collator.PRIMARY);

		return coll.compare(this.target, o.getTarget());
	}
}
