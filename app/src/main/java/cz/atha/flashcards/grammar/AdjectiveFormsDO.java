package cz.atha.flashcards.grammar;

public class AdjectiveFormsDO {

    private AdjectiveDO adjective;
    private String nominitiveSingularMaleAnimate;
    private String nominitiveSingularMaleInanimate;
    private String nominitiveSingularFemale;
    private String nominitiveSingularNeuter;
    private String nominitivePluralMaleAnimate;
    private String nominitivePluralMaleInanimate;
    private String nominitivePluralFemale;
    private String nominitivePluralNeuter;
    private String genitiveSingularMaleAnimate;
    private String genitiveSingularMaleInanimate;
    private String genitiveSingularFemale;
    private String genitiveSingularNeuter;
    private String genitivePluralMaleAnimate;
    private String genitivePluralMaleInanimate;
    private String genitivePluralFemale;
    private String genitivePluralNeuter;
    private String dativeSingularMaleAnimate;
    private String dativeSingularMaleInanimate;
    private String dativeSingularFemale;
    private String dativeSingularNeuter;
    private String dativePluralMaleAnimate;
    private String dativePluralMaleInanimate;
    private String dativePluralFemale;
    private String dativePluralNeuter;
    private String accusitiveSingularMaleAnimate;
    private String accusitiveSingularMaleInanimate;
    private String accusitiveSingularFemale;
    private String accusitiveSingularNeuter;
    private String accusitivePluralMaleAnimate;
    private String accusitivePluralMaleInanimate;
    private String accusitivePluralFemale;
    private String accusitivePluralNeuter;
    private String vocativeSingularMaleAnimate;
    private String vocativeSingularMaleInanimate;
    private String vocativeSingularFemale;
    private String vocativeSingularNeuter;
    private String vocativePluralMaleAnimate;
    private String vocativePluralMaleInanimate;
    private String vocativePluralFemale;
    private String vocativePluralNeuter;
    private String locativeSingularMaleAnimate;
    private String locativeSingularMaleInanimate;
    private String locativeSingularFemale;
    private String locativeSingularNeuter;
    private String locativePluralMaleAnimate;
    private String locativePluralMaleInanimate;
    private String locativePluralFemale;
    private String locativePluralNeuter;
    private String instrumentalSingularMaleAnimate;
    private String instrumentalSingularMaleInanimate;
    private String instrumentalSingularFemale;
    private String instrumentalSingularNeuter;
    private String instrumentalPluralMaleAnimate;
    private String instrumentalPluralMaleInanimate;
    private String instrumentalPluralFemale;
    private String instrumentalPluralNeuter;

    public AdjectiveDO getAdjective() {
        return adjective;
    }

    public String getNominitiveSingularMaleAnimate() {
        return nominitiveSingularMaleAnimate;
    }

    public String getNominitiveSingularMaleInanimate() {
        return nominitiveSingularMaleInanimate;
    }

    public String getNominitiveSingularFemale() {
        return nominitiveSingularFemale;
    }

    public String getNominitiveSingularNeuter() {
        return nominitiveSingularNeuter;
    }

    public String getNominitivePluralMaleAnimate() {
        return nominitivePluralMaleAnimate;
    }

    public String getNominitivePluralMaleInanimate() {
        return nominitivePluralMaleInanimate;
    }

    public String getNominitivePluralFemale() {
        return nominitivePluralFemale;
    }

    public String getNominitivePluralNeuter() {
        return nominitivePluralNeuter;
    }

    public String getGenitiveSingularMaleAnimate() {
        return genitiveSingularMaleAnimate;
    }

    public String getGenitiveSingularMaleInanimate() {
        return genitiveSingularMaleInanimate;
    }

    public String getGenitiveSingularFemale() {
        return genitiveSingularFemale;
    }

    public String getGenitiveSingularNeuter() {
        return genitiveSingularNeuter;
    }

    public String getGenitivePluralMaleAnimate() {
        return genitivePluralMaleAnimate;
    }

    public String getGenitivePluralMaleInanimate() {
        return genitivePluralMaleInanimate;
    }

    public String getGenitivePluralFemale() {
        return genitivePluralFemale;
    }

    public String getGenitivePluralNeuter() {
        return genitivePluralNeuter;
    }

    public String getDativeSingularMaleAnimate() {
        return dativeSingularMaleAnimate;
    }

    public String getDativeSingularMaleInanimate() {
        return dativeSingularMaleInanimate;
    }

    public String getDativeSingularFemale() {
        return dativeSingularFemale;
    }

    public String getDativeSingularNeuter() {
        return dativeSingularNeuter;
    }

    public String getDativePluralMaleAnimate() {
        return dativePluralMaleAnimate;
    }

    public String getDativePluralMaleInanimate() {
        return dativePluralMaleInanimate;
    }

    public String getDativePluralFemale() {
        return dativePluralFemale;
    }

    public String getDativePluralNeuter() {
        return dativePluralNeuter;
    }

    public String getAccusitiveSingularMaleAnimate() {
        return accusitiveSingularMaleAnimate;
    }

    public String getAccusitiveSingularMaleInanimate() {
        return accusitiveSingularMaleInanimate;
    }

    public String getAccusitiveSingularFemale() {
        return accusitiveSingularFemale;
    }

    public String getAccusitiveSingularNeuter() {
        return accusitiveSingularNeuter;
    }

    public String getAccusitivePluralMaleAnimate() {
        return accusitivePluralMaleAnimate;
    }

    public String getAccusitivePluralMaleInanimate() {
        return accusitivePluralMaleInanimate;
    }

    public String getAccusitivePluralFemale() {
        return accusitivePluralFemale;
    }

    public String getAccusitivePluralNeuter() {
        return accusitivePluralNeuter;
    }

    public String getVocativeSingularMaleAnimate() {
        return vocativeSingularMaleAnimate;
    }

    public String getVocativeSingularMaleInanimate() {
        return vocativeSingularMaleInanimate;
    }

    public String getVocativeSingularFemale() {
        return vocativeSingularFemale;
    }

    public String getVocativeSingularNeuter() {
        return vocativeSingularNeuter;
    }

    public String getVocativePluralMaleAnimate() {
        return vocativePluralMaleAnimate;
    }

    public String getVocativePluralMaleInanimate() {
        return vocativePluralMaleInanimate;
    }

    public String getVocativePluralFemale() {
        return vocativePluralFemale;
    }

    public String getVocativePluralNeuter() {
        return vocativePluralNeuter;
    }

    public String getLocativeSingularMaleAnimate() {
        return locativeSingularMaleAnimate;
    }

    public String getLocativeSingularMaleInanimate() {
        return locativeSingularMaleInanimate;
    }

    public String getLocativeSingularFemale() {
        return locativeSingularFemale;
    }

    public String getLocativeSingularNeuter() {
        return locativeSingularNeuter;
    }

    public String getLocativePluralMaleAnimate() {
        return locativePluralMaleAnimate;
    }

    public String getLocativePluralMaleInanimate() {
        return locativePluralMaleInanimate;
    }

    public String getLocativePluralFemale() {
        return locativePluralFemale;
    }

    public String getLocativePluralNeuter() {
        return locativePluralNeuter;
    }

    public String getInstrumentalSingularMaleAnimate() {
        return instrumentalSingularMaleAnimate;
    }

    public String getInstrumentalSingularMaleInanimate() {
        return instrumentalSingularMaleInanimate;
    }

    public String getInstrumentalSingularFemale() {
        return instrumentalSingularFemale;
    }

    public String getInstrumentalSingularNeuter() {
        return instrumentalSingularNeuter;
    }

    public String getInstrumentalPluralMaleAnimate() {
        return instrumentalPluralMaleAnimate;
    }

    public String getInstrumentalPluralMaleInanimate() {
        return instrumentalPluralMaleInanimate;
    }

    public String getInstrumentalPluralFemale() {
        return instrumentalPluralFemale;
    }

    public String getInstrumentalPluralNeuter() {
        return instrumentalPluralNeuter;
    }

    public AdjectiveFormsDO(AdjectiveDO a){
        this.adjective = a;
        AdjectiveModifierDO modifier = new AdjectiveModifierDO();

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.nominative);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.nominitiveSingularMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.animate);
        this.nominitiveSingularMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.nominitiveSingularFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.nominitiveSingularNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.plural);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.nominitivePluralMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.inanimate);
        this.nominitivePluralMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.nominitivePluralFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.nominitivePluralNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.genitive);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.genitiveSingularMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.animate);
        this.genitiveSingularMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.genitiveSingularFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.genitiveSingularNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.plural);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.genitivePluralMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.inanimate);
        this.genitivePluralMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.genitivePluralFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.genitivePluralNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.dative);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.dativeSingularMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.animate);
        this.dativeSingularMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.dativeSingularFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.dativeSingularNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.plural);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.dativePluralMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.inanimate);
        this.dativePluralMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.dativePluralFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.dativePluralNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.accusative);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.accusitiveSingularMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.animate);
        this.accusitiveSingularMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.accusitiveSingularFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.accusitiveSingularNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.plural);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.accusitivePluralMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.inanimate);
        this.accusitivePluralMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.accusitivePluralFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.accusitivePluralNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.vocative);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.vocativeSingularMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.animate);
        this.vocativeSingularMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.vocativeSingularFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.vocativeSingularNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.plural);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.vocativePluralMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.inanimate);
        this.vocativePluralMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.vocativePluralFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.vocativePluralNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.locative);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.locativeSingularMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.animate);
        this.locativeSingularMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.locativeSingularFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.locativeSingularNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.plural);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.locativePluralMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.inanimate);
        this.locativePluralMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.locativePluralFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.locativePluralNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.singular);
        modifier.setCase(Case.instrumental);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.instrumentalSingularMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.animate);
        this.instrumentalSingularMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.instrumentalSingularFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.instrumentalSingularNeuter = adjective.getForm(modifier);

        modifier.setNumber(Number.plural);
        modifier.setGender(Gender.male);
        modifier.setAnimate(Animation.animate);
        this.instrumentalPluralMaleAnimate = adjective.getForm(modifier);
        modifier.setAnimate(Animation.inanimate);
        this.instrumentalPluralMaleInanimate = adjective.getForm(modifier);
        modifier.setGender(Gender.female);
        this.instrumentalPluralFemale = adjective.getForm(modifier);
        modifier.setGender(Gender.neuter);
        this.instrumentalPluralNeuter = adjective.getForm(modifier);
    }
}
