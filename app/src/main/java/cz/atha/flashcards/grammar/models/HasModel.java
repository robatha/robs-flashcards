package cz.atha.flashcards.grammar.models;

import cz.atha.flashcards.grammar.WordDO;

public interface HasModel extends WordDO {

    ModelDO getModel();

    void setModel(ModelDO m);

    String getForm(Object modifier);
}
