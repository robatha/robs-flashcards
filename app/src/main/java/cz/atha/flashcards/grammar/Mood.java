/**
 * 
 */
package cz.atha.flashcards.grammar;

import java.util.Locale;

/**
 * @author rob
 * 
 */

public enum Mood {
    indicative, imperative, conditional, unknown;

    public static Mood toMood(String str) {
        // indicative, imperative, conditional, unknown
        String m = new String(str).toUpperCase(Locale.ENGLISH);
        if (m.startsWith("IN"))
            return Mood.indicative;
        if (m.startsWith("IM"))
            return Mood.imperative;
        if (m.startsWith("C"))
            return Mood.conditional;

        // give up
        return Mood.unknown;
    }
    public interface HasMood {



        Mood getMood();
        void setMood(Mood m);
    }
}