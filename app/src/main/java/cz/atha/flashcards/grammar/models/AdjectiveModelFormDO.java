package cz.atha.flashcards.grammar.models;

import cz.atha.flashcards.dao.ModelAdjectiveForm;
import cz.atha.flashcards.grammar.Animation;
import cz.atha.flashcards.grammar.Case;
import cz.atha.flashcards.grammar.Gender;
import cz.atha.flashcards.grammar.Number;

public class AdjectiveModelFormDO {

    private long id;
    private Gender gender;
    private Animation animate;
    private Case caze;
    private Number number;
    private String replaceString;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Animation getAnimate() {
        return animate;
    }

    public void setAnimate(Animation animate) {
        this.animate = animate;
    }

    public Case getCase() {
        return caze;
    }

    public void setCase(Case caze) {
        this.caze = caze;
    }

    public Number getNumber() {
        return number;
    }

    public void setNumber(Number number) {
        this.number = number;
    }

    public String getReplaceString() {
        return replaceString;
    }

    public void setReplaceString(String replaceString) {
        this.replaceString = replaceString;
    }

    public AdjectiveModelFormDO(ModelAdjectiveForm obj) {
        this.id = obj.id;
        this.gender = Gender.toGender(obj.gender);
        this.animate = Animation.toAnimation(obj.animate);
        this.caze = Case.toCase(obj.caze);
        this.number = Number.toNumber(obj.number);
        this.replaceString = obj.replaceString;
    }

    public boolean isCorrectForm(Object obj) {

        if (obj == null)
            return false;

//        Log.e(LOGTAG, "gender");
        if (obj instanceof Gender.HasGender && this.gender != Gender.unknown) {

            if (((Gender.HasGender) obj).getGender() != this.gender)
                return false;

            // only compare animate if it's male
            if (((Gender.HasGender) obj).getGender() == Gender.male) {
                if (obj instanceof Animation.HasAnimation) {
                    if (((Animation.HasAnimation) obj).getAnimate() != this.animate)
                        return false;
                }
            }
        }

        //    Log.e(LOGTAG, "case");
        if (obj instanceof Case.HasCase && this.getCase() != Case.unknown) {
            if (((Case.HasCase) obj).getCase() != this.getCase())
                return false;
        }

        //  Log.e(LOGTAG, "number");
        if (obj instanceof Number.HasNumber && this.number != Number.unknown) {
            if (((Number.HasNumber) obj).getNumber() != this.number)
                return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "AdjectiveModelFormDO{" +
                "id=" + id +
                ", gender=" + gender +
                ", animate=" + animate +
                ", caze=" + caze +
                ", number=" + number +
                ", replaceString='" + replaceString + '\'' +
                '}';
    }
}
