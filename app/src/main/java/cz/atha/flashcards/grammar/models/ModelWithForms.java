package cz.atha.flashcards.grammar.models;

import java.util.List;

import cz.atha.flashcards.dao.Model;

public interface ModelWithForms<T> {
    void setModel(Model model);

    void addForm(T form);

    Model getModel();

    List<T> getForms();
}
