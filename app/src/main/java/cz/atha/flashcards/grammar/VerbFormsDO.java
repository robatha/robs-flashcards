package cz.atha.flashcards.grammar;

import java.util.ArrayList;
import java.util.List;

public class VerbFormsDO {

    private VerbDO verb;
    private String presentFirstSingular;
    private String presentFirstPlural;
    private String presentSecondSingular;
    private String presentSecondPlural;
    private String presentThirdSingular;
    private String presentThirdPlural;
    private String lFormMasculineSingular;
    private String lFormFeminineSingular;
    private String lFormNueterSingular;
    private String lFormMasculinePlural;
    private String lFormFemininePlural;
    private String lFormNueterPlural;
    private String futureFirstSingular;
    private String futureFirstPlural;
    private String futureSecondSingular;
    private String futureSecondPlural;
    private String futureThirdSingular;
    private String futureThirdPlural;
    private String imperativeFirstPlural;
    private String imperativeSecondSingular;
    private String imperativeSecondPlural;
    private List<String> aspectOtherForms;


    public VerbFormsDO(VerbDO verb) {
        this.verb = verb;

        VerbModifierDO modifier = new VerbModifierDO();
        modifier.setVoice(Voice.active);
        modifier.setMood(Mood.indicative);
        modifier.setTense(Tense.present);

        modifier.setPerson(Person.first);
        modifier.setNumber(Number.singular);
        this.presentFirstSingular = verb.getForm(modifier);

        modifier.setNumber(Number.plural);
        this.presentFirstPlural = verb.getForm(modifier);

        modifier.setPerson(Person.second);
        modifier.setNumber(Number.singular);
        this.presentSecondSingular = verb.getForm(modifier);

        modifier.setNumber(Number.plural);
        this.presentSecondPlural = verb.getForm(modifier);

        modifier.setPerson(Person.third);
        modifier.setNumber(Number.singular);
        this.presentThirdSingular = verb.getForm(modifier);

        modifier.setNumber(Number.plural);
        this.presentThirdPlural = verb.getForm(modifier);

        modifier.setTense(Tense.past);
        modifier.setNumber(Number.singular);
        modifier.setGender(Gender.male);
        this.lFormMasculineSingular = verb.getForm(modifier);

        modifier.setGender(Gender.female);
        this.lFormFeminineSingular = verb.getForm(modifier);

        modifier.setGender(Gender.neuter);
        this.lFormNueterSingular = verb.getForm(modifier);


        modifier.setNumber(Number.plural);
        modifier.setGender(Gender.male);
        this.lFormMasculinePlural = verb.getForm(modifier);

        modifier.setGender(Gender.female);
        this.lFormFemininePlural = verb.getForm(modifier);

        modifier.setGender(Gender.neuter);
        this.lFormNueterPlural = verb.getForm(modifier);

        /* old and busted
        this.futureFirstSingular = "budu " + verb.getTarget();
        this.futureFirstPlural = "budeme " + verb.getTarget();
        this.futureSecondSingular = "budeš " + verb.getTarget();
        this.futureSecondPlural = "budete " + verb.getTarget();
        this.futureThirdSingular = "bude " + verb.getTarget();
        this.futureThirdPlural = "budou " + verb.getTarget();
        */
        modifier.setTense(Tense.future);
        modifier.setPerson(Person.first);
        modifier.setNumber(Number.singular);
        this.futureFirstSingular = VerbDO.getAuxiliaryVerbs(modifier, verb, verb.getForm(modifier));
        modifier.setNumber(Number.plural);
        this.futureFirstPlural = VerbDO.getAuxiliaryVerbs(modifier, verb, verb.getForm(modifier));
        modifier.setPerson(Person.second);
        modifier.setNumber(Number.singular);
        this.futureSecondSingular = VerbDO.getAuxiliaryVerbs(modifier, verb, verb.getForm(modifier));
        modifier.setNumber(Number.plural);
        this.futureSecondPlural = VerbDO.getAuxiliaryVerbs(modifier, verb, verb.getForm(modifier));
        modifier.setPerson(Person.third);
        modifier.setNumber(Number.singular);
        this.futureThirdSingular = VerbDO.getAuxiliaryVerbs(modifier, verb, verb.getForm(modifier));
        modifier.setNumber(Number.plural);
        this.futureThirdPlural = VerbDO.getAuxiliaryVerbs(modifier, verb, verb.getForm(modifier));

        modifier.setMood(Mood.imperative);
        modifier.setPerson(Person.first);
        modifier.setNumber(Number.plural);
        this.imperativeFirstPlural = verb.getForm(modifier);

        modifier.setPerson(Person.second);
        modifier.setNumber(Number.singular);
        this.imperativeSecondSingular = verb.getForm(modifier);

        modifier.setNumber(Number.plural);
        this.imperativeSecondPlural = verb.getForm(modifier);

        aspectOtherForms = new ArrayList<String>();
        aspectOtherForms.add("počkat");
    }

    public VerbDO getVerb() {
        return verb;
    }

    public String getPresentFirstSingular() {
        return presentFirstSingular;
    }

    public String getPresentFirstPlural() {
        return presentFirstPlural;
    }

    public String getPresentSecondSingular() {
        return presentSecondSingular;
    }

    public String getPresentSecondPlural() {
        return presentSecondPlural;
    }

    public String getPresentThirdSingular() {
        return presentThirdSingular;
    }

    public String getPresentThirdPlural() {
        return presentThirdPlural;
    }

    public String getLFormMasculineSingular() {
        return lFormMasculineSingular;
    }

    public String getLFormFeminineSingular() {
        return lFormFeminineSingular;
    }

    public String getLFormNueterSingular() {
        return lFormNueterSingular;
    }

    public String getLFormMasculinePlural() {
        return lFormMasculinePlural;
    }

    public String getLFormFemininePlural() {
        return lFormFemininePlural;
    }

    public String getLFormNueterPlural() {
        return lFormNueterPlural;
    }

    public String getFutureFirstSingular() {
        return futureFirstSingular;
    }

    public String getFutureFirstPlural() {
        return futureFirstPlural;
    }

    public String getFutureSecondSingular() {
        return futureSecondSingular;
    }

    public String getFutureSecondPlural() {
        return futureSecondPlural;
    }

    public String getFutureThirdSingular() {
        return futureThirdSingular;
    }

    public String getFutureThirdPlural() {
        return futureThirdPlural;
    }

    public String getImperativeFirstPlural() {
        return imperativeFirstPlural;
    }

    public String getImperativeSecondSingular() {
        return imperativeSecondSingular;
    }

    public String getImperativeSecondPlural() {
        return imperativeSecondPlural;
    }
}
