/**
 * 
 */
package cz.atha.flashcards.grammar;


import java.util.Locale;

/**
 * @author rob
 * 
 */
@Deprecated
public enum Formality {
	informal, formal, unknown;

	public static Formality toFormality(String str) {
		if (str != null) {
			String f = new String(str).toUpperCase(Locale.ENGLISH);
			if (f.startsWith("F"))
				return Formality.formal;
			else if (f.startsWith("I"))
				return Formality.informal;
		}

		// give up
		return Formality.unknown;
	}
	public interface HasFormality {

		/**
		 * @author rob
		 *
		 */


		Formality getFormality();
		void setFormality(Formality f);

	}
}