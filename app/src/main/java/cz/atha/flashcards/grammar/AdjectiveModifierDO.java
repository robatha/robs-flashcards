/**
 * 
 */
package cz.atha.flashcards.grammar;

/**
 * @author rob
 * 
 */
public class AdjectiveModifierDO implements Gender.HasGender, Case.HasCase,
		Animation.HasAnimation, Number.HasNumber {


	private Gender gender = Gender.unknown;
	private Animation animate = Animation.unknown;
	private Case _case = Case.unknown;
	private Number number = Number.unknown;


	@Override
	public Gender getGender() {
		return gender;
	}

	@Override
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Override
	public Animation getAnimate() {
		return animate;
	}

	@Override
	public void setAnimate(Animation animate) {
		this.animate = animate;
	}

	@Override
	public Case getCase() {
		return _case;
	}

	@Override
	public void setCase(Case _case) {
		this._case = _case;
	}

	@Override
	public Number getNumber() {
		return number;
	}

	@Override
	public void setNumber(Number number) {
		this.number = number;
	}

	// an adjective modifies a noun
	public void modify(NounDO noun){
		gender = noun.getGender();
		animate = noun.getAnimate();
	}

	@Override
	public String toString() {
		return "AdjectiveModifierDO{" +
				"gender=" + gender +
				", animate=" + animate +
				", _case=" + _case +
				", number=" + number +
				'}';
	}
}
