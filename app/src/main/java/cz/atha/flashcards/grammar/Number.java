/**
 * 
 */
package cz.atha.flashcards.grammar;


import java.util.Locale;

/**
 * @author rob
 *
 */

public enum Number {
	singular, plural, unknown;

	public static Number toNumber(String str) {
		if (str != null) {
			// try string match
			String n = new String(str).toUpperCase(Locale.ENGLISH);
			if (n.startsWith("S"))
				return Number.singular;
			else if (n.startsWith("P"))
				return Number.plural;
		}

		// give up
		return Number.unknown;
	}
	public interface HasNumber {


		Number getNumber();
		void setNumber(Number n);
	}
}