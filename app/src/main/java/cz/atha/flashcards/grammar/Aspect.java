package cz.atha.flashcards.grammar;

import java.util.Locale;


public enum Aspect {
    perfective, imperfective, unknown;

    public static Aspect toAspect(String str) {
        if (str != null) {
            String a = new String(str).toUpperCase(Locale.ENGLISH);
            if (a.startsWith("P"))
                return Aspect.perfective;
            if (a.startsWith("I"))
                return Aspect.imperfective;
        }
        // give up
        return Aspect.unknown;
    }
    public interface HasAspect {

        Aspect getAspect();

        void setAspect(Aspect a);

    }

}