/**
 * 
 */
package cz.atha.flashcards.grammar;

import android.os.Parcel;


import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author rob
 *
 * This is largely incomplete and I'm not sure what role it's going to have.
 *
 */
// TODO update this maybe delete this!!!!
public class PhraseDO implements WordDO, Mood.HasMood, Tense.HasTense, Person.HasPerson,
		Number.HasNumber, Aspect.HasAspect, Gender.HasGender, Animation.HasAnimation, Voice.HasVoice {

	// used for every word
	private String target;
	private String meaning;
	private String image;
	private String audio;
	private List<String> relatedWords = null;
	private List<String> examples = null;

	// static properties of this word - used for conjugation
	private Aspect aspect = Aspect.unknown;

	// dynamic properties of this word, causes it to mutate form
	private Voice voice = Voice.unknown;
	private Mood mood = Mood.unknown;
	private Tense tense = Tense.unknown;
	private Person person = Person.unknown;
	private Number number = Number.unknown;
	private Gender gender = Gender.unknown;
	private Animation animate = Animation.unknown;

	public PhraseDO(){}

	public static final Creator CREATOR = new Creator() {
		public PhraseDO createFromParcel(Parcel in) {
			PhraseDO vo = new PhraseDO();
			vo.target = in.readString();
			vo.meaning = in.readString();
			vo.image = in.readString();
			vo.audio = in.readString();
			vo.relatedWords = in.createStringArrayList();
			vo.examples = in.createStringArrayList();
            // TODO model
            vo.aspect = Aspect.valueOf(in.readString());
            vo.voice = Voice.valueOf(in.readString());
			vo.mood = Mood.valueOf(in.readString());
			vo.tense = Tense.valueOf(in.readString());
			vo.person = Person.valueOf(in.readString());
			vo.number = Number.valueOf(in.readString());
			vo.gender = Gender.valueOf(in.readString());
			vo.animate = Animation.valueOf(in.readString());

			return vo;
		}

		public PhraseDO[] newArray(int size) {
			return new PhraseDO[size];
		}

	};

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.target);
		dest.writeString(this.meaning);
		dest.writeString(this.image);
		dest.writeString(this.audio);
		dest.writeStringList(this.relatedWords);
		dest.writeStringList(this.examples);
        // TODO model
        dest.writeString(this.aspect.name());
		dest.writeString(this.voice.name());
		dest.writeString(this.mood.name());
		dest.writeString(this.tense.name());
		dest.writeString(this.person.name());
		dest.writeString(this.number.name());
		dest.writeString(this.gender.name());
		dest.writeString(this.animate.name() );

	}

	@Override
	public int describeContents(){
		return 0;
	}


	@Override
	public String getTarget() {
		return target;
	}

	@Override
	public void setTarget(String t) {
		this.target = t;
	}

	@Override
	public String getMeaning() {
		return meaning;
	}

	@Override
	public void setMeaning(String m) {
		this.meaning = m;
	}

	@Override
	public String getImage() {
		return image;
	}

	@Override
	public void setImage(String i) {
		this.image = i;
	}

	@Override
	public Aspect getAspect() {
		return this.aspect;
	}

    @Override
	public void setAspect(Aspect a) {
		this.aspect = a;
	}

	@Override
	public Voice getVoice() {
		return this.voice;
	}

	@Override
	public void setVoice(Voice v) {
		this.voice = v;
	}

	@Override
	public Mood getMood() {
		return this.mood;
	}

	@Override
	public void setMood(Mood m) {
		this.mood = m;
	}

	@Override
	public Tense getTense() {
		return this.tense;
	}

    @Override
    public void setTense(Tense t) {
		this.tense = t;
	}

	@Override
	public Person getPerson() {
		return person;
	}

    @Override
    public void setPerson(Person p) {
		this.person = p;
	}

	@Override
	public Number getNumber() {
		return number;
	}

    @Override
	public void setNumber(Number n) {
		this.number = n;
	}

	@Override
	public Gender getGender() {
		return this.gender;
	}

    @Override
	public void setGender(Gender g) {
		this.gender = g;
	}

	@Override
	public Animation getAnimate() {
		return animate;
	}

    @Override
	public void setAnimate(Animation a) {
		this.animate = a;
	}

	@Override
	public String getAudio(){ return this.audio; }

	@Override
	public void setAudio(String a){this.audio = a; }

	@Override
	public List<String> getRelatedWordIds() {
		return relatedWords;
	}

	@Override
	public void setRelatedWordIds(List<String> relatedWords) {
		this.relatedWords = relatedWords;
	}

	@Override
	public List<String> getExamples() {
		return examples;
	}

	@Override
	public void setExamples(List<String> examples) {
		this.examples = examples;
	}

	@Override
	public String toString() {
		return "PhraseDO{" +
				"target='" + target + '\'' +
				", meaning='" + meaning + '\'' +
				", image='" + image + '\'' +
				", audio='" + audio + '\'' +
				", relatedWords=" + relatedWords +
				", examples=" + examples +
				", aspect=" + aspect +
				", voice=" + voice +
				", mood=" + mood +
				", tense=" + tense +
				", person=" + person +
				", number=" + number +
				", gender=" + gender +
				", animate=" + animate +
				'}';
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		WordDO copy = (WordDO)super.clone();
		copy.setRelatedWordIds(new ArrayList<>(copy.getRelatedWordIds()));
		copy.setExamples(new ArrayList<>(copy.getExamples()));
		return copy;
	}

	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		PhraseDO compare = (PhraseDO) obj;

		if (target == null){
			if (compare.target != null) return false;
		} else if (!target.equals(compare.target)) return false;

		if (meaning == null){
			if (compare.meaning != null) return false;
		} else if (!meaning.equals(compare.meaning)) return false;

		if (image == null){
			if (compare.image != null) return false;
		} else if (!image.equals(compare.image)) return false;

		if (audio == null){
			if (compare.audio != null) return false;
		} else if (!audio.equals(compare.audio)) return false;

		return true;
	}

	@Override
	public int hashCode(){

		int result = 17;
		if (target != null)
			result = 31 * result + target.hashCode();
		if (meaning != null)
			result = 31 * result + meaning.hashCode();
		if (image != null)
			result = 31 * result + image.hashCode();
		if (audio != null)
			result = 31 * result + audio.hashCode();

		return result;

	}

	@Override
	public int compareTo(WordDO o) {
		Collator coll = Collator.getInstance(Locale.forLanguageTag("cs-CZ"));
		coll.setStrength(Collator.PRIMARY);

		return coll.compare(this.target, o.getTarget());
	}
}
