package cz.atha.flashcards.grammar.models;

import android.util.Log;

import cz.atha.flashcards.grammar.Animation;
import cz.atha.flashcards.grammar.Aspect;
import cz.atha.flashcards.grammar.Gender;
import cz.atha.flashcards.grammar.Mood;
import cz.atha.flashcards.grammar.Number;
import cz.atha.flashcards.grammar.Person;
import cz.atha.flashcards.grammar.Tense;
import cz.atha.flashcards.grammar.Voice;
import cz.atha.flashcards.dao.ModelVerbForm;

public class VerbModelFormDO {

    final private static String LOGTAG = "VerbModelFormDO";

    private long id;
    private Voice voice;
    private Mood mood;
    private Tense tense;
    private Person person;
    private Number number;
    private Gender gender;
    private Animation animate;
    private String replaceString;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Voice getVoice() {
        return voice;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    public Mood getMood() {
        return mood;
    }

    public void setMood(Mood mood) {
        this.mood = mood;
    }

    public Tense getTense() {
        return tense;
    }

    public void setTense(Tense tense) {
        this.tense = tense;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Number getNumber() {
        return number;
    }

    public void setNumber(Number number) {
        this.number = number;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Animation getAnimate() {
        return animate;
    }

    public void setAnimate(Animation animate) {
        this.animate = animate;
    }

    public String getReplaceString() {
        return replaceString;
    }

    public void setReplaceString(String replaceString) {
        this.replaceString = replaceString;
    }

    public VerbModelFormDO(ModelVerbForm obj) {
        this.id = obj.id;
        this.voice = Voice.toVoice(obj.voice);
        this.mood = Mood.toMood(obj.mood);
        this.tense = Tense.toTense(obj.tense);
        this.person = Person.toPerson(obj.person);
        this.number = Number.toNumber(obj.number);
        this.gender = Gender.toGender(obj.gender);
        this.animate = Animation.toAnimation(obj.animate);
        this.replaceString = obj.replaceString;
    }

    public int isCorrectForm(Object obj, Aspect aspect) {
        int score = 0;

        if (obj == null)
            return -1;

        Gender genderOfObj = Gender.unknown;
        Animation animationOfObj = Animation.unknown;
        Number numberOfObj = Number.unknown;
        Person personOfObj = Person.unknown;
        Mood moodOfObj = Mood.unknown;
        Voice voiceOfObj = Voice.unknown;
        Tense tenseOfObj = Tense.unknown;

        if (obj instanceof Gender.HasGender) genderOfObj = ((Gender.HasGender) obj).getGender();
        if (obj instanceof Animation.HasAnimation)
            animationOfObj = ((Animation.HasAnimation) obj).getAnimate();
        if (obj instanceof Number.HasNumber) numberOfObj = ((Number.HasNumber) obj).getNumber();
        if (obj instanceof Person.HasPerson) personOfObj = ((Person.HasPerson) obj).getPerson();
        if (obj instanceof Mood.HasMood) moodOfObj = ((Mood.HasMood) obj).getMood();
        if (obj instanceof Voice.HasVoice) voiceOfObj = ((Voice.HasVoice) obj).getVoice();
        if (obj instanceof Tense.HasTense) tenseOfObj = ((Tense.HasTense) obj).getTense();

        // we need to account for aspect
        if (aspect == Aspect.perfective) {
            // for perfective verbs, future is expressed with 'present form'
            if (tenseOfObj == Tense.future) {
                tenseOfObj = Tense.present;
            }
            // present doesn't exist for perfective verbs
            else if (tenseOfObj == Tense.present){
                return -1;
            }
        }

        // direct match
        if (genderOfObj == this.gender) {
            score++;
        }
        // direct mismatch
        else if (genderOfObj != Gender.unknown && this.gender != Gender.unknown) {
            Log.d(LOGTAG, "Failed on gender!" + this.gender + " vs " + genderOfObj);
            return -1;
        }

        // direct match
        if (animationOfObj == this.animate) {
            score++;
        }
        // direct mismatch
        else if (animationOfObj != Animation.unknown && this.animate != Animation.unknown) {
            Log.d(LOGTAG, "Failed on animation!" + this.animate + " vs " + animationOfObj);
  //          Log.d(LOGTAG, "form-" + this.toString());
            return -1;
        }

        if (numberOfObj == this.number) {
            score++;
        }
        // direct mismatch
        else if (numberOfObj != Number.unknown && this.number != Number.unknown) {
            Log.d(LOGTAG, "Failed on number!" + this.number + " vs " + numberOfObj);
            return -1;
        }

        if (personOfObj == this.person) {
            score++;
        }
        // direct mismatch
        else if (personOfObj != Person.unknown && this.person != Person.unknown) {
            Log.d(LOGTAG, "Failed on person!" + this.person + " vs " + personOfObj);
            return -1;
        }

        if (moodOfObj == this.mood) {
            score++;
        }
        // direct mismatch
        else if (moodOfObj != Mood.unknown && this.mood != Mood.unknown) {
        //    Log.d(LOGTAG, "Failed on mood!" + this.mood + " vs " + moodOfObj);
            return -1;
        }

        if (voiceOfObj == this.voice) {
            score++;
        }
        // direct mismatch
        else if (voiceOfObj != Voice.unknown && this.voice != Voice.unknown) {
//            Log.d(LOGTAG, "Failed on voice!" + this.voice + " vs " + voiceOfObj);
            return -1;
        }

        if (tenseOfObj == this.tense) {
            score++;
        }
        // direct mismatch
        else if (tenseOfObj != Tense.unknown && this.tense != Tense.unknown) {
  //          Log.d(LOGTAG, "Failed on tense!" + this.tense + " vs " + tenseOfObj);
            return -1;
        }

//        Log.d(LOGTAG, "matched, score=" + score);
        return score;
    }

    @Override
    public String toString() {
        return "VerbModelFormDO{" +
                "id=" + id +
                ", voice=" + voice +
                ", mood=" + mood +
                ", tense=" + tense +
                ", person=" + person +
                ", number=" + number +
                ", gender=" + gender +
                ", animate=" + animate +
                ", replaceString='" + replaceString + '\'' +
                '}';
    }
}
