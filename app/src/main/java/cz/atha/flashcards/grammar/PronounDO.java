/**
 *
 */
package cz.atha.flashcards.grammar;

import android.os.Parcel;


import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author rob
 */
public class PronounDO implements WordDO, Person.HasPerson, Number.HasNumber, Gender.HasGender,
        Case.HasCase, Animation.HasAnimation, PronounClass.HasPronounClass
        // TODO add models!!!
        // , hasModel
{

    // used for every word
    private String target;
    private String meaning;
    private String image;
    private String audio;
    private List<String> relatedWords = null;
    private List<String> examples = null;

    // TODO investigate these, I don't think they are needed
//    private ModelVO model;
//    public ModelVO getModel() { return model; }
//    public void setModel(ModelVO model) { this.model = model; }

    private PronounClass pronounClass = PronounClass.unknown;

    // gender and animate can be static (like on / ona) or dynamic (like můj / moje) depending
    // on pronoun class
    // Furthermore, they may be both, as in the case of "jejího". However, since the static property
    // never affects other word forms in the sentence, there's no need to keep track of it
    private Gender gender = Gender.unknown;
    private Animation animate = Animation.unknown;

    private Number number = Number.unknown;
    private Person person = Person.unknown;

    // dynamic attributes done by pattern
    private Case _case = Case.unknown;

    public PronounDO(){}
    // pronouns are useful for things like conjugations so need an easy constructor
    public PronounDO(String t, String m, Person p, Number n, Gender g) {
        this.target = t;
        this.meaning = null;
        this.image = null;
        this.audio = null;
        this.relatedWords = null;
        this.examples = null;
        this.gender = g;
        this.animate = Animation.animate;
        this._case = Case.nominative;
        this.number = n;
        this.person = p;
        this.pronounClass = PronounClass.personal;
    }

    public static final Creator CREATOR = new Creator() {
        public PronounDO createFromParcel(Parcel in) {
            PronounDO vo = new PronounDO();
            vo.target = in.readString();
            vo.meaning = in.readString();
            vo.image = in.readString();
            vo.audio = in.readString();
            vo.relatedWords = in.createStringArrayList();
            vo.examples = in.createStringArrayList();
            vo.gender = Gender.valueOf(in.readString());
            vo.animate = Animation.valueOf(in.readString());
            vo._case = Case.valueOf(in.readString());
            vo.number = Number.valueOf(in.readString());
            vo.person = Person.valueOf(in.readString());
            vo.pronounClass = PronounClass.valueOf(in.readString());

            return vo;
        }

        public PronounDO[] newArray(int size) {
            return new PronounDO[size];
        }

    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.target);
        dest.writeString(this.meaning);
        dest.writeString(this.image);
        dest.writeString(this.audio);
        dest.writeStringList(this.relatedWords);
        dest.writeStringList(this.examples);
        dest.writeString(this.gender.name());
        dest.writeString(this.animate.name());
        dest.writeString(this._case.name());
        dest.writeString(this.number.name());
        dest.writeString(this.person.name());
        dest.writeString(this.pronounClass.name());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * @return the gender
     */
    @Override
    public Gender getGender() {
        return gender;
    }

    /**
     * @param g the gender to set
     */
    @Override
    public void setGender(Gender g) {
        this.gender = g;
    }


    /**
     * @return the plurality
     */
    @Override
    public Number getNumber() {
        return number;
    }

    /**
     * @param n the plurality to set
     */
    @Override
    public void setNumber(Number n) {
        this.number = n;
    }

    @Override
    public Person getPerson() {
        return person;
    }

    @Override
    public void setPerson(Person p) {
        this.person = p;
    }

    @Override
    public Case getCase() {
        return this._case;
    }

    @Override
    public void setCase(Case c) {
        this._case = c;
    }

    @Override
    public Animation getAnimate() {
        return this.animate;
    }

    @Override
    public void setAnimate(Animation b) {
        this.animate = b;
    }

    @Override
    public String getTarget() {
        return this.target;
    }

    @Override
    public void setTarget(String t) {
        this.target = t;
    }

    @Override
    public String getMeaning() {
        return this.meaning;
    }

    @Override
    public void setMeaning(String m) {
        this.meaning = m;
    }

    @Override
    public String getImage() {
        return this.image;
    }

    @Override
    public void setImage(String i) {
        this.image = i;
    }

    @Override
    public String getAudio() {
        return this.audio;
    }

    @Override
    public List<String> getRelatedWordIds() {
        return relatedWords;
    }

    @Override
    public void setRelatedWordIds(List<String> relatedWords) {
        this.relatedWords = relatedWords;
    }

    @Override
    public List<String> getExamples() {
        return examples;
    }

    @Override
    public void setExamples(List<String> examples) {
        this.examples = examples;
    }

    @Override
    public void setAudio(String a) {
        this.audio = a;
    }

    @Override
    public PronounClass getPronounClass() {
        return pronounClass;
    }

    @Override
    public void setPronounClass(PronounClass c) {
        pronounClass = c;
    }



    @Override
    public String toString() {
        return "PronounDO{" +
                "target='" + target + '\'' +
                ", meaning='" + meaning + '\'' +
                ", image='" + image + '\'' +
                ", audio='" + audio + '\'' +
                ", relatedWords=" + relatedWords +
                ", examples=" + examples +
//                ", model=" + model +
                ", pronounClass=" + pronounClass +
                ", gender=" + gender +
                ", animate=" + animate +
                ", number=" + number +
                ", person=" + person +
                ", _case=" + _case +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        WordDO copy = (WordDO)super.clone();
        copy.setRelatedWordIds(new ArrayList<>(copy.getRelatedWordIds()));
        copy.setExamples(new ArrayList<>(copy.getExamples()));
        return copy;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        PronounDO compare = (PronounDO) obj;

        if (target == null) {
            if (compare.target != null) return false;
        } else if (!target.equals(compare.target)) return false;

        if (meaning == null) {
            if (compare.meaning != null) return false;
        } else if (!meaning.equals(compare.meaning)) return false;

        if (image == null) {
            if (compare.image != null) return false;
        } else if (!image.equals(compare.image)) return false;

        if (audio == null) {
            if (compare.audio != null) return false;
        } else if (!audio.equals(compare.audio)) return false;

        /*
        if (model == null) {
            if (compare.model != null) return false;
        } else if (!model.equals(compare.model)) return false;
*/
        if (gender == null) {
            if (compare.gender != null) return false;
        } else if (!gender.equals(compare.gender)) return false;

        if (pronounClass == null) {
            if (compare.pronounClass != null) return false;
        } else if (!pronounClass.equals(compare.pronounClass)) return false;

        if (animate != compare.animate)
            return false;

        return true;
    }

    @Override
    public int hashCode() {

        int result = 17;
        if (target != null)
            result = 31 * result + target.hashCode();
        if (meaning != null)
            result = 31 * result + meaning.hashCode();
        if (image != null)
            result = 31 * result + image.hashCode();
        if (audio != null)
            result = 31 * result + audio.hashCode();
 /*       if (model != null)
            result = 31 * result + model.hashCode();

  */
        if (gender != null)
            result = 31 * result + gender.hashCode();
        if (pronounClass != null)
            result = 31 * result + pronounClass.hashCode();
        // TODO animate???

        return result;

    }

    @Override
    public int compareTo(WordDO o) {
        Collator coll = Collator.getInstance(Locale.forLanguageTag("cs-CZ"));
        coll.setStrength(Collator.PRIMARY);

        return coll.compare(this.target, o.getTarget());
    }

    // this is a list of personal pronouns to use anywhere
    final public static List<PronounDO>pronouns;
    static {
        pronouns = new ArrayList<>();
        pronouns.add( new PronounDO("já (muž)", "I (male)", Person.first, Number.singular, Gender.male));
        pronouns.add( new PronounDO("já (žena)", "I (female)", Person.first, Number.singular, Gender.female));
        pronouns.add( new PronounDO("ty (muž)", "you (male)", Person.second, Number.singular, Gender.male));
        pronouns.add( new PronounDO("ty (žena)", "you (female)", Person.second, Number.singular, Gender.female));
        pronouns.add( new PronounDO("on", "he", Person.third, Number.singular, Gender.male));
        pronouns.add( new PronounDO("ona", "she", Person.third, Number.singular, Gender.female));
        pronouns.add( new PronounDO("ono", "it", Person.third, Number.singular, Gender.neuter));
        pronouns.add( new PronounDO("my", "we", Person.first, Number.plural, Gender.male));
        pronouns.add( new PronounDO("vy", "you", Person.second, Number.plural, Gender.male));
        pronouns.add( new PronounDO("oni", "they", Person.third, Number.plural, Gender.male));

    }
}
