/**
 * 
 */
package cz.atha.flashcards.grammar;


import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.atha.flashcards.dao.Word;

/**
 * @author rob
 * 
 */
public interface WordDO extends Parcelable, Cloneable, Comparable<WordDO> {


    /**
	 * @return the question
	 */
	String getTarget();

	/**
	 * @param t
	 */
	void setTarget(String t);

	/**
	 * @return the answer
	 */
	String getMeaning();

	/**
	 * @param m
	 *            the answer to set
	 */
	void setMeaning(String m);


	/**
	 * @return the imageURI
	 */
	String getImage();

	/**
	 * @param id
	 *            the imageURI to set
	 */
	void setImage(String id);

	/**
	 * @return filename of audio
	 */
	String getAudio();

	/**
	 * @param a
	 *
	 */
	void setAudio(String a);

	List<String> getRelatedWordIds();

	void setRelatedWordIds(List<String> relatedWordIds);

	List<String> getExamples();

	void setExamples(List<String> examples);

	public Object clone() throws CloneNotSupportedException ;

	static Map<String, Class<? extends WordDO>> wordToWordDOTypeMap = new HashMap<>();

	static WordDO toWordDO(Word word) {
		Class<? extends WordDO> clazz = null;
		switch (word.partOfSpeech) {
			case "adjective":
				clazz = AdjectiveDO.class;
				break;
			case "adverb":
				clazz = AdverbDO.class;
				break;
			case "expression":
				clazz = ExpressionDO.class;
				break;
			case "noun":
				clazz = NounDO.class;
				break;
			case "number":
				clazz = NumberDO.class;
				break;
			case "pronoun":
				clazz = PronounDO.class;
				break;
			case "preposition":
				clazz = PrepositionDO.class;
				break;
			case "verb":
				clazz = VerbDO.class;
				break;
			default:
				clazz = ExpressionDO.class;
				break;
		}

		WordDO wordDO = null;
		try {
			wordDO = clazz.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			wordDO = new ExpressionDO();
		}

		wordDO.setTarget(word.target);
		wordDO.setMeaning(word.meaning);
		wordDO.setImage(word.image);
		wordDO.setAudio(word.audio);

		// animate defaults to false
		if (wordDO instanceof Animation.HasAnimation) {
			String animate = word.animate;
			((Animation.HasAnimation) wordDO).setAnimate(Animation.toAnimation(animate));
		}

		// gender defaults to Gender.unknown
		if (wordDO instanceof Gender.HasGender) {
			String gender = word.gender;
			((Gender.HasGender) wordDO).setGender(Gender.toGender(gender));
		}

		// preposition specific
		if (wordDO instanceof HasExtendedForm) {
			String extendedForm = word.extended_form;
			((HasExtendedForm) wordDO).setExtendedForm(extendedForm);
		}
		// verb specific
		if (wordDO instanceof Aspect.HasAspect) {
			String aspect = word.aspect;
			((Aspect.HasAspect) wordDO).setAspect(Aspect.toAspect(aspect));
		}

		return wordDO;
	}

	static List<WordDO> toWordDO(List<Word> words) {
		List list = new ArrayList();
		for(Word word : words){
			WordDO wordDO = toWordDO(word);
			list.add(wordDO);
		}
		return list;
	}
}
