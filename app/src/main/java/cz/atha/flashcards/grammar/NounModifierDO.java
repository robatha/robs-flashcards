/**
 * 
 */
package cz.atha.flashcards.grammar;

/**
 * @author rob
 * 
 */
public class NounModifierDO implements Case.HasCase, Number.HasNumber {


	// static properties of this word - used for declination
	private Case _case = Case.unknown;
	private Number number = Number.unknown;


	@Override
	public Case getCase() {
		return this._case;
	}

	@Override
	public void setCase(Case c) {
		this._case = c;
	}

	@Override
	public Number getNumber() {
		return this.number;
	}

	@Override
	public void setNumber(Number n) {
		this.number = n;
	}



	@Override
	public String toString() {

		return "NounDO [ case=" + _case + ", number=" + number + "]";
	}

}
