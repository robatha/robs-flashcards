package cz.atha.flashcards.grammar;

import java.util.Locale;


public enum Tense {
	present, past, future, unknown;

	public static Tense toTense(String str) {
		if (str != null) {
			// try string match Pr,Pa,F
			String c = new String(str).toUpperCase(Locale.ENGLISH);
			if (c.startsWith("PR"))
				return Tense.present;
			else if (c.startsWith("PA"))
				return Tense.past;
			else if (c.startsWith("F") || c.startsWith("FU"))
				return Tense.future;
		}
		// give up
		return Tense.unknown;
	}
	public interface HasTense {


		void setTense(Tense t);
		Tense getTense();
	}
}