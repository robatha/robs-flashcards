/**
 * 
 */
package cz.atha.flashcards.grammar;

/**
 * @author rob
 * 
 */

/**
 * @author rob
 *
 */
public enum Gender {

	male, female, neuter, unknown;

	public static Gender toGender(String str) {
		if (str != null) {
			if (str.startsWith("f"))
				return Gender.female;
			else if (str.startsWith("m"))
				return Gender.male;
			else if (str.startsWith("n"))
				return Gender.neuter;
		}

		return Gender.unknown;
	}

	public interface HasGender {

		Gender getGender();
		void setGender(Gender g);
	}

}