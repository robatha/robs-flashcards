package cz.atha.flashcards.grammar;

import java.util.Locale;


public enum Case {

	nominative, genitive, dative, accusative, vocative, locative, instrumental, unknown;
	private final static String LOGTAG = "Case";
	public static Case toCase(String str) {
		if (str != null) {
			// try number 1-7 first
			String c = new String(str).toUpperCase(Locale.ENGLISH);
			if (c.startsWith("1")) return Case.nominative;
			if (c.startsWith("2")) return Case.genitive;
			if (c.startsWith("3")) return Case.dative;
			if (c.startsWith("4")) return Case.accusative;
			if (c.startsWith("5")) return Case.vocative;
			if (c.startsWith("6")) return Case.locative;
			if (c.startsWith("7")) return Case.instrumental;

			// try string match N,G,D,A,V,L,I
			if (c.startsWith("N")) return Case.nominative;
			if (c.startsWith("G")) return Case.genitive;
			if (c.startsWith("D")) return Case.dative;
			if (c.startsWith("A")) return Case.accusative;
			if (c.startsWith("V")) return Case.vocative;
			if (c.startsWith("L")) return Case.locative;
			if (c.startsWith("I")) return Case.instrumental;
		}
		// give up
		return Case.unknown;
	}
	public interface HasCase {

		/**
		 * @author rob
		 *
		 */


		Case getCase();
		void setCase(Case c);


	}
}