
package cz.atha.flashcards.grammar;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import cz.atha.flashcards.grammar.models.HasModel;
import cz.atha.flashcards.grammar.models.ModelDO;
import cz.atha.flashcards.grammar.models.VerbModelDO;
import cz.atha.flashcards.grammar.models.VerbModelFormDO;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author rob
 */
public class VerbDO implements WordDO, Aspect.HasAspect, HasModel {

    private final static String LOGTAG = "VerbDO";

    // used for every word
    private String target;
    private String meaning;
    private String image;
    private String audio;
    private List<String> relatedWords = null;
    private List<String> examples = null;

    // static properties of this word - used for conjugation
    private Aspect aspect = Aspect.unknown;
    private VerbModelDO model = null;

    public VerbDO() {
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public VerbDO createFromParcel(Parcel in) {
            VerbDO vo = new VerbDO();
            vo.target = in.readString();
            vo.meaning = in.readString();
            vo.image = in.readString();
            vo.audio = in.readString();
            vo.relatedWords = in.createStringArrayList();
            vo.examples = in.createStringArrayList();
            vo.aspect = Aspect.valueOf(in.readString());
            // TODO model

            return vo;
        }

        public VerbDO[] newArray(int size) {
            return new VerbDO[size];
        }

    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.target);
        dest.writeString(this.meaning);
        dest.writeString(this.image);
        dest.writeString(this.audio);
        dest.writeStringList(this.relatedWords);
        dest.writeStringList(this.examples);
        dest.writeString(this.aspect.name());
        // TODO model
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String getTarget() {
        return target;
    }

    @Override
    public void setTarget(String t) {
        this.target = t;
    }

    @Override
    public String getMeaning() {
        return meaning;
    }

    @Override
    public void setMeaning(String m) {
        this.meaning = m;
    }

    @Override
    public ModelDO getModel() {
        return model;
    }

    @Override
    public void setModel(ModelDO m) {
        this.model = (VerbModelDO) m;
    }

    @Override
    public Aspect getAspect() {
        return this.aspect;
    }

    @Override
    public void setAspect(Aspect a) {
        this.aspect = a;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public void setImage(String i) {
        this.image = i;
    }

    @Override
    public String getAudio() {
        return this.audio;
    }

    @Override
    public void setAudio(String a) {
        this.audio = a;
    }

    @Override
    public List<String> getRelatedWordIds() {
        return relatedWords;
    }

    @Override
    public void setRelatedWordIds(List<String> relatedWords) {
        this.relatedWords = relatedWords;
    }

    @Override
    public List<String> getExamples() {
        return examples;
    }

    @Override
    public void setExamples(List<String> examples) {
        this.examples = examples;
    }

    @Override
    public String getForm(Object modifier) {

        String dictionaryForm = getTarget();
        String rest = "";
        // the below fixes it when we get a phrase and the verb is really the first word
        int i = getTarget().indexOf(' ');
        if (i > 0) {
            dictionaryForm = getTarget().substring(0, i);
            rest = getTarget().substring(i);
        }
        // in case we try to get form from word with no model!
        if (model == null)
            return null;

        Log.d(LOGTAG, "Calling getForm() dictionaryForm=" + dictionaryForm);
        Log.d(LOGTAG, "Calling getForm() modifier=" + modifier.toString());
        Log.d(LOGTAG, "Calling getForm() model=" + model.getName());


        VerbModelFormDO foundForm = null;
        // find form with the best score
        int score = -1;
        for (VerbModelFormDO form : model.getForms()) {
            int newScore = form.isCorrectForm(modifier, this.aspect);
            if (newScore > score) {
                score = newScore;
                foundForm = form;
            }
        }

        // didn't find appropriate form
        if (foundForm == null)
            return null;
        Log.d(LOGTAG, "found form=" + foundForm.toString());

        // get replace string
        String replaceString = foundForm.getReplaceString();
        if (replaceString == null) {
            Log.e(LOGTAG, "No replace string: " + modifier.toString());
            return null;
        }
        Log.d(LOGTAG, "found replace string: " + replaceString);

        // replace with stem
        Pattern p = Pattern.compile(model.getRegex());
        Matcher m = p.matcher(dictionaryForm);

        // if no match or not enough groups to make new form, keep looking
        if (!m.matches()) {
            Log.e(LOGTAG, "no match: " + p.pattern());
            return null;
        }
        int replaceGroupsCount = replaceString.split("$").length - 1;
        if (m.groupCount() < replaceGroupsCount) {
            Log.e(LOGTAG, "no groupcountmatch: " + m.groupCount() + " vs "
                    + replaceGroupsCount);
            return null;
        }
        return  m.replaceAll(replaceString) + rest;
    }

    public static String getAuxiliaryVerbs(Object modifier, VerbDO verbDO, String verbPhrase) {
        Tense tense = Tense.present;
        Voice voice = Voice.active;
        Mood mood = Mood.indicative;

        String verb = verbPhrase;
        String theRest = "";

        if (verbPhrase == null){
            Log.e(LOGTAG, "verb is null: " + verbPhrase);
            return null;
        }
        int index = verbPhrase.indexOf(' ');
        if(index > 0) {
            theRest = verbPhrase.substring(index).trim();
            verb = verbPhrase.substring(0, index);
        }

        if ((modifier instanceof Tense.HasTense) && ((Tense.HasTense)modifier).getTense() != Tense.unknown)
            tense = ((Tense.HasTense)modifier).getTense();
        if ((modifier instanceof Voice.HasVoice) && ((Voice.HasVoice) modifier).getVoice() == Voice.unknown)
            voice = ((Voice.HasVoice) modifier).getVoice();
        if ((modifier instanceof Mood.HasMood) && ((Mood.HasMood) modifier).getMood() != Mood.unknown)
            mood = ((Mood.HasMood) modifier).getMood();


        // add aux verbs
        // TODO we're only supporting active indicative now, add more here
        String aux = null;
        if (voice == Voice.active) {

            // if it's future and the verb form is unchanged, then we do this by adding 'byt + inf'
            if (mood == Mood.indicative && tense == Tense.future && verbDO.getTarget().equals(verbPhrase)) {
                aux = getActiveIndicativeFutureHelper(modifier);
                // search for reflexivies so we can move it to position 2
                if (theRest.startsWith("se ")){
                    aux = aux + " se";
                    theRest = theRest.substring(2);
                }
                else if (theRest.startsWith("si ")){
                    aux = aux +" si";
                    theRest = theRest.substring(2);
                }
                return aux + " " + verb.trim() + theRest;

            } else {
                if (mood == Mood.indicative && tense == Tense.past) {
                    aux = getActiveIndicativePastHelper(modifier);
                } else if (mood == Mood.conditional && tense == Tense.present){
                    aux = getActiveConditionalPresentHelper(modifier);
                }
                // search for 'jsi se' and 'jsi si' to replace with 'ses' and 'sis'
                if (aux != null && theRest != null) {
                    if (aux.equals("jsi")) {
                        if (theRest.startsWith("se ")) {
                            aux = "ses";
                            theRest = theRest.substring(3);
                        } else if (theRest.startsWith("si ")) {
                            aux = "sis";
                            theRest = theRest.substring(3);
                        }
                    }
                }
                String ret = verb.trim();
                if(aux != null && aux.length() > 0)
                    ret = ret + " " + aux.trim();
                if (theRest != null && theRest.length() > 0)
                    ret = ret + " " + theRest.trim();
                return ret;

            }
        }

        return verbPhrase;
    }

    static String getActiveIndicativePastHelper(Object modifier) {
        Person p = Person.unknown;
        Number n = Number.unknown;
        if (modifier instanceof Person.HasPerson) p = ((Person.HasPerson) modifier).getPerson();
        if (modifier instanceof Number.HasNumber) n = ((Number.HasNumber) modifier).getNumber();

        switch (p) {
            // we are assuming that if we don't know the number, default to plural
            // if we don't know the person, default to third
            case first:
                return (n == Number.singular) ? "jsem" : "jsme";
            case second:
                return (n == Number.singular) ? "jsi" : "jste";
            default:
                return "";
        }
    }

    static String getActiveIndicativeFutureHelper(Object modifier) {
        Person p = Person.unknown;
        Number n = Number.unknown;
        if (modifier instanceof Person.HasPerson) p = ((Person.HasPerson) modifier).getPerson();
        if (modifier instanceof Number.HasNumber) n = ((Number.HasNumber) modifier).getNumber();

        switch (p) {
            // we are assuming that if we don't know the number, default to plural
            // if we don't know the person, default to third
            case first:
                return (n == Number.singular) ? "budu" : "budeme";
            case second:
                return (n == Number.singular) ? "budeš" : "budete";
            default:
                return (n == Number.singular) ? "bude" : "budou";
        }
    }

    static String getActiveConditionalPresentHelper(Object modifier) {
        Person p = Person.unknown;
        Number n = Number.unknown;
        if (modifier instanceof Person.HasPerson) p = ((Person.HasPerson) modifier).getPerson();
        if (modifier instanceof Number.HasNumber) n = ((Number.HasNumber) modifier).getNumber();

        switch (p) {
            // we are assuming that if we don't know the number, default to plural
            // if we don't know the person, default to third
            case first:
                return (n == Number.singular) ? "bych" : "bychom";
            case second:
                return (n == Number.singular) ? "bys" : "byste";
            default:
                return "by";
        }
    }

    @Override
    public String toString() {
        return "VerbDO{" +
                "LOGTAG='" + LOGTAG + '\'' +
                ", target='" + target + '\'' +
                ", meaning='" + meaning + '\'' +
                ", image='" + image + '\'' +
                ", audio='" + audio + '\'' +
                ", relatedWords=" + relatedWords +
                ", examples=" + examples +
                ", aspect=" + aspect +
                ", model=" + model +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        WordDO copy = (WordDO) super.clone();
        copy.setRelatedWordIds(new ArrayList<>(copy.getRelatedWordIds()));
        copy.setExamples(new ArrayList<>(copy.getExamples()));
        return copy;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        VerbDO compare = (VerbDO) obj;

        if (target == null) {
            if (compare.target != null) return false;
        } else if (!target.equals(compare.target)) return false;

        if (meaning == null) {
            if (compare.meaning != null) return false;
        } else if (!meaning.equals(compare.meaning)) return false;

        if (image == null) {
            if (compare.image != null) return false;
        } else if (!image.equals(compare.image)) return false;

        if (audio == null) {
            if (compare.audio != null) return false;
        } else if (!audio.equals(compare.audio)) return false;

        if (model == null) {
            if (compare.model != null) return false;
        } else if (!model.equals(compare.model)) return false;

        if (aspect == null) {
            if (compare.aspect != null) return false;
        } else if (!aspect.equals(compare.aspect)) return false;

        return true;
    }

    @Override
    public int hashCode() {

        int result = 17;
        if (target != null)
            result = 31 * result + target.hashCode();
        if (meaning != null)
            result = 31 * result + meaning.hashCode();
        if (image != null)
            result = 31 * result + image.hashCode();
        if (audio != null)
            result = 31 * result + audio.hashCode();
        if (model != null)
            result = 31 * result + model.hashCode();
        if (aspect != null)
            result = 31 * result + aspect.hashCode();
        // TODO animate???

        return result;

    }

    @Override
    public int compareTo(WordDO o) {
        Collator coll = Collator.getInstance(Locale.forLanguageTag("cs-CZ"));
        coll.setStrength(Collator.PRIMARY);

        return coll.compare(this.target, o.getTarget());
    }
}
