/**
 * 
 */
package cz.atha.flashcards.grammar;


import java.util.Locale;

/**
 * @author rob
 * 
 */

public enum Person {
	first, second, third, unknown;
	public static Person toPerson(String str) {
		if (str != null) {
			// try number 1-3 first
			int i = 0;
			try {
				i = Integer.parseInt(str);
			} catch (Exception e) {
			}
			switch (i) {
				case 1: return Person.first;
				case 2: return Person.second;
				case 3: return Person.third;
			}

			// try string match F,S,T
			String p = new String(str).toUpperCase(Locale.ENGLISH);
			if (p.startsWith("F"))
				return Person.first;
			if (p.startsWith("G"))
				return Person.second;
			if (p.startsWith("D"))
				return Person.third;
		}
		// give up
		return Person.unknown;
	}
	public interface HasPerson {

		Person getPerson();
		void setPerson(Person p);

	}
}