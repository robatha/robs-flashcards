package cz.atha.flashcards.grammar;

public enum Animation {

    animate, inanimate, unknown;

    public static Animation toAnimation(String str) {
        if (str != null) {
            if (str.startsWith("a"))
                return Animation.animate;
            else if (str.startsWith("i"))
                return Animation.inanimate;
        }

        return Animation.unknown;
    }

    public interface HasAnimation {

        Animation getAnimate();

        void setAnimate(Animation a);

    }
}