package cz.atha.flashcards.grammar.models;

import cz.atha.flashcards.grammar.Animation;
import cz.atha.flashcards.grammar.Case;
import cz.atha.flashcards.grammar.Gender;
import cz.atha.flashcards.grammar.Number;
import cz.atha.flashcards.dao.ModelNounForm;

public class NounModelFormDO {

    private long id;
    private Gender gender;
    private Animation animate;
    private Case caze;
    private Number number;
    private String replaceString;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Animation getAnimate() {
        return animate;
    }

    public void setAnimate(Animation animate) {
        this.animate = animate;
    }

    public Case getCase() {
        return caze;
    }

    public void setCase(Case caze) {
        this.caze = caze;
    }

    public Number getNumber() {
        return number;
    }

    public void setNumber(Number number) {
        this.number = number;
    }

    public String getReplaceString() {
        return replaceString;
    }

    public void setReplaceString(String replaceString) {
        this.replaceString = replaceString;
    }

    public NounModelFormDO(ModelNounForm obj){
        this.id = obj.id;
        this.gender = Gender.toGender(obj.gender);
        this.animate = Animation.toAnimation(obj.animate);
        this.caze = Case.toCase(obj.caze);
        this.number = Number.toNumber(obj.number);
        this.replaceString = obj.replaceString;
    }

    public int isCorrectForm(Object obj) {
        int score = 0;

        if (obj == null)
            return -1;

        Gender genderOfObj = Gender.unknown;
        Animation animationOfObj = Animation.unknown;
        Number numberOfObj = Number.unknown;
        Case caseOfObj = Case.unknown;

        if (obj instanceof Gender.HasGender) genderOfObj = ((Gender.HasGender) obj).getGender();
        if (obj instanceof Animation.HasAnimation)
            animationOfObj = ((Animation.HasAnimation) obj).getAnimate();
        if (obj instanceof Number.HasNumber) numberOfObj = ((Number.HasNumber) obj).getNumber();
        if (obj instanceof Case.HasCase) caseOfObj = ((Case.HasCase) obj).getCase();

        // direct match
        if (genderOfObj == this.gender) {
            score++;
        }
        // direct mismatch
        else if (genderOfObj != Gender.unknown && this.gender != Gender.unknown) {
//            Log.d(LOGTAG, "Failed on gender!" + this.gender + " vs " + genderOfObj);
            return -1;
        }

        // direct match
        if (animationOfObj == this.animate) {
            score++;
        }
        // direct mismatch
        else if (animationOfObj != Animation.unknown && this.animate != Animation.unknown) {
            //          Log.d(LOGTAG, "Failed on animation!" + this.animate + " vs " + animationOfObj);
            //          Log.d(LOGTAG, "form-" + this.toString());
            return -1;
        }

        if (numberOfObj == this.number) {
            score++;
        }
        // direct mismatch
        else if (numberOfObj != Number.unknown && this.number != Number.unknown) {
            //        Log.d(LOGTAG, "Failed on number!" + this.number + " vs " + numberOfObj);
            return -1;
        }

        if (caseOfObj == this.caze) {
            score++;
        }
        else if (caseOfObj != Case.unknown && this.caze != Case.unknown){
            return -1;
        }

        return score;
    }

    @Override
    public String toString() {
        return "NounModelFormDO{" +
                "id=" + id +
                ", gender=" + gender +
                ", animate=" + animate +
                ", caze=" + caze +
                ", number=" + number +
                ", replaceString='" + replaceString + '\'' +
                '}';
    }
}
