/**
 * 
 */
package cz.atha.flashcards.grammar;

/**
 * @author rob
 * 
 */

public enum Voice {
	active, passive, none, unknown;

	public static Voice toVoice(String str) {
		if (str != null) {
			if (str.startsWith("p"))
				return Voice.passive;
			else if (str.startsWith("a"))
				return Voice.active;
			else if (str.startsWith("n"))
				return Voice.none;
		}

		return Voice.unknown;
	}
	public interface HasVoice {
		Voice getVoice();
		void setVoice(Voice v);
	}
}