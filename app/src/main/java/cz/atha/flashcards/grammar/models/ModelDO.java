package cz.atha.flashcards.grammar.models;


import java.util.List;


public interface ModelDO <T>{


    long getId();

    void setId(long id);

    String getSrc();

    void setSrc(String src);

    String getName();

    void setName(String name);

    String getRegex();

    void setRegex(String regex);

    List<T> getForms();

    void setForms(List<T> forms);



}

