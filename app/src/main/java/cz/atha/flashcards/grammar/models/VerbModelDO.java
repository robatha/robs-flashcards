package cz.atha.flashcards.grammar.models;


import java.util.ArrayList;
import java.util.List;


public class VerbModelDO implements ModelDO<VerbModelFormDO> {

    private long id;
    private String src;
    private String name;
    private String regex;
    private List<VerbModelFormDO> forms;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getSrc() {
        return src;
    }

    @Override
    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getRegex() {
        return regex;
    }

    @Override
    public void setRegex(String regex) {
        this.regex = regex;
    }

    @Override
    public List<VerbModelFormDO> getForms() {
        return forms;
    }

    @Override
    public void setForms(List<VerbModelFormDO> forms) {
        this.forms = forms;
    }

    public VerbModelDO(ModelWithForms obj){
        this.id = obj.getModel().id;
        this.src = obj.getModel().src;
        this.name = obj.getModel().name;
        this.regex = obj.getModel().regex;
        this.forms = new ArrayList();
    }


}

