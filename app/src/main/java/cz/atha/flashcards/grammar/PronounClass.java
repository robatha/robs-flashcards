/**
 * 
 */
package cz.atha.flashcards.grammar;


/**
 * In Czech, there are seven pronoun classes:
 *
 * <ol>
 * <li>personal: já, ty, on, ona, ono, my, vy, oni, ony, ona, and the reflexive se (-self)</li>
 * <li>possesive: můj, tvůj, jeho, její, náš, váš, jejich, svůj (reflexive)</li>
 * <li>demonstrative: ten, tento, tenhle, onen, takový, týž, tentýž, sám (this/that)</li>
 * <li>interrogative: kdo, co, jaký, který, čí (equivalent of Wh- words in English)</li>
 * <li>relative: kdo, co, jaký, který, čí, jenž</li>
 * <li>indeterminate: někdo, něco, některý, nějaký, něčí; ledakdo...; kdokoli...;</li>
 * <li>kdosi, cosi, kterýsi, jakýsi, čísi; leckdo, lecco, leckterý...;</li>
 * <li>každý, všechen (like something/anything in English)</li>
 * <li>negative: nikdo, nic, nijaký, ničí, žádný</li>
 * </ol>
 * @author rob
 */
public enum PronounClass {

personal, possesive, demonstrative, interrogative, relative, indeterminate, negative, unknown;

	public static PronounClass toPronounClass(String str) {
		if (str != null) {
			if (str.startsWith("per"))
				return PronounClass.personal;
			else if (str.startsWith("pos"))
				return PronounClass.possesive;
			else if (str.startsWith("d"))
				return PronounClass.demonstrative;
			else if (str.startsWith("int"))
				return PronounClass.interrogative;
			else if (str.startsWith("r"))
				return PronounClass.relative;
			else if (str.startsWith("ind"))
				return PronounClass.indeterminate;
			else if (str.startsWith("n"))
				return PronounClass.negative;
		}

		return PronounClass.unknown;
	}

	public interface HasPronounClass {

		PronounClass getPronounClass();
		void setPronounClass(PronounClass c);
	}

}